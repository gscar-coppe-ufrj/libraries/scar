# ==============================================================================
# Macros pretty messages
#

if (NOT WIN32)
    string(ASCII 27 Esc)
    set(ColourReset "${Esc}[m")
    set(ColourBold  "${Esc}[1m")
    set(Red         "${Esc}[31m")
    set(Green       "${Esc}[32m")
    set(Yellow      "${Esc}[33m")
    set(Blue        "${Esc}[34m")
    set(Magenta     "${Esc}[35m")
    set(Cyan        "${Esc}[36m")
    set(White       "${Esc}[37m")
    set(BoldRed     "${Esc}[1;31m")
    set(BoldGreen   "${Esc}[1;32m")
    set(BoldYellow  "${Esc}[1;33m")
    set(BoldBlue    "${Esc}[1;34m")
    set(BoldMagenta "${Esc}[1;35m")
    set(BoldCyan    "${Esc}[1;36m")
    set(BoldWhite   "${Esc}[1;37m")
endif ()

function(message)
    list(GET ARGV 0 MessageType)
    if (MessageType STREQUAL FATAL_ERROR OR MessageType STREQUAL SEND_ERROR)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${BoldRed}${ARGV}${ColourReset}")
    elseif (MessageType STREQUAL WARNING)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${BoldYellow}${ARGV}${ColourReset}")
    elseif (MessageType STREQUAL AUTHOR_WARNING)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${BoldCyan}${ARGV}${ColourReset}")
    elseif (MessageType STREQUAL STATUS)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${Green}${ARGV}${ColourReset}")
    else ()
        _message("${ARGV}")
    endif ()
endfunction()


# ==============================================================================
# Macros for Source file management
#
#  SCAR_SET_SOURCES - Like SCAR_SET_SOURCES_PLAT with platform = ON (Built on all platforms)
#    Usage: SCAR_SET_SOURCES( out name sources)
#    Example: SCAR_SET_SOURCES( SRCS Foundation src/Foundation.cpp)
#
#  SCAR_SET_HEADERS - Adds a list of files to the headers of a components
#    Usage: SCAR_SET_HEADERS( out name headers)
#      INPUT:
#           out             the variable the headers are added to
#           name:           the name of the components
#           headers:        a list of files to add to HDRSt
#    Example: SCAR_SET_HEADERS( SCAR_HEADERS type include/type/Time.hpp include/types/Point.hpp )
#

macro(SCAR_ADD_COMPONENT_IF_ENABLED component)
    string(TOUPPER "SCAR_ENABLE_${component}" enable_component)
    string(REPLACE "-" "_" enable_component ${enable_component})
    if (${enable_component})
        SCAR_ADD_COMPONENT("${component}")
    endif ()
endmacro()

macro(SCAR_ADD_COMPONENT component)
    add_subdirectory("${component}")
    list(APPEND SCAR_COMPONENTS "${component}")
endmacro()

macro(SCAR_DISPLAY_QTCREATOR package)
    if (SCAR_ENABLE_QTCREATOR_FILES)
        if (TARGET "dummy-${package}")
            target_sources("dummy-${package}" ${ARGN})
        else ()
            add_custom_target("dummy-${package}" SOURCES ${ARGN})
        endif ()
    endif ()
endmacro()

macro(SCAR_SET_SOURCES_RECURSE package)
    file(GLOB_RECURSE SCAR_TMP "src/*.cpp" "src/*.c")
    SCAR_SET_SOURCES(${package} ${SCAR_TMP})
endmacro()

macro(SCAR_SET_SOURCES package)
    SCAR_DISPLAY_QTCREATOR(${pacakge} ${ARGN})
    source_group("${package}\\Source Files" FILES ${ARGN})
    string(TOUPPER "SCAR_${package}_SOURCES" SCAR_TMP)
    list(APPEND ${SCAR_TMP} ${ARGN})
    list(APPEND SCAR_SOURCES ${ARGN})
    set(${SCAR_TMP} ${${SCAR_TMP}} PARENT_SCOPE)
    set(SCAR_SOURCES ${SCAR_SOURCES} PARENT_SCOPE)
endmacro()

macro(SCAR_SET_HEADERS_RECURSE package)
    file(GLOB_RECURSE SCAR_TMP "include/*.hpp" "include/*.h")
    SCAR_SET_HEADERS(${package} ${SCAR_TMP})
endmacro()

macro(SCAR_SET_HEADERS package)
    SCAR_DISPLAY_QTCREATOR(${pacakge} ${ARGN})
    source_group("${package}\\Header Files" FILES ${ARGN})
    set_source_files_properties(${ARGN} PROPERTIES HEADER_FILE_ONLY TRUE)
    string(TOUPPER "SCAR_${package}_HEADERS" SCAR_TMP)
    list(APPEND ${SCAR_TMP} ${ARGN})
    list(APPEND SCAR_HEADERS ${ARGN})
    set(${SCAR_TMP} ${${SCAR_TMP}} PARENT_SCOPE)
    set(SCAR_HEADERS ${SCAR_HEADERS} PARENT_SCOPE)
endmacro()

macro(SCAR_DISPLAY_VISUAL_STUDIO package)
    source_group("${package}\\Header Files" FILES ${ARGN})
endmacro()

# ==============================================================================
# General target configuration macros
#

macro(SCAR_CONFIGURE_CURRENT_TARGET target_name)
    set(SCAR_CURRENT_TARGET ${target_name})
    string(REPLACE "-" "_" SCAR_CURRENT_TARGET_PY "${SCAR_CURRENT_TARGET}_py")
endmacro()

# ==============================================================================
# Macros for configuring Python bindings files
#
#  SCAR_GENERATE_PYTHON_BINDINGS - Generates Python bindings files
#    Usage: SCAR_GENERATE_PYTHON_BINDINGS(target_name)
#      INPUT:
#           target_name             the name of the target. e.g. linear-system
#    Example: SCAR_GENERATE_PYTHON_BINDINGS(linear-system)
#

macro(SCAR_TARGET_HAS_PYTHON_BINDINGS ret)
    set(SCAR_CURRENT_PYTHON_DIR "${CMAKE_CURRENT_LIST_DIR}/python")
    if (SCAR_ENABLE_PYBIND AND EXISTS "${SCAR_CURRENT_PYTHON_DIR}/setup.py" AND EXISTS "${SCAR_CURRENT_PYTHON_DIR}/__init__.py")
        set(${ret} ON)
    else()
        set(${ret} OFF)
    endif()
endmacro()

macro(SCAR_GENERATE_PYTHON_BINDINGS target_name)
    SCAR_CONFIGURE_CURRENT_TARGET(${target_name})
    SCAR_TARGET_HAS_PYTHON_BINDINGS(SCAR_TMP)
    if (SCAR_TMP)
        pybind11_add_module(${SCAR_CURRENT_TARGET_PY} python/python_bindings.cpp)
        target_link_libraries(${SCAR_CURRENT_TARGET_PY} PUBLIC ${target_name})
        set_target_properties(${SCAR_CURRENT_TARGET_PY}
            PROPERTIES
            SUFFIX ".so"
            LIBRARY_OUTPUT_DIRECTORY "${SCAR_PYTHON_BUILD_DIRECTORY}/${target_name}/python/${target_name}"
        )
        configure_file(
            "python/__init__.py"
            "${SCAR_PYTHON_BUILD_DIRECTORY}/${target_name}/python/${target_name}/__init__.py"
        )
        configure_file(
            "python/setup.py"
            "${SCAR_PYTHON_BUILD_DIRECTORY}/${target_name}/python/setup.py"
        )
    endif ()
endmacro()

# ==============================================================================
# Macros for Package generation and installation
#
#  SCAR_GENERATE_PACKAGE - Generates *Config.cmake
#    Usage: SCAR_GENERATE_PACKAGE(target_name)
#      INPUT:
#           target_name             the name of the target. e.g. types
#    Example: SCAR_GENERATE_PACKAGE(types)
#
#  SCAR_INSTALL - Install the given target
#    Usage: SCAR_INSTALL(target_name)
#      INPUT:
#           target_name             the name of the target. e.g. types
#    Example: SCAR_INSTALL(types)
#

macro(SCAR_GENERATE_PACKAGE target_name pkgcfg_requires pkgcfg_libs pkgcfg_cflags)
    include(CMakePackageConfigHelpers)
    write_basic_package_version_file(
        "${CMAKE_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-${target_name}ConfigVersion.cmake"
        VERSION ${PROJECT_VERSION}
        COMPATIBILITY ExactVersion
    )
    export(EXPORT "${target_name}Targets"
        FILE "${CMAKE_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-${target_name}Targets.cmake"
        NAMESPACE "${SCAR_NAMESPACE}::"
    )
    configure_file("cmake/library.cmake.in"
        "${CMAKE_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-${target_name}Config.cmake"
        @ONLY
    )

    # Set config script install location in a location that find_package() will
    # look for, which is different on MS Windows than for UNIX
    # Note: also set in root CMakeLists.txt
    if (WIN32)
        set(ScarConfigPackageLocation "cmake")
    else ()
        set(ScarConfigPackageLocation "lib/${PROJECT_NAME}/cmake")
    endif ()

    install(
        EXPORT "${target_name}Targets"
        FILE "${PROJECT_NAME}-${target_name}Targets.cmake"
        NAMESPACE "${SCAR_NAMESPACE}::"
        DESTINATION "${ScarConfigPackageLocation}"
    )

    install(
        FILES
            "${CMAKE_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-${target_name}Config.cmake"
            "${CMAKE_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-${target_name}ConfigVersion.cmake"
        DESTINATION "${ScarConfigPackageLocation}"
        COMPONENT devel
    )

    # PkgConfig stuff
    set(PKGCONFIG_REQUIRES "${pkgcfg_requires}")
    set(PKGCONFIG_LIBS     "${pkgcfg_libs}")
    set(PKGCONFIG_CFLAGS   "${pkgcfg_cflags}")
    configure_file(
        "${CMAKE_CURRENT_LIST_DIR}/cmake/library.pc.in"
        "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/pkgconfig/${PROJECT_NAME}-${target_name}.pc"
        @ONLY
    )
endmacro()

macro(SCAR_INSTALL target_name)
    install(
        DIRECTORY include/${PROJECT_NAME}
        DESTINATION include
        COMPONENT devel
        FILES_MATCHING REGEX "[a-zA-Z0-9_-]*\.(h|hpp)"
    )

    install(
        TARGETS "${target_name}" EXPORT "${target_name}Targets"
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin
        INCLUDES DESTINATION include/${PROJECT_NAME}
    )

    # Install python library
    SCAR_TARGET_HAS_PYTHON_BINDINGS(SCAR_TMP)
    if (SCAR_TMP)
        install(
            CODE "execute_process(
                COMMAND python3 setup.py install --prefix=${CMAKE_INSTALL_PREFIX}
                WORKING_DIRECTORY ${SCAR_PYTHON_BUILD_DIRECTORY}/${target_name}/python
            )"
        )
    endif ()
endmacro()
