if (CMAKE_VERSION VERSION_LESS 3.2.0)
    message(FATAL_ERROR "SCAR requires at least CMake version 3.2.0")
endif ()

if (NOT @PROJECT_NAME@_FIND_COMPONENTS)
    return()
endif ()

set(_@PROJECT_NAME@_FIND_PARTS_REQUIRED)
if (@PROJECT_NAME@_FIND_REQUIRED)
    set(_@PROJECT_NAME@_FIND_PARTS_REQUIRED REQUIRED)
endif ()
set(_@PROJECT_NAME@_FIND_PARTS_QUIET)
if (@PROJECT_NAME@_FIND_QUIETLY)
    set(_@PROJECT_NAME@_FIND_PARTS_QUIET QUIET)
endif ()

get_filename_component(_@PROJECT_NAME@_install_prefix "${CMAKE_CURRENT_LIST_DIR}" ABSOLUTE)

set(_@PROJECT_NAME@_NOTFOUND_MESSAGE)

# Let components find each other, but don't overwrite CMAKE_PREFIX_PATH
set(_@PROJECT_NAME@_CMAKE_PREFIX_PATH_old ${CMAKE_PREFIX_PATH})
set(CMAKE_PREFIX_PATH ${_@PROJECT_NAME@_install_prefix})

foreach(component ${@PROJECT_NAME@_FIND_COMPONENTS})
    find_package(@PROJECT_NAME@-${component}
        ${_@PROJECT_NAME@_FIND_PARTS_QUIET}
        ${_@PROJECT_NAME@_FIND_PARTS_REQUIRED}
        PATHS "${_@PROJECT_NAME@_install_prefix}" NO_DEFAULT_PATH
    )
    if (NOT @PROJECT_NAME@-${component}_FOUND)
        if (@PROJECT_NAME@_FIND_REQUIRED_${component})
            set(_@PROJECT_NAME@_NOTFOUND_MESSAGE "${_@PROJECT_NAME@_NOTFOUND_MESSAGE}Failed to find SCAR component \"${component}\" config file at \"${_@PROJECT_NAME@_install_prefix}/${component}/${component}Config.cmake\"\n")
        elseif (NOT @PROJECT_NAME@_FIND_QUIETLY)
            message(WARNING "Failed to find SCAR component \"${component}\" config file at \"${_@PROJECT_NAME@_install_prefix}/${component}/${component}Config.cmake\"")
        endif ()
    endif ()
endforeach()

# Restore the original CMAKE_PREFIX_PATH value
set(CMAKE_PREFIX_PATH ${_@PROJECT_NAME@_CMAKE_PREFIX_PATH_old})

if (_@PROJECT_NAME@_NOTFOUND_MESSAGE)
    set(@PROJECT_NAME@_NOT_FOUND_MESSAGE "${_@PROJECT_NAME@_NOTFOUND_MESSAGE}")
    set(@PROJECT_NAME@_FOUND False)
endif ()
