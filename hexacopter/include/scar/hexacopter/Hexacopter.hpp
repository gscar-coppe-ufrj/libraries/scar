/**
 * @file Hexacopter.hpp
 * @author LEAD
 * @brief Defines a class to handle an Hexarotor settings ad retrieve parameters
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include <cmath>
#include <Eigen/Eigen>
// #include "scar/hexacopter/HelperFunctions.hpp"
#include "scar/types/Time.hpp"
#include <stdexcept>

namespace scar
{
namespace hexacopter
{

const double GRAVITY = 9.80665;

typedef Eigen::RowVectorXd Input;
typedef Eigen::VectorXd Output;
typedef Eigen::Matrix3d Rot;

class Hexacopter
{
private:
    double _km;
    double _kf;
    Eigen::VectorXd _max_vel;
    double _arm_length;
    bool _is_symmetrical;
    Rot drone_rotation;

public:
    Hexacopter(double km, double kf, Eigen::VectorXd max_vel, double arm_length, bool is_symmetrical = true);
    ~Hexacopter();
    double getKm() const;
    double getKf() const;
    double getArmLength() const;
    bool is_symmetrical() const;
    Eigen::VectorXd allocator(const Eigen::VectorXd& input);
};

} // namespace hexacopter

} // namespace scar
