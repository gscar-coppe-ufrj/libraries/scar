// #pragma once

// #include <cmath>
// #include <eigen3/Eigen/Eigen>
// #include <iostream>
// #include "scar/hexacopter/Hexacopter.hpp"

// namespace scar
// {
// namespace hexacopter
// {

// /*!
//  * \brief Create alocation matrix to force and torque into motors angular velocities from an hexacopter. 
//  *
//  * \param hexa Hexacopter object
//  * \param input force/torque
//  */
// Eigen::VectorXd alocator(const Hexacopter& hexa, const Eigen::VectorXd& input)
// {
//     if(input.size()!=4)
//     {
//         std::cerr << "Input size different than 4 (X, Y, Z torques and Z force)" << std::endl;
//         return;
//     }

//     Eigen::MatrixXd alocation_matrix(4,6);
    
//     auto arm_length = hexa.getArmLength();
//     auto kf = hexa.getKf();
//     auto km = hexa.getKm();
//     auto angle;
//     auto c60 = std::cos(angle);
//     auto c0 = std::cos(0);
//     auto s60 = std::sin(angle);
//     auto s0 = std::sin(0);
//     auto cte = arm_length*kf;

//     if(!hexa.is_symmetrical())
//     {
//         angle = 0;
//         return;
//     } else
//     {
//         // 60 degrees between each arm
//         angle = 2*M_PI/3;
//     }
    
//     alocation_matrix << 
//         cte*c60,  cte*c0,  cte*c60, -cte*c60, -cte*c0, -cte*c60,
//         cte*s60,  cte*s0, -cte*s60, -cte*s60,  cte*s0,  cte*s60,
//              kf,      kf,       kf,       kf,      kf,       kf,
//              km,     -km,      -km,       km,      km,      -km;
    
//     Eigen::MatrixXd alocation_matrix_inv = alocation_matrix.completeOrthogonalDecomposition().pseudoInverse();
//     return alocation_matrix_inv * input;
// }
// } // namespace hexacopter
    
// } // namespace scar
