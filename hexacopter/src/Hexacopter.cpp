#include "scar/hexacopter/Hexacopter.hpp"

using namespace ::scar::hexacopter;

Hexacopter::Hexacopter(double km, double kf, Eigen::VectorXd max_vel, double arm_length, bool is_symmetrical):
    _km(km), _kf(kf), _max_vel(max_vel), _arm_length(arm_length), _is_symmetrical(is_symmetrical)
{
}

Hexacopter::~Hexacopter()
{
}

double Hexacopter::getKm() const
{
    return this->_km;
}
double Hexacopter::getKf() const 
{
    return this->_kf;
}
double Hexacopter::getArmLength() const
{
    return this->_arm_length;
}

bool Hexacopter::is_symmetrical() const
{
    return this->_is_symmetrical;
}

Eigen::VectorXd Hexacopter::allocator(const Eigen::VectorXd& input)
{
    if(input.size()!=4)
    {
        throw std::invalid_argument ( "Input size different than 4 (X, Y, Z torques and Z force)" );
    }

    Eigen::MatrixXd alocation_matrix(4,6);
    
    const auto arm_length = this->getArmLength();
    auto kf = this->getKf();
    auto km = this->getKm();
    double angle = M_PI/3;
    auto c60 = std::cos(angle);
    auto c0 = std::cos(0);
    auto s60 = std::sin(angle);
    auto s0 = std::sin(0);
    auto cte = arm_length*kf;

    if(!this->is_symmetrical())
    {
        angle = 0;
        Eigen::VectorXd error(4);
        error.setZero();
        return error;
    } else
    {
        // 60 degrees between each arm
        angle = 2*M_PI/3;
    }
    
    alocation_matrix << 
        cte*c60,  cte*c0,  cte*c60, -cte*c60, -cte*c0, -cte*c60,
        cte*s60,  cte*s0, -cte*s60, -cte*s60,  cte*s0,  cte*s60,
             kf,      kf,       kf,       kf,      kf,       kf,
             km,     -km,      -km,       km,      km,      -km;
    
    Eigen::MatrixXd alocation_matrix_inv = alocation_matrix.completeOrthogonalDecomposition().pseudoInverse();
    return alocation_matrix_inv * input;
}