#include <gtest/gtest.h>
#include <iostream>
#include "scar/trajectory/TrajectorySpherical.hpp"

using namespace ::scar::trajectory;
using namespace std;


Eigen::Matrix3d getAndDisplay(TrajectorySpherical &planner, Eigen::Vector3d &lin_pos, Eigen::Vector3d &lin_vel, Eigen::Vector3d &lin_acc, double time)
{
    planner.get(lin_pos, lin_vel, lin_acc, time);
    Eigen::Matrix3d print;
    print.col(0) = lin_pos;
    print.col(1) = lin_vel;
    print.col(2) = lin_acc;
    // std::cout << print << std::endl;
    // std::cout << "--" << std::endl;
    return print;
}
/**
 * @brief GTest to validate an a priori known behavior
 * Robot position initialized at 1 0 0
 * Target position initialized at 1 1 0
 * Virtual target position is generated in front of the robot, (robot_pos - target_pos) apart from it.
 * Virtual target position initialized at 1 -1 0
 */
TEST(TestTrajectoryPlanner, test_robot_system_coordinates)
{

    Eigen::Vector3d robot_pos, target_pos, lin_pos, lin_vel, lin_acc;
    Eigen::Matrix3d robot_rot;
    Eigen::MatrixXd expected_pos, error_pos_vel_acc;
    double tf;

    robot_pos << 1, 0, 0;
    robot_rot << 0, 1, 0,
                -1, 0, 0,
                 0, 0, 1;

    target_pos << 1, 1, 0;

    TrajectorySpherical planner(robot_pos, robot_rot, target_pos, 1, 1, 5);
    tf = planner.getFinalTime();

    // Test robot initialization
    expected_pos.resize(3, 3);
    expected_pos.setZero();
    error_pos_vel_acc.resize(3, 3);
    error_pos_vel_acc.setZero();
    // Expected position, velocity and acceleration
    expected_pos << 1, 0, 0,
                    -1, 0, 0,
                    0, 0, 0;

    error_pos_vel_acc = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, 0) - expected_pos;
    EXPECT_LT(error_pos_vel_acc.norm(), 1e-10) << "Initial position error: " << error_pos_vel_acc  << std::endl;

    // Test position in the middle of operation
    expected_pos.resize(3, 1);
    expected_pos.setZero();
    error_pos_vel_acc.resize(3, 1);
    error_pos_vel_acc.setZero();
    // Expected position
    expected_pos << 2,
                    0,
                    0;

    error_pos_vel_acc = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf/2).col(0) - expected_pos;
    EXPECT_LT(error_pos_vel_acc.norm(), 1e-10) << "Intermediate position error : " << error_pos_vel_acc  << std::endl;

    // Test robot final position
    expected_pos.resize(3, 3);
    expected_pos.setZero();
    error_pos_vel_acc.resize(3, 3);
    error_pos_vel_acc.setZero();
    // Expected position, velocity and acceleration
    expected_pos << 1, 0, 0,
                    1, 0, 0,
                    0, 0, 0;

    error_pos_vel_acc = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf) - expected_pos;
    EXPECT_LT(error_pos_vel_acc.norm(), 1e-10) << "Final position error: " << error_pos_vel_acc << std::endl;
}

/**
 * @brief GTest to validade a TrajectorySpherical with random initial and target position
 * and then use reconfigure method
 */
TEST(TestTrajectoryPlanner, test_random_spherical_point)
{
    Eigen::Vector3d robot_pos, target_pos, new_target_pos, lin_pos, lin_vel, lin_acc;
    Eigen::Matrix3d robot_rot, error_pos_vel_acc;
    double tf;

    // Create random initial target position
    robot_pos = Eigen::Vector3d::Random()*100;
    target_pos = Eigen::Vector3d::Random()*100;
    robot_rot.setIdentity();

    TrajectorySpherical planner(robot_pos, robot_rot, target_pos, 1, 1, 5);

    tf = planner.getFinalTime();

    error_pos_vel_acc.col(0) = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf).col(0) - target_pos;
    error_pos_vel_acc.col(1) = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf).col(1);
    error_pos_vel_acc.col(2) = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf).col(2);
    EXPECT_LT(error_pos_vel_acc.norm(), 1e-10) << "Final position error for random target position: " << error_pos_vel_acc << std::endl;
}

TEST(TestTrajectoryPlanner, test_reconfigure)
{
    Eigen::Vector3d robot_pos, target_pos, new_target_pos;
    Eigen::Matrix3d robot_rot, error_pos_vel_acc;
    Eigen::Vector3d lin_pos;
    Eigen::Vector3d lin_vel;
    Eigen::Vector3d lin_acc;
    double tf;

    // Create random initial and target position
    robot_pos = Eigen::Vector3d::Random()*100;
    target_pos = Eigen::Vector3d::Random()*100;
    robot_rot.setIdentity();

    TrajectorySpherical planner(robot_pos, robot_rot, target_pos, 1, 1, 5);
    tf = planner.getFinalTime();

    // Reconfigure
    new_target_pos << 10,1, 10;
    planner.reconfigure(new_target_pos);
    tf = planner.getFinalTime();

    error_pos_vel_acc.col(0) = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf).col(0) - new_target_pos;
    error_pos_vel_acc.col(1) = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf).col(1);
    error_pos_vel_acc.col(2) = getAndDisplay(planner, lin_pos, lin_vel, lin_acc, tf).col(2);

    EXPECT_LT(error_pos_vel_acc.norm(), 1e-10) << "Final position error for new target position: " << error_pos_vel_acc << std::endl;
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
