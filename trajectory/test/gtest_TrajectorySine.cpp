#include <gtest/gtest.h>
#include "scar/trajectory/TrajectorySine.hpp"


using namespace ::scar::trajectory;

scar::types::Waypoint generateSineThirdOrder(const Eigen::MatrixXd& config, const scar::types::Time& ts)
{
    Eigen::Index dim = config.rows();
    Eigen::MatrixXd output(dim, 4);
    Eigen::ArrayXd amp, freq, phase, offset;
    amp    = config.col(0);
    freq   = config.col(1);
    phase  = config.col(2);
    offset = config.col(3);
    double time = ts.toSeconds();

    output.col(0) =   amp * Eigen::pow(freq, 0) * Eigen::sin(freq * time + phase) + offset;
    output.col(1) =   amp * Eigen::pow(freq, 1) * Eigen::cos(freq * time + phase);
    output.col(2) = - amp * Eigen::pow(freq, 2) * Eigen::sin(freq * time + phase);
    output.col(3) = - amp * Eigen::pow(freq, 3) * Eigen::cos(freq * time + phase);

    return scar::types::Waypoint(output, ts);
}

scar::types::Waypoint generateSineSecondOrder(const Eigen::MatrixXd& config, const scar::types::Time& ts)
{
    Eigen::Index dim = config.rows();
    Eigen::MatrixXd output(dim, 3);
    Eigen::ArrayXd amp, freq, phase, offset;
    amp    = config.col(0);
    freq   = config.col(1);
    phase  = config.col(2);
    offset = config.col(3);
    double time = ts.toSeconds();

    output.col(0) =   amp * Eigen::pow(freq, 0) * Eigen::sin(freq * time + phase) + offset;
    output.col(1) =   amp * Eigen::pow(freq, 1) * Eigen::cos(freq * time + phase);
    output.col(2) = - amp * Eigen::pow(freq, 2) * Eigen::sin(freq * time + phase);

    return scar::types::Waypoint(output, ts);
}

void compareWaypoint(
    TrajectorySine& traj,
    const scar::types::Time& time,
    const Eigen::MatrixXd& config,
    scar::types::Waypoint (*fun) (const Eigen::MatrixXd&, const scar::types::Time&)
)
{
    double error = (traj.get(time).getData() - fun(config, time).getData()).norm();
	EXPECT_LE(error, 1e-10) << "Maximum error is: " << error;
	EXPECT_EQ(traj.get(time).getTime().toSeconds(), fun(config, time).getTime().toSeconds());
}

static scar::types::Time
    t1 = scar::types::Time::fromSeconds(0),
    t2 = scar::types::Time::fromSeconds(0.5),
    t3 = scar::types::Time::fromSeconds(1),
    t4 = scar::types::Time::fromSeconds(2);

TEST(TrajectorySineClass, sine_value_dim4)
{
	Eigen::MatrixXd config(4,4);
    config << 0.5, M_PI/4, M_PI/2, 2,
			2, M_PI/2, M_PI/6, 1,
            1, 0, M_PI/3, 0,
			0, 1, M_PI/2, 1;

	uint order = 3;
	TrajectorySine traj_sine(config, order);

    compareWaypoint(traj_sine, t1, config, &generateSineThirdOrder);
    compareWaypoint(traj_sine, t2, config, &generateSineThirdOrder);
    compareWaypoint(traj_sine, t3, config, &generateSineThirdOrder);
    compareWaypoint(traj_sine, t4, config, &generateSineThirdOrder);
}

TEST(TrajectorySineClass, sine_value_dim6_all_ones)
{
    // Configuration from a 6 dimension trajectory with 2 derivatives
	Eigen::MatrixXd config(6,4);
	config.setOnes();

	uint order = 2;
	TrajectorySine traj_sine(config, order);

    compareWaypoint(traj_sine, t1, config, &generateSineSecondOrder);
    compareWaypoint(traj_sine, t2, config, &generateSineSecondOrder);
    compareWaypoint(traj_sine, t3, config, &generateSineSecondOrder);
    compareWaypoint(traj_sine, t4, config, &generateSineSecondOrder);
}

TEST(TrajectorySineClass, sine_value_dim6)
{
    Eigen::MatrixXd config(6,4);
    config <<    0.5,    0,   0,  0.5,
                 0.5,  0.5,   0,    2,
                   1,    1, 0.5, 0.25,
                   0, M_PI,   0,    0,
              M_PI/4,    0,   1,    0,
                   0,    1,   0,    0;

    int order = 2;
    TrajectorySine traj_sine(config, order);

	compareWaypoint(traj_sine, t1, config, &generateSineSecondOrder);
    compareWaypoint(traj_sine, t2, config, &generateSineSecondOrder);
    compareWaypoint(traj_sine, t3, config, &generateSineSecondOrder);
    compareWaypoint(traj_sine, t4, config, &generateSineSecondOrder);
}


int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
