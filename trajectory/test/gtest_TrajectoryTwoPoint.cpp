#include <gtest/gtest.h>
#include <iostream>
#include "scar/trajectory/TrajectoryTwoPoint.hpp"

using namespace ::scar::trajectory;
using namespace std;

/**
 * GTest to validate the matrix constructor using a loop to test different target dimensions
 * Test is successfull if the difference between target and result is less then 1e-10
 * Initial position is always equal to 0
 */
TEST(TestTwoPointClass, test_matrix_target)
{
    scar::types::Time final_time;

    for (int dimension = 1; dimension < 6; ++dimension)
    {
        // Create references
        scar::types::Waypoint::Data target(dimension, 3);
        scar::types::Waypoint::Data vec(dimension, 3);
        vec.setZero();
        target = 1 * scar::types::Waypoint::Data::Constant(dimension, 3, dimension);

        // Create Waypoint
        scar::types::Time ts_initial = scar::types::Time::fromSeconds(0);
        scar::types::Time ts_target = scar::types::Time::fromSeconds(10);
        scar::types::Waypoint waypoint_initial(vec, ts_initial);
        scar::types::Waypoint waypoint_target(target, ts_target);
        scar::trajectory::TrajectoryWaypoints::Waypoints waypoints(2);
        waypoints[0] = waypoint_initial;
        waypoints[1] = waypoint_target;
        scar::types::Waypoint::Data v_min(dimension, 1);
        v_min.setZero();
        scar::types::Waypoint::Data v_max(dimension, 1);
        v_max =scar::types::Waypoint::Data::Constant(dimension, 1, 10);

        // Initialize trajectory two points
        TrajectoryTwoPoint traj_matrix(waypoints, v_max, false);
        final_time = traj_matrix.getFinalTime();

        // Create waypoints in the  beginning, middle and end of trajectory
        scar::types::Waypoint::Data initial_time_waypoint = traj_matrix.get(ts_initial).getData();
        scar::types::Waypoint::Data mid_time_waypoint = traj_matrix.get(final_time/2).getData();
        scar::types::Waypoint::Data final_time_waypoint = traj_matrix.get(final_time).getData();

        EXPECT_TRUE((final_time_waypoint - mid_time_waypoint).norm() != 0);
        EXPECT_TRUE((mid_time_waypoint - initial_time_waypoint).norm() != 0);
        EXPECT_TRUE((final_time_waypoint - initial_time_waypoint).norm() != 0);

        // Check if it got to target
        EXPECT_LT((final_time_waypoint - target).norm(), 1e-10);
    }
}

TEST(TestTwoPointClass, test_compute_final_time_method)
{
    scar::types::Waypoint::Data data1(1,3);
    scar::types::Time ts1 = scar::types::Time::fromSeconds(1);
    data1 << 1, 0, 0;
    scar::types::Waypoint::Data data2(1,3);
    scar::types::Time ts2 = scar::types::Time::fromSeconds(3);
    data2 << 3, 0, 0;

    scar::types::Waypoint::Data v_min(1,1);
    v_min << 0;

    scar::types::Waypoint::Data v_max(1,1);
    v_max << 100;

    scar::types::Waypoint way1(data1, ts1);
    scar::types::Waypoint way2(data2, ts2);
    scar::trajectory::TrajectoryWaypoints::Waypoints ways = {way1, way2};

    scar::trajectory::TrajectoryTwoPoint two_point(ways, v_max);

    EXPECT_TRUE(two_point.getFinalTime() == ts2);
    EXPECT_TRUE(two_point.getInitialTime() == ts1);
    EXPECT_TRUE(two_point.getDuration() == (ts2 - ts1));
}

TEST(TestTwoPointClass, test_vel_saturation)
{
    scar::types::Waypoint::Data data1(1,3);
    scar::types::Time ts1 = scar::types::Time::fromSeconds(1);
    data1 << 1, 0, 0;
    scar::types::Waypoint::Data data2(1,3);
    scar::types::Time ts2 = scar::types::Time::fromSeconds(3);
    data2 << 3, 3, 3;

    scar::types::Waypoint::Data v_min(1,1);
    v_min << 0;

    scar::types::Waypoint::Data v_max(1,1);
    v_max << 2;

    scar::types::Waypoint way1(data1, ts1);
    scar::types::Waypoint way2(data2, ts2);
    scar::trajectory::TrajectoryWaypoints::Waypoints ways = {way1, way2};

    scar::trajectory::TrajectoryTwoPoint two_point(ways, v_max);
    EXPECT_TRUE(two_point.getFinalTime() != ts2);
    EXPECT_TRUE(two_point.getInitialTime() == ts1);
    EXPECT_TRUE(two_point.getDuration() != (ts2 - ts1));
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
