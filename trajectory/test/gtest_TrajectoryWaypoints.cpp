#include <gtest/gtest.h>
#include "scar/trajectory/TrajectoryWaypoints.hpp"


using namespace ::scar;
using namespace ::scar::trajectory;


class TrajectoryTest : public TrajectoryWaypoints
{
public:
    TrajectoryTest() : TrajectoryWaypoints() {}
    TrajectoryTest(const Waypoints& waypoints, bool sort) : TrajectoryWaypoints(waypoints, sort) {}
    TrajectoryTest(const Waypoints& waypoints, const types::Waypoint::Data& min, const types::Waypoint::Data& max, bool sort)
        : TrajectoryWaypoints(waypoints, min, max, sort) {}

    types::Waypoint get(types::Time) override
    {
        return getWaypoints().back();
    }
};


TEST(TestTrajectory, test_constructors)
{
    auto data = Eigen::MatrixXd::Identity(3,3);
    auto data_min  = Eigen::MatrixXd::Zero(3,3);
    auto data_max  = Eigen::MatrixXd::Ones(3,3);
    auto data_to_saturate = 2*Eigen::MatrixXd::Ones(3,3);
    types::Time time = types::Time::fromSeconds(1);
    auto wp_a = types::Waypoint(data,   time);
    auto wp_b = types::Waypoint(data, 2*time);
    auto wp_c = types::Waypoint(data, 3*time);
    auto wp_d = types::Waypoint(data, 4*time);

    auto wp_a_sat = types::Waypoint(data_to_saturate, time);
    auto wp_b_sat = types::Waypoint(data_to_saturate, 2*time);
    auto wp_c_sat = types::Waypoint(data_to_saturate, 3*time);
    auto wp_d_sat = types::Waypoint(data_to_saturate, 4*time);

    TrajectoryWaypoints::Waypoints vec_wp_reorder( {wp_a, wp_b, wp_d, wp_c} );
    TrajectoryWaypoints::Waypoints vec_wp_ordered( {wp_a, wp_b, wp_c, wp_d} );
    TrajectoryWaypoints::Waypoints vec_wp_null( {wp_a, wp_b, wp_c, wp_d, types::Waypoint()} );
    TrajectoryWaypoints::Waypoints vec_wp_sat( {wp_a_sat, wp_b_sat, wp_c_sat, wp_d_sat} );

    TrajectoryTest traj;
    EXPECT_TRUE(traj.isNull());
    traj = TrajectoryTest(vec_wp_reorder, false);
    EXPECT_TRUE(traj.isNull());
    traj = TrajectoryTest(vec_wp_reorder, data_min, data_max, false);
    EXPECT_TRUE(traj.isNull());
    traj = TrajectoryTest(vec_wp_null, data_min, data_max, false);
    EXPECT_TRUE(traj.isNull());
    traj = TrajectoryTest(vec_wp_null, data_min, data_max, true);
    EXPECT_TRUE(traj.isNull());

    traj = TrajectoryTest(vec_wp_reorder, true);
    EXPECT_FALSE(traj.isNull());
    traj = TrajectoryTest(vec_wp_reorder, data_min, data_max, true);
    EXPECT_FALSE(traj.isNull());
    traj = TrajectoryTest(vec_wp_ordered, data_min, data_max, false);
    EXPECT_FALSE(traj.isNull());

    // Test saturated waypoints
    traj = TrajectoryTest(vec_wp_sat, data_min, data_max, true);
    for (auto v : traj.getWaypoints())
        EXPECT_TRUE(data_max == v.getData());

    traj = TrajectoryTest(vec_wp_reorder, true);
    EXPECT_TRUE(std::equal(vec_wp_ordered.begin(), vec_wp_ordered.end(), traj.getWaypoints().begin(),
        [] (types::Waypoint a, types::Waypoint b) {return a.getTime() == b.getTime();}
    ));
    EXPECT_FALSE(std::equal(vec_wp_reorder.begin(), vec_wp_reorder.end(), traj.getWaypoints().begin(),
        [] (types::Waypoint a, types::Waypoint b) {return a.getTime() == b.getTime();}
    ));
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
