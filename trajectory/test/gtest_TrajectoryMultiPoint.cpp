#include "scar/trajectory/TrajectoryMultiPoint.hpp"
#include <gtest/gtest.h>
#include <iostream>

using namespace ::scar::trajectory;
using namespace std;

/**
 * @brief generateWaypointVector creates a matrix vector and vector[i] is a matrix of size row x col and values = i + init_value
 *
 * @param size Vector size
 * @param row Matrix row number
 * @param col Matrix column number
 * @return TrajectoryMultiPoint::WaypointVector
 */
TrajectoryWaypoints::Waypoints generateWaypointVector(unsigned long size, int row, int col, long init_value = 0)
{
    TrajectoryWaypoints::Waypoints trajectory_values(size);
    scar::types::Waypoint::Data mat(row, col);
    for(long i = init_value; (i - init_value) < long(size); ++i)
    {
        trajectory_values[size_t(i - init_value)] = scar::types::Waypoint(i*mat.setOnes(), scar::types::Time::fromSeconds(5*i));
    }
       return trajectory_values;
}

class TrajectoryMultiPointTest : public ::testing::Test
{
protected:
    TrajectoryMultiPoint *myMultiPointTrajectory;
    unsigned long size = 3;
    int row = 3;
    int col = 3;
    int max_limit = 4;
    int min_limit = 0;
    bool sort = false;
    scar::types::Waypoint::Data v_max, v_min;

    virtual void SetUp()
    {
        v_max.resize(row,1);
        v_max = scar::types::Waypoint::Data::Constant(row, 1, max_limit);
        myMultiPointTrajectory = new TrajectoryMultiPoint(generateWaypointVector(size, row, col, 0), v_max, sort);
    }

    virtual void TearDown()
    {
        delete myMultiPointTrajectory;
    }
};

TEST_F(TrajectoryMultiPointTest, initial_waypoint_test)
{
    double error = 0;
    scar::types::Time ts;
    TrajectoryWaypoints::Waypoints expected_result = generateWaypointVector(size - 1, row, col, 0);
    error = (expected_result[0].getData() - myMultiPointTrajectory->get(expected_result[0].getTime()).getData()).norm();
    EXPECT_TRUE(error < 1e-10) << "Maximum error is: " << error;
}

TEST_F(TrajectoryMultiPointTest, final_waypoint_test)
{
    double error = 0;
    scar::types::Time ts;
    TrajectoryWaypoints::Waypoints expected_result = generateWaypointVector(size, row, col, 0);
    error = (expected_result[size-1].getData() - myMultiPointTrajectory->get(expected_result[size-1].getTime()).getData()).norm();
    error += (expected_result[size-1].getData() - myMultiPointTrajectory->get(myMultiPointTrajectory->getFinalTime()).getData()).norm();
    EXPECT_TRUE(error < 1e-10) << "Maximum error is: " << error;
}

TEST_F(TrajectoryMultiPointTest, get_test)
{
    double error = 0;
    scar::types::Time ts;
    TrajectoryWaypoints::Waypoints expected_result = generateWaypointVector(size, row, col, 0);
    for(size_t i = 0; i < size; ++ i)
    {
        error += (expected_result[i].getData() - myMultiPointTrajectory->get(expected_result[i].getTime()).getData()).norm();
    }
    EXPECT_TRUE(error < 1e-10) << "Maximum error is: " << error;
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
