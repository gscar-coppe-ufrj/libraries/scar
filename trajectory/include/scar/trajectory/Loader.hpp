/**
 * @file Loader.hpp
 * @author LEAD
 * @brief Defines an auxiliary class to load a data file and convert it to Eigen array matrix
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include <Eigen/Dense>

#include <vector>


namespace scar
{
namespace trajectory
{


/**
 * @brief Loads a data file and writes its content to an Eigen array matrix. It is preferable to
 * provide data files written row-wise.
 */
class Loader
{
public:
    const static Eigen::Index LIMIT_DISABLED = -1;

private:
    const static long RESERVED_SIZE = 1000;

    // TODO(jcmonteiro) adapt this method to efficiently load data files that are written column-wise
    void load(std::string filename, std::vector<int> indices, Eigen::Index n_rows, std::array<Eigen::Index, 2> window);

public:
    Eigen::ArrayXXd data;

    Loader(std::string filename, std::vector<int> indices);
    Loader(std::vector<std::string> filenames, std::vector<int> indices);
    Loader(std::string filename, std::vector<int> indices, std::array<Eigen::Index, 2> window);
    Loader(std::vector<std::string> filenames, std::vector<int> indices, std::array<Eigen::Index, 2> window);
};


}
}
