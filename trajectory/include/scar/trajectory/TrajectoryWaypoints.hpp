/**
 * @file TrajectoryWaypoints.hpp
 * @author LEAD
 * @brief Defines a base class to create trajectory using more than 2 waypoints.
 * This class implements functions to sort, apply constraints to the waypoints and retrieve information
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "scar/trajectory/TrajectoryBase.hpp"


namespace scar
{
namespace trajectory
{


class TrajectoryWaypoints: public TrajectoryBase
{
public:
    typedef std::vector<types::Waypoint> Waypoints;

private:
    bool is_null;

protected:
    // Protected vec_waypoints to reconfigure if necessary. Ex.: When initializing TrajectoryTwoPoint
    Waypoints vec_waypoints;
    /**
     * @brief initialize max values when dimensions is in vector form
     *
     * @param value Default value
     * @param rows Number of rows
     * @param cols Number of cols
     * @param mat Matrix to insert
     * @param i Row to insert the matrix
     * @param j Column to insert the matrix
     * @return A matrix ...
     */
    Eigen::MatrixXd initialize(double value, long rows, long cols, const Eigen::MatrixXd& mat, int i, int j);

    /**
     * @brief Adjust the times for each waypoint
     */
    void adjustTimes(const std::vector<types::Time>& times);

    /**
     * @brief Method to initializes vec_waypoint apllying min and max constraints, but considering that max is a vector
     * OBS.: Method developed for TrajectoryTwoPoint class
     * @param waypoints
     * @param max
     * @param sort
     */
    void reconfigureVectorialMax(const Waypoints& waypoints, const types::Waypoint::Data& max, bool sort);

public:
    /** Null constructor */
    TrajectoryWaypoints();

    /** Default constructor when no min and max is given */
    TrajectoryWaypoints(const Waypoints& waypoints, bool sort = false);

    /** Default constructor
     * Conforms waypoints to fit between min and max
     * min and max are always Matrices
     */
    TrajectoryWaypoints(const Waypoints& waypoints, const types::Waypoint::Data& min, const types::Waypoint::Data& max, bool sort = false);

    /** Virtual desctructor because of virtual methods */
    virtual ~TrajectoryWaypoints();

    /** True if the type has not been properly initialized */
    virtual bool isNull() const;

    /** Default conversion to string */
    virtual operator std::string() const;

    /**
     * @brief Returns the trajectory value at time \p ts
     * @param ts Time on which to evaluate the trajectory
     * @return TrajectoryWaypoints at time \p ts
     * Return a const reference is hard because the Waypoint at time ts has to be created in this method
     */
    virtual types::Waypoint get(types::Time ts) = 0;

    /**
     * @brief Returns the waypoints that compose this trajectory
     * @return The waypoints
     */
    const Waypoints& getWaypoints() const;

    /**
     * @return The total duration of this trajectory
     * @see #getInitialTime, #getFinalTime
     */
    types::Time getDuration() const;

    /**
     * @return The initial time of this trajectory
     * @see #getFinalTime, #getDuration
     */
    const types::Time& getInitialTime() const;

    /**
     * @return The final time of this trajectory
     * virtual because of a different implementation in TwoPointTrajectory class
     * @see #getInitialTime, #getDuration
     */
    virtual const types::Time& getFinalTime() const;
};


}
}
