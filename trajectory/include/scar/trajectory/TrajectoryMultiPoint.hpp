/**
 * @file TrajectoryMultiPoint.hpp
 * @author LEAD
 * @brief Defines a trajecotry with multipoints.
 * Each element from a vector is a trajectory, and the get functions return the value from the correspondent element.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "scar/trajectory/TrajectoryTwoPoint.hpp"
#include <iostream>


namespace scar
{
namespace trajectory
{

class TrajectoryMultiPoint : public TrajectoryWaypoints
{
public:
    typedef std::vector<TrajectoryTwoPoint>  TrajectoryVector;

private:
    TrajectoryVector vec_trajectory;

public:
    /**
     * @brief Construct a new TrajectoryWaypoints Multi Point:: TrajectoryWaypoints Multi Point object
     * Null initialization
     *
     */
    TrajectoryMultiPoint();

    /**
     * @brief Construct a new TrajectoryWaypoints Multi Point:: TrajectoryWaypoints Multi Point object
     * Initializes a trajectory vector with information from swaypoints limiting velocity to v_max. Each trajectory is an instance from TrajectoryTwoPoint and there will be waypoints.size() - 1 elements in trajectory_vec
     *
     * @param waypoints
     * @param v_max
     * @param delta_time_vec Vector of time intervals
     */
    TrajectoryMultiPoint(Waypoints _vec_waypoint, const types::Waypoint::Data& v_max, bool sort);

    /**
     * @brief Method to obtain the final time vector for each trajectory
     * @return const types::Time&
     */
    const types::Time& getFinalTime() const override;

    types::Waypoint get(types::Time ts) override;
};

}
}
