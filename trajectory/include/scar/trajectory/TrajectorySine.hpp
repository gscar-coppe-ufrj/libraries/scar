/**
 * @file TrajectorySine.hpp
 * @author LEAD
 * @brief Defines a sinusoidal trajectory.
 * \f$ x = A * sin(\omega * t + \theta) + \phi \f$
 *
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "scar/trajectory/TrajectoryBase.hpp"
#include <cmath>
#include <Eigen/Eigen>


namespace scar
{
namespace trajectory
{

class TrajectorySine: public TrajectoryBase
{
private:
    /**
     * @brief cols represents the number of derivatives
     */
    Eigen::Index cols;
    Eigen::Index rows;

    Eigen::ArrayXXd frequency, amp_times_freq_pow, phase, offset;

public:
    /**
     * @brief Construct a new Trajectory Sine object
     * @param config each column corresponds to a sine parameters: amplitude, frequency (rad/s), phase (rad), and offset; in this order
     * @param order number of derivatives
     */
    TrajectorySine(const Eigen::MatrixXd& config, Eigen::Index order);
    ~TrajectorySine();
    types::Waypoint get(types::Time ts) override;
};


}
}
