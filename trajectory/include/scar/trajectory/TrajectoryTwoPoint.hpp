/**
 * @file TrajectoryTwoPoint.hpp
 * @author LEAD
 * @brief Class to create a trajectory going from waypoint A to waypoint B.
 * This class and \a TrajectorySine in Trajectory library are the ones that actually create a trajectory, all the other classes only use it.
 * The implementation in \a TrajectoryTwoPoint considers that the final waypoint will have the correspondent derivatives equal to zero.
 * It uses a polynomial equation to define the trajectory.
 * @version 0.1
 * @date 2019-08-12
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include "scar/trajectory/TrajectoryWaypoints.hpp"
#include <Eigen/Eigen>
#include <Eigen/Dense>


namespace scar
{
namespace trajectory
{


class TrajectoryTwoPoint : public TrajectoryWaypoints
{
private:
    types::Time duration;
    long n_polys; // Long because of Eigen
    Eigen::MatrixXd coeffs, linear_system_matrix_A;
    types::Waypoint::Data info_a, info_b;

    void generateMatrixA();
    Eigen::VectorXd solveSystem(const Eigen::VectorXd& info_a, const Eigen::VectorXd& info_b);
    void computeDuration(types::Time t_min, const Eigen::VectorXd& v_max);

    static void evalBeta1(Eigen::Vector4d& beta, types::Time time);
    static void evalBeta2(Eigen::Vector4d& beta, types::Time time);
    static void evalBeta3(Eigen::Vector4d& beta, types::Time time);

    /*!
    * \brief configure Configures the trajectory to go from point A to point B in t_min seconds, starting at t = 0
    * \param t_min is one possible Time to reach point B
    * \return True if configuration was successful
    */
    bool configure(types::Time t_min, const Eigen::VectorXd& v_max);

    /*!
    * \brief setNTrajectories Sets the number of distinct trajectories
    * \param n The number of trajectories
    */
    void setNTrajectories(long n);

    /*!
    * \brief setPointA Sets the position, velocity and acceleration of the initial point
    * \param pos_vel_acc Column 1 contains the positions, column 2 the velocities and column 3 the accelerations
    */
    void setPointA(const types::Waypoint::Data& waypoint_a);

    /*!
    * \brief setPointC Sets the position, velocity and acceleration of the final point
    * \param pos_vel_acc Column 1 contains the positions, column 2 the velocities and column 3 the accelerations
    */
    void setPointB(const types::Waypoint::Data& waypoint_b);

public:
    // Null constructor
    TrajectoryTwoPoint();
    // Initialize using initial and desired Waypoint, minimum time is the difference between time from Waypoints
    TrajectoryTwoPoint(const Waypoints& waypoints, const types::Waypoint::Data& v_max, bool sort = false);

    ~TrajectoryTwoPoint() override;

    /*!
    * \brief get Returns the position, velocity and acceleration at time "time"
    * \param time The time to take the measurement
    * \return A matrix where the column 1 contains the positions, column 2 the velocities and column 3 the accelerations
    */
    types::Waypoint get(types::Time time) override;

};


}
}
