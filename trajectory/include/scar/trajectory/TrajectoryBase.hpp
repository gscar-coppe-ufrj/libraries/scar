/**
 * @file TrajectoryBase.hpp
 * @author LEAD
 * @brief Defines a base class for trajectories.
 * This base class implements only a virtual method to get Waypoints in a given time.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "scar/types/Waypoint.hpp"


namespace scar
{
namespace trajectory
{

// Pure virtual class
class TrajectoryBase
{
public:
    /** Default empty constructor */
    TrajectoryBase();

    /** Default empty desctructor */
    virtual ~TrajectoryBase();

    /**
     * @brief Returns the trajectory value at time \p ts
     * @param ts Time on which to evaluate the trajectory
     * @return TrajectoryBase at time \p ts
     * Return a const reference is hard because the Waypoint at time ts has to be created in this method
     */
    virtual types::Waypoint get(types::Time ts) = 0;
};


}
}
