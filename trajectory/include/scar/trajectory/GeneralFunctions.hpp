/**
 * @file GeneralFunctions.hpp
 * @author LEAD
 * @brief Defines and declares functions that may be useful to generate trajectories.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include <Eigen/Eigen>


namespace Eigen
{

typedef Matrix<double, 6, 6> Matrix6d;
typedef Matrix<double, 6, 1> Vector6d;

}


namespace scar
{
namespace trajectory
{


/*!
 * \brief rot_x returns the elementary rotation matrix corresponding to a rotation of a frame around the x axis.
 *
 * \f[
 * R_{x}(\theta) = \left[\begin{array}{ccc}
 * 1 & 0 & 0 \\
 * 0 & c(\theta) & -s(\theta) \\
 * 0 & s(\theta) & c(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle around the x axis
 * \return Rotation matrix
 */
Eigen::Matrix3d rot_x(double ang);

/*!
 * \brief rot_y returns the elementary rotation matrix corresponding to a rotation of a frame around the y axis.
 *
 * \f[
 * R_{y}(\theta) = \left[\begin{array}{ccc}
 * c(\theta) & 0 & s(\theta) \\
 * 0 & 1 & 0 \\
 * -s(\theta) & 0 & c(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle around the y axis
 * \return Rotation matrix
 */
Eigen::Matrix3d rot_y(double ang);

/*!
 * \brief rot_z returns the elementary rotation matrix corresponding to a rotation of a frame around the z axis.
 *
 * \f[
 * R_{z}(\theta) = \left[\begin{array}{ccc}
     * c(\theta) & -s(\theta) & 0 \\
     * s(\theta) & c(\theta) & 0 \\
     * 0 & 0 & 1
     * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle around the x axis
 * \return Rotation matrix
 */
Eigen::Matrix3d rot_z(double ang);

/*!
 * \brief rotAngleAxis returns the rotation matrix corresponding to a rotation of an angle \f$ \theta \f$ over an axis \f$ \vec{h} \f$.
 *
 * \f[
 * R(\theta,h) = I + \sin(\theta)\hat{h} + (1-\cos(\theta))\hat{h}^2
 * \f]
 *
 * \param ang Rotation angle around the h axis
 * \param h Axis of rotation
 * \return Rotation matrix
 */
template<typename Derived>
Eigen::Matrix3d rotAngleAxis(double ang, const Eigen::MatrixBase<Derived> & h)
{
    double c = cos(ang);
    double s = sin(ang);
    double v = 1 - c;
    Eigen::Matrix3d ret;
    ret << h(0)*h(0)*v + c, h(0)*h(1)*v - h(2)*s, h(0)*h(2)*v + h(1)*s,
            h(0)*h(1)*v + h(2)*s, h(1)*h(1)*v + c, h(1)*h(2)*v - h(0)*s,
            h(0)*h(2)*v - h(1)*s, h(1)*h(2)*v + h(0)*s, h(2)*h(2)*v + c;
    return ret;
}

/*!
 * \brief Calculates the skew-symmetric matrix of a vector
 *
 * Considering a vector \f$ v \in \mathbb{R}^3 \f$, the skew-symmetric matrix \f$ \hat{v} \in \mathbb{R}^{3 \times 3} \f$ can be written with the elements of \f$ v \f$:
 *
 * \f[
 * v = \left[\begin{array}{ccc} x \\ y \\ z \end{array}\right] \qquad
 * \hat{v} = \left[\begin{array}{ccc}
 * 0 & -z & y \\
 * z & 0 & -x \\
 * -y & x & 0
 * \end{array}\right]
 * \f]
 *
 * \param vec Vector
 * \return Skew-symmetric matrix
 */
template<typename Derived>
Eigen::Matrix3d hat(const Eigen::MatrixBase<Derived> & vec)
{
    Eigen::Matrix3d ret;
    ret << 0, -vec(2), vec(1),
            vec(2), 0, -vec(0),
            -vec(1), vec(0), 0;
    return ret;
}


/*!
 * \brief Calculates the skew-symmetric matrix of a vector
 *
 * Considering a vector \f$ v \in \mathbb{R}^3 \f$, the skew-symmetric matrix \f$ \hat{v} \in \mathbb{R}^{3 \times 3} \f$ can be written with the elements of \f$ v \f$:
 *
 * \f[
 * v = \left[\begin{array}{ccc} x \\ y \\ z \end{array}\right] \qquad
 * \hat{v} = \left[\begin{array}{ccc}
 * 0 & -z & y \\
 * z & 0 & -x \\
 * -y & x & 0
 * \end{array}\right]
 * \f]
 *
 * \param hatvec Skew-symmetric matrix of a Vector
 * \return Vector
 */
template<typename Derived>
Eigen::Matrix3d unhat(const Eigen::MatrixBase<Derived> & hatvec)
{
    Eigen::Vector3d ret;
    ret << hatvec(2,1), hatvec(0,2), hatvec(1,0);
    return ret;
}

/*!
 * \brief Calculates the partial derivative of the elementary rotation matrix of a rotation on an x axis
 *
 * Considering \f$ R_x \in \mathbb{R}^{3 \times 3} \f$ as a rotation around the x axis by the angle \f$ \theta \in \mathbb{R} \f$, the partial derivative of this matrix relative to \f$ \theta \f$ is:
 *
 * \f[
 * \frac{\partial R_{x}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * 0 & 0 & 0 \\
 * 0 & -s(\theta) & -c(\theta) \\
 * 0 & c(\theta) & -s(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle
 * \return Partial derivative of rot_x
 */
Eigen::Matrix3d delRot_x(double ang);

/*!
 * \brief Calculates the partial derivative of the elementary rotation matrix of a rotation on a y axis
 *
 * Considering \f$ R_y \in \mathbb{R}^{3 \times 3} \f$ as a rotation around the y axis by the angle \f$ \theta \in \mathbb{R} \f$, the partial derivative of this matrix relative to \f$ \theta \f$ is:
 *
 * \f[
 * \frac{\partial R_{y}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * -s(\theta) & 0 & c(\theta) \\
 * 0 & 0 & 0 \\
 * -c(\theta) & 0 & -s(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle
 * \return Partial derivative of Rot_y
 */
Eigen::Matrix3d delRot_y(double ang);

/*!
 * \brief Calculates the partial derivative of the elementary rotation matrix of a rotation on a z axis
 *
 * Considering \f$ R_z \in \mathbb{R}^{3 \times 3} \f$ as a rotation around the z axis by the angle \f$ \theta \in \mathbb{R} \f$, the partial derivative of this matrix relative to \f$ \theta \f$ is:
 *
 * \f[
 * \frac{\partial R_{z}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * -s(\theta) & -c(\theta) & 0 \\
 * c(\theta) & -s(\theta) & 0 \\
 * 0 & 0 & 0
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle
 * \return Partial derivative of Rot_z
 */
Eigen::Matrix3d delRot_z(double ang);

/*!
 * \brief Wraps the angle to the (-pi,pi] interval
 *
 * \param ang angle to be wrapped
 */
void wrap2Pi(double & ang);

/*!
 * \brief Wraps the angles to the (-pi,pi] interval
 *
 * \param ang vector of angles to be wrapped
 */
void wrap2Pi(Eigen::VectorXd & ang);

unsigned int nChooseK(unsigned int N, unsigned int K);

template<typename Derived>
unsigned int polynomialDegree(const Eigen::MatrixBase<Derived> & P)
{
    int ret = 0;
    for (unsigned int i = 0; i < P.size(); i++)
    {
        if (fabs(P(i)) > 1e-8)
        {
            ret = P.size() - i - 1;
            break;
        }
    }
    return ret;
}

template<typename Derived>
Eigen::VectorXd vectorShiftLeft(const Eigen::MatrixBase<Derived> & V, int n)
{
    int s = V.size();
    Eigen::VectorXd ret = Eigen::VectorXd::Zero(s);
    ret.head(s-n) = V.tail(s-n);
    return ret;
}

/*!
 * \brief polynomialDivision Performs a polynomial division such that N/D = q/D + r
 *
 * Note that q and r should have the same size as D prior to calling this function.
 *
 * \param N Numerator
 * \param D Denominator
 * \param q Quotient
 * \param r Remainder
 */
template<typename Derived>
void polynomialDivision(const Eigen::MatrixBase<Derived> & N, const Eigen::MatrixBase<Derived> & D, Eigen::MatrixBase<Derived> & q, Eigen::MatrixBase<Derived> & r)
{
    unsigned int s = D.size();

    // These instructions only assert that the new size equals the old size, and do nothing else
    // This is so because N, D are MatrixBase objects
    q.resize(s);
    r.resize(s);

    q.setZero();
    r.setZero();
    unsigned int i = 0;

    Eigen::VectorXd N_aux(s);
    N_aux = N;
    Eigen::VectorXd D_aux(s);
    int degD = polynomialDegree(D);
    int degN_aux = polynomialDegree(N_aux);
    int n = degN_aux - degD;
    double a;

    while (n >= 0)
    {
        D_aux = vectorShiftLeft(D,n);
        a = N_aux(i)/D_aux(i);
        q(s-n-1) = a;
        D_aux *= a;
        N_aux -= D_aux;
        i ++;
        n = polynomialDegree(N_aux) - degD;
    }
    r = N_aux;
}

/*!
 * \brief convertCartesian2Cylindrical Converts from cartesian to cylindrical coordinates
 *
 * x = rho * cos(theta)       rho   = sqrt(x*x + y*y)
 * y = rho * sin(theta)  -->  theta = atan2(y, x)
 * z = z'                     z'    = z
 *
 * \param pos position in cartesian coordiantes
 * \return position in cylindrical coordinates (rho, theta, z)
 */
Eigen::Vector3d convertCartesian2Cylindrical(const Eigen::Vector3d &pos);

/*!
 * \brief convertCartesian2Spherical Converts from cartesian to spherical coordinates
 *
 * x = rho * cos(theta) * cos(psi)       rho   = sqrt(x*x + y*y + z*z)
 * y = rho * sin(theta) * cos(psi)  -->  theta = atan2(y, x)
 * z = rho * sin(psi)                    psi   = asin(z / rho)
 *
 * \param pos position in cartesian coordiantes
 * \return position in spherical coordinates (rho, theta, psi)
 */
Eigen::Vector3d convertCartesian2Spherical(const Eigen::Vector3d &pos);

/*!
 * \brief convertSpherical2Cartesian Converts from spherical to cartesian coordinates
 *
 * x = rho * cos(theta) * cos(psi)       rho   = sqrt(x*x + y*y + z*z)
 * y = rho * sin(theta) * cos(psi)  <--  theta = atan2(y, x)
 * z = rho * sin(psi)                    psi   = asin(z / rho)
 *
 * \param pos position in spherical coordiantes
 * \return position in cartesian coordinates (x, y, z)
 */
Eigen::Vector3d convertSpherical2Cartesian(const Eigen::Vector3d &pos);

/*!
 * \brief convertSpherical2Cartesian_Jacobian Returns the Jacobian matrix that maps spherical coordinates velocities to cartesian space
 * \return the Jacobian matrix
 */
Eigen::Matrix3d convertSpherical2Cartesian_Jacobian(const Eigen::Vector3d &pos);

/*!
 * \brief convertSpherical2Cartesian_dotJacobian Returns the Jacobian matrix derivative w.r.t. time
 * \return the Jacobian matrix derivative
 */
Eigen::Matrix3d convertSpherical2Cartesian_dotJacobian(const Eigen::Vector3d &pos, const Eigen::Vector3d &vel);


}
}
