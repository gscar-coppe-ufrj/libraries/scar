#include "scar/trajectory/TrajectoryWaypoints.hpp"

#include <algorithm>


namespace scar
{
namespace trajectory
{


TrajectoryWaypoints::TrajectoryWaypoints() :
    TrajectoryBase(), is_null(true), vec_waypoints()
{
}

TrajectoryWaypoints::TrajectoryWaypoints(const Waypoints& waypoints, bool sort) :
    TrajectoryWaypoints(waypoints, types::Waypoint::Data(), types::Waypoint::Data(), sort)

{
}

TrajectoryWaypoints::TrajectoryWaypoints(const Waypoints& waypoints, const types::Waypoint::Data& min, const types::Waypoint::Data& max, bool sort) :
    TrajectoryBase(), is_null(waypoints.empty()), vec_waypoints(waypoints)
{
    if (is_null)
    {
        vec_waypoints.clear();
        return;
    }

    if (sort)
        std::sort(vec_waypoints.begin(), vec_waypoints.end(),
            [] (types::Waypoint& wp_a, types::Waypoint& wp_b) { return wp_a.getTime() < wp_b.getTime(); }
        );

    types::Time last_time;
    for (auto& wp : vec_waypoints)
    {
        wp = types::Waypoint(wp.getData(), min, max, wp.getTime());
        if (wp.isNull() || wp.getTime() <= last_time)
        {
            is_null = true;
            vec_waypoints.clear();
            return;
        }
        last_time = wp.getTime();
    }
}

TrajectoryWaypoints::~TrajectoryWaypoints()
{
}

bool TrajectoryWaypoints::isNull() const
{
    return is_null;
}

TrajectoryWaypoints::operator std::string() const
{
    if (vec_waypoints.empty())
    {
        return "trajectory: empty";
    }
    std::string ret = "trajectory:";
    for (const types::Waypoint& wp : vec_waypoints)
        ret += std::string("\n") + std::string(wp);
    return ret;
}

const TrajectoryWaypoints::Waypoints& TrajectoryWaypoints::getWaypoints() const
{
    return vec_waypoints;
}

types::Time TrajectoryWaypoints::getDuration() const
{
    return getFinalTime() - getInitialTime();
}

const types::Time& TrajectoryWaypoints::getInitialTime() const
{
    return vec_waypoints.cbegin()->getTime();
}

const types::Time& TrajectoryWaypoints::getFinalTime() const
{
    return vec_waypoints.crbegin()->getTime();
}

types::Waypoint::Data TrajectoryWaypoints::initialize(double value, long rows, long cols, const Eigen::MatrixXd& mat, int i, int j)
{
    Eigen::MatrixXd new_matrix(rows, cols);
    auto mat_rows = mat.rows();
    auto mat_cols = mat.cols();
    new_matrix = Eigen::MatrixXd::Constant(rows, cols, value);
    new_matrix.block(i, j, mat_rows, mat_cols) << mat;
    return new_matrix;
}

void TrajectoryWaypoints::adjustTimes(const std::vector<types::Time>& times)
{
    if (times.size() != vec_waypoints.size())
        throw std::logic_error("TrajectoryWaypoints::adjustTimes - number of waypoints differ from the time vector size");
    auto iter_wayp = vec_waypoints.begin();
    auto iter_time = times.cbegin();
    while (iter_wayp != vec_waypoints.end())
    {
        *iter_wayp = types::Waypoint(iter_wayp->getData(), *iter_time);
        ++iter_wayp;
        ++iter_time;
    }
}

void TrajectoryWaypoints::reconfigureVectorialMax(const Waypoints& waypoints, const types::Waypoint::Data& max, bool sort)
{
    // Method called to apply max contraints considering that max is a Vector instead of a Matrix
    vec_waypoints = waypoints;
    is_null = false;
    auto rows = vec_waypoints[0].getData().rows();
    auto cols = vec_waypoints[0].getData().cols();
    types::Waypoint::Data new_min(rows, cols);
    new_min.setZero();
    auto new_max = new_min;

    // Converts vector to matrix and sets all other values to INFINITY
    new_max = initialize(double(INFINITY), rows, cols, max, 0, 1);
    // Sort routine if required
    if (sort)
        std::sort(vec_waypoints.begin(), vec_waypoints.end(),
            [] (types::Waypoint& wp_a, types::Waypoint& wp_b) { return wp_a.getTime() < wp_b.getTime(); }
        );

    types::Time last_time;
    for (auto& wp : vec_waypoints)
    {
        // Updates vec_waypoints with wp considering contraints from
        wp = types::Waypoint(wp.getData(), new_min, new_max, wp.getTime());
        if (wp.isNull() || wp.getTime() <= last_time)
        {
            is_null = true;
            vec_waypoints.clear();
            return;
        }
        last_time = wp.getTime();
    }
}

}
}
