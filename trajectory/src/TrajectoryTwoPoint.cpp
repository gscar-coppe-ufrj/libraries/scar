#include "scar/trajectory/TrajectoryTwoPoint.hpp"
#include <iostream>


namespace scar
{
namespace trajectory
{

TrajectoryTwoPoint::TrajectoryTwoPoint()
{

}

TrajectoryTwoPoint::TrajectoryTwoPoint(const TrajectoryWaypoints::Waypoints& waypoints, const types::Waypoint::Data& v_max, bool sort):
    TrajectoryWaypoints()
{
    // Condition v_max size to fit in TrajectoryWaypoints constructor
    this->reconfigureVectorialMax(waypoints, v_max, sort);

    // Check waypoints size
    if (waypoints.size() != 2)
        std::cerr << "[Error] Only 2 waypoints are necessary in TrajectoryTwoPoint class to create a trajectory";

    types::Waypoint initial_waypoint = this->getWaypoints()[0];
    types::Waypoint desired_waypoint = this->getWaypoints()[1];
    types::Time t_min = desired_waypoint.getTime() - initial_waypoint.getTime();

    long nTrajectories = desired_waypoint.getData().rows();

    if (nTrajectories != v_max.size())
        std::cerr << "[Warn] v_max and desired position dimension are different";

    setNTrajectories(nTrajectories);

    setPointA(initial_waypoint.getData());
    setPointB(desired_waypoint.getData());
    configure(t_min, v_max);
    adjustTimes( {initial_waypoint.getTime(), initial_waypoint.getTime() + duration} );
}

TrajectoryTwoPoint::~TrajectoryTwoPoint()
{
}

void TrajectoryTwoPoint::setNTrajectories(long n)
{
    n_polys = n;
}

void TrajectoryTwoPoint::generateMatrixA()
{
    //double t_max = getFinalTime();
    types::Time t0 = types::Time::fromMicroseconds(0);
    types::Time t1 = duration / 3;
    types::Time t2 = 2 * t1;
    types::Time t3 = duration;
    Eigen::Vector4d beta(4);
    beta.setZero(); // this removes an incorrect warning of uninitialized beta

    linear_system_matrix_A.setZero(12, 12);
    //
    evalBeta1(beta, t0);
    linear_system_matrix_A.block<1,4>(0, 0) = beta;
    evalBeta2(beta, t0);
    linear_system_matrix_A.block<1,4>(1, 0) = beta;
    evalBeta3(beta, t0);
    linear_system_matrix_A.block<1,4>(2, 0) = beta;
    //
    evalBeta1(beta, t3);
    linear_system_matrix_A.block<1,4>(3, 8) = beta;
    evalBeta2(beta, t3);
    linear_system_matrix_A.block<1,4>(4, 8) = beta;
    evalBeta3(beta, t3);
    linear_system_matrix_A.block<1,4>(5, 8) = beta;
    //
    evalBeta1(beta, t1);
    linear_system_matrix_A.block<1,4>(6, 0) =  beta;
    linear_system_matrix_A.block<1,4>(6, 4) = -beta;
    evalBeta2(beta, t1);
    linear_system_matrix_A.block<1,4>(7, 0) =  beta;
    linear_system_matrix_A.block<1,4>(7, 4) = -beta;
    evalBeta3(beta, t1);
    linear_system_matrix_A.block<1,4>(8, 0) =  beta;
    linear_system_matrix_A.block<1,4>(8, 4) = -beta;
    //
    evalBeta1(beta, t2);
    linear_system_matrix_A.block<1,4>(9, 4) =  beta;
    linear_system_matrix_A.block<1,4>(9, 8) = -beta;
    evalBeta2(beta, t2);
    linear_system_matrix_A.block<1,4>(10, 4) =  beta;
    linear_system_matrix_A.block<1,4>(10, 8) = -beta;
    evalBeta3(beta, t2);
    linear_system_matrix_A.block<1,4>(11, 4) =  beta;
    linear_system_matrix_A.block<1,4>(11, 8) = -beta;
}

Eigen::VectorXd TrajectoryTwoPoint::solveSystem(const Eigen::VectorXd &data_a, const Eigen::VectorXd &data_b)
{
    Eigen::VectorXd b(12);
    b << data_a, data_b, 0, 0, 0, 0, 0, 0;
    return linear_system_matrix_A.colPivHouseholderQr().solve(b);
}

void TrajectoryTwoPoint::computeDuration(types::Time t_min, const Eigen::VectorXd &v_max)
{
    duration = types::Time::fromMicroseconds(0);
    for (unsigned int k = 0; k < n_polys; ++k)
    {
        duration = (duration > t_min) ? duration : t_min;
        // Expression obtained empirically, that shows a maximum at 2.2* the difference between point A and B when vel and acc start and end at 0.
        auto expression = types::Time::fromSeconds(2.2 * std::abs(info_a(k, 0) - info_b(k, 0)) / v_max(k));
        duration = (duration > expression) ? duration : expression;
    }
}

/**
 * @brief Function to create trajectory equation
 * 
 * @param beta 
 * @param time Time in microseconds
 * Obs.: Operate in seconds to avoid numerical overflow
 */
void TrajectoryTwoPoint::evalBeta1(Eigen::Vector4d &beta, types::Time time)
{
    beta(0) = time.toSeconds()*time.toSeconds()*time.toSeconds();
    beta(1) = time.toSeconds()*time.toSeconds();
    beta(2) = time.toSeconds();
    beta(3) = 1;
}

void TrajectoryTwoPoint::evalBeta2(Eigen::Vector4d &beta, types::Time time)
{
    beta(0) = 3*time.toSeconds()*time.toSeconds();
    beta(1) = 2*time.toSeconds();
    beta(2) = 1;
    beta(3) = 0;
}

void TrajectoryTwoPoint::evalBeta3(Eigen::Vector4d &beta, types::Time time)
{
    beta(0) = 6*time.toSeconds();
    beta(1) = 2;
    beta(2) = 0;
    beta(3) = 0;
}

void TrajectoryTwoPoint::setPointA(const types::Waypoint::Data &pos_vel_acc)
{
    if (pos_vel_acc.rows() != n_polys)
        throw std::logic_error("[ERROR] (TrajectoryTwoPoint) Must provide as many initial points as the number of independent trajectories");
    if (pos_vel_acc.cols() != 3)
        throw std::logic_error("[ERROR] (TrajectoryTwoPoint) The points matrix must have three columns");
    info_a = pos_vel_acc;
}

void TrajectoryTwoPoint::setPointB(const types::Waypoint::Data &pos_vel_acc)
{
    if (pos_vel_acc.rows() != n_polys)
        throw std::logic_error("[ERROR] (TrajectoryTwoPoint) Must provide as many initial points as the number of independent trajectories");
    if (pos_vel_acc.cols() != 3)
        throw std::logic_error("[ERROR] (TrajectoryTwoPoint) The points matrix must have three columns");
    info_b = pos_vel_acc;
}

bool TrajectoryTwoPoint::configure(types::Time t_min, const Eigen::VectorXd &v_max)
{
    if (t_min <= types::Time::fromMicroseconds(0))
    {
        std::cerr << "[WARN] (TrajectoryTwoPoint) Must provide a positive final time. "
                     << "Refusing to configure trajectory." << std::endl;
        return false;
    }

    computeDuration(t_min, v_max);

    generateMatrixA();

    coeffs.resize(n_polys, 12);
    for (unsigned int k = 0; k < n_polys; ++k)
    {
        coeffs.row(k) = solveSystem(info_a.row(k), info_b.row(k));
    }

    // After creating the coefficients, release allocatted memory from other data members
    linear_system_matrix_A.resize(0,0);
    if (coeffs.hasNaN())
    {
        coeffs.resize(0,0);
        duration = types::Time::fromMicroseconds(0);
        return false;
    }

    info_a.resize(0,0);
    info_b.resize(0,0);
    return true;
}

types::Waypoint TrajectoryTwoPoint::get(types::Time time)
{
    unsigned int offset;
    if (time < duration / 3)
        offset = 0;
    else if (time < duration * 2 / 3)
        offset = 4;
    else
    {
        offset = 8;
        if (time > duration)
            time = duration;
    }
    const Eigen::MatrixXd &poly = coeffs.block(0, offset, n_polys, 4);

    Eigen::MatrixXd ret(n_polys, 3);
    Eigen::Vector4d beta;

    // Initialization
    beta.setZero(); // this removes an incorrect warning of uninitialized beta

    // Populate ret
    evalBeta1(beta, time);
    ret.col(0) = poly * beta;
    //
    evalBeta2(beta, time);
    ret.col(1) = poly * beta;
    //
    evalBeta3(beta, time);
    ret.col(2) = poly * beta;

    return types::Waypoint(ret,time);
}

}
}
