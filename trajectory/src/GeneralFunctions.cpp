#include "scar/trajectory/GeneralFunctions.hpp"


namespace scar
{
namespace trajectory
{


Eigen::Matrix3d rot_x(double ang)
{
    double c = cos(ang);
    double s = sin(ang);
    Eigen::Matrix3d ret;
    ret << 1, 0, 0,
            0, c, -s,
            0, s, c;
    return ret;
}


Eigen::Matrix3d rot_y(double ang)
{
    double c = cos(ang);
    double s = sin(ang);
    Eigen::Matrix3d ret;
    ret << c, 0, s,
            0, 1, 0,
            -s, 0, c;
    return ret;
}


Eigen::Matrix3d rot_z(double ang)
{
    double c = cos(ang);
    double s = sin(ang);
    Eigen::Matrix3d ret;
    ret << c, -s, 0,
            s, c, 0,
            0, 0, 1;
    return ret;
}


Eigen::Matrix3d delRot_x(double ang)
{
    double c = cos(ang);
    double s = sin(ang);
    Eigen::Matrix3d ret;
    ret << 0, 0, 0,
            0, -s, -c,
            0, c, -s;
    return ret;
}


Eigen::Matrix3d delRot_y(double ang)
{
    double c = cos(ang);
    double s = sin(ang);
    Eigen::Matrix3d ret;
    ret << -s, 0, c,
            0, 0, 0,
            -c, 0, -s;
    return ret;
}


Eigen::Matrix3d delRot_z(double ang)
{
    double c = cos(ang);
    double s = sin(ang);
    Eigen::Matrix3d ret;
    ret << -s, -c, 0,
            c, -s, 0,
            0, 0, 0;
    return ret;
}

void wrap2Pi(double & ang)
{
    ang = std::fmod(ang,2*M_PI);
    if (ang > M_PI)
        ang -= 2*M_PI;
    else if (ang <= -M_PI)
        ang += 2*M_PI;
}

void wrap2Pi(Eigen::VectorXd & ang)
{
    for (unsigned int i = 0; i < ang.size(); i++)
        wrap2Pi(ang(i));
}

unsigned int nChooseK(unsigned int N, unsigned int K)
{
    double ret = 1;
    for (unsigned int i = 1; i <= K; i++)
        ret *= (N + 1 - i)/(double)i;
    return (unsigned int)ret;
}

Eigen::Vector3d convertCartesian2Cylindrical(const Eigen::Vector3d &pos)
{
    Eigen::Vector3d pos_cylindrical;
    double x = pos(0), y = pos(1);
    pos_cylindrical << std::sqrt(x*x + y*y), std::atan2(y, x), pos(2);
    return pos_cylindrical;
}

Eigen::Vector3d convertCartesian2Spherical(const Eigen::Vector3d &pos)
{
    Eigen::Vector3d pos_spherical;
    double x = pos(0), y = pos(1), z = pos(2), rho = std::sqrt(x*x + y*y + z*z);
    pos_spherical << rho, std::atan2(y, x), std::asin(z / rho);
    return pos_spherical;
}

Eigen::Vector3d convertSpherical2Cartesian(const Eigen::Vector3d &pos)
{
    Eigen::Vector3d pos_cartesian;
    double rho = pos(0), theta = pos(1), psi = pos(2);
    double rho_cos = rho * std::cos(psi);
    pos_cartesian << rho_cos * std::cos(theta), rho_cos * std::sin(theta), rho * std::sin(psi);
    return pos_cartesian;
}

Eigen::Matrix3d convertSpherical2Cartesian_Jacobian(const Eigen::Vector3d &pos)
{
    double rho = pos(0), the = pos(1), psi = pos(2);

    double c_psi = std::cos(psi), s_psi = std::sin(psi);
    double c_the = std::cos(the), s_the = std::sin(the);
    double rho_cos_psi = rho * c_psi;
    double rho_sin_psi = rho * s_psi;

    Eigen::Matrix3d ret;
    ret << c_the * c_psi, -rho_cos_psi * s_the, -rho_sin_psi * c_the,
           s_the * c_psi,  rho_cos_psi * c_the, -rho_sin_psi * s_the,
                   s_psi,                    0,  rho_cos_psi;
    return ret;
}

Eigen::Matrix3d convertSpherical2Cartesian_dotJacobian(const Eigen::Vector3d &pos, const Eigen::Vector3d &vel)
{
    double  rho = pos(0),  the = pos(1),  psi = pos(2);
    double drho = vel(0), dthe = vel(1), dpsi = vel(2);

    double c_psi = std::cos(psi), s_psi = std::sin(psi);
    double c_the = std::cos(the), s_the = std::sin(the);
    double rho_cos_psi = rho * c_psi;
    double rho_sin_psi = rho * s_psi;
    double drho_cos_psi = drho * c_psi;

    Eigen::Matrix3d ret;
    ret << - dpsi*c_the*s_psi - dthe*c_psi*s_the, dpsi*rho_sin_psi*s_the - drho_cos_psi*s_the - dthe*rho_cos_psi*c_the,   dthe*rho_sin_psi*s_the - drho*c_the*s_psi - dpsi*rho_cos_psi*c_the,
             dthe*c_psi*c_the - dpsi*s_psi*s_the,   drho_cos_psi*c_the - dpsi*rho*c_the*s_psi - dthe*rho_cos_psi*s_the,   - drho*s_psi*s_the - dpsi*rho_cos_psi*s_the - dthe*rho*c_the*s_psi,
                                     dpsi*c_psi,                                                                    0,                                         rho_cos_psi - dpsi*rho_sin_psi;
    return ret;
}


}
}
