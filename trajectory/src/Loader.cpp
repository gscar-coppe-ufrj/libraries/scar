#include "scar/trajectory/Loader.hpp"

#include <fstream>
#include <iostream>
#include <exception>


using namespace ::scar::trajectory;


namespace
{

static inline bool isIn(char ch, const std::string& str)
{
    return str.find(ch) != std::string::npos;
}

static inline void jumpLine(std::ifstream& file, char eol)
{
    char ch = ' ';
    while (file.good() && ch != eol)
        file.get(ch);
}

}


Loader::Loader(std::string filename, std::vector<int> indices) :
    Loader(filename, indices, {LIMIT_DISABLED, LIMIT_DISABLED})
{
}

Loader::Loader(std::vector<std::string> filenames, std::vector<int> indices) :
    Loader(filenames, indices, {LIMIT_DISABLED, LIMIT_DISABLED})
{
}

Loader::Loader(std::string filename, std::vector<int> indices, std::array<Eigen::Index, 2> window) :
    data(0, 0)
{
    load(filename, indices, 0, window);
}

Loader::Loader(std::vector<std::string> filenames, std::vector<int> indices, std::array<Eigen::Index, 2> window) :
    data(0, 0)
{
    Eigen::Index rows = 0;
    for (auto filename : filenames)
    {
        load(filename, indices, rows, window);
        if (rows == 0)
            rows = data.rows();
    }
}

void Loader::load(std::string filename, std::vector<int> indices, Eigen::Index n_rows, std::array<Eigen::Index, 2> window)
{
    std::ifstream file(filename);
    if (!file.is_open())
        throw std::invalid_argument("unable to open file");

    if (window[0] == -1)
        window[0] = std::numeric_limits<Eigen::Index>::min();
    if (window[1] == -1)
        window[1] = std::numeric_limits<Eigen::Index>::max();

    char eol = file.widen('\n');
    std::vector<char> delims_numbers({' ', ',', eol, file.widen('\t')});
    std::vector<char> delims_line({'\n'});
    //
    auto gen_delims_string = [] (const std::vector<char>& delims) -> std::string {
        std::stringstream ss;
        for (auto delim : delims)
            ss << delim;
        return ss.str();
    };
    //
    auto delims_numbers_str = gen_delims_string(delims_numbers);
    auto delims_line_str = gen_delims_string(delims_line);
    //
    Eigen::Index offset = data.rows();
    Eigen::Index n_cols = data.cols();
    if (data.rows() == 0)
    {
        data.conservativeResize(
            indices.empty() ? RESERVED_SIZE : static_cast<Eigen::Index>(indices.size()),
            RESERVED_SIZE
        );
    }
    else
    {
        data.conservativeResize(
            data.rows() + n_rows,
            data.cols()
        );
    }
    Eigen::Index row = offset, row_file = 0;
    auto target_row = indices.begin();
    while (file.good())
    {
        if (!indices.empty())
        {
            if (row_file != *target_row)
            {
                ++row_file;
                jumpLine(file, eol);
                continue;
            }
        }
        char ch = '0';
        unsigned int col = 0, col_file = 0;
        if (data.rows() == row)
            data.conservativeResize(data.rows() + RESERVED_SIZE, data.cols());
        while (!isIn(ch, delims_line_str) && file.good())
        {
            char buffer[30];
            unsigned int k = 0;
            ch = '0';
            while (file.good())
            {
                file.get(ch);
                if (!isIn(ch, delims_numbers_str) && file.good())
                    buffer[k++] = ch;
                else
                    break;
            }
            std::string num;
            if (k != 0)
            {
                if (col_file < window[0])
                {
                    ++col_file;
                    continue;
                }
                num = std::string(buffer, k);
                if (data.cols() <= col)
                    data.conservativeResize(data.rows(), data.cols() + RESERVED_SIZE);
                data(row, col++) = std::atof(num.c_str());
                ++col_file;
                if (col_file > window[1])
                    break;
            }
        }
        if (n_cols == 0)
            n_cols = col;
        if (col != 0)
        {
            if (col != n_cols)
            {
                data.resize(0,0);
                throw std::invalid_argument("some rows have a different number of columns");
            }
            ++row;
        }
        if (!indices.empty())
        {
            if (col == 0)
            {
                data.resize(0,0);
                throw std::invalid_argument("the number of columns in one of the requested rows is not valid");
            }
            if (++target_row == indices.end())
                break;
            ++row_file;
        }
    }
    data.conservativeResize(row, n_cols);
}
