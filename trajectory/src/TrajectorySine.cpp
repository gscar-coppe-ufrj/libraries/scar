#include "scar/trajectory/TrajectorySine.hpp"
#include <iostream>

namespace scar
{
namespace trajectory
{

TrajectorySine::TrajectorySine(const Eigen::MatrixXd& config, Eigen::Index order) :
    cols(order + 1),
    rows(config.rows()),
    frequency(rows, cols),
    amp_times_freq_pow(rows, cols),
    phase(rows, cols),
    offset(rows, cols)
{
    Eigen::ArrayXd cfg_amplitude(config.col(0));
    Eigen::ArrayXd cfg_frequency(config.col(1));
    Eigen::ArrayXd cfg_phase(config.col(2));
    Eigen::ArrayXd cfg_offset(config.col(3));

    amp_times_freq_pow.col(0) = cfg_amplitude;
    phase.col(0) = cfg_phase;
    frequency.col(0) = cfg_frequency;
    for (Eigen::Index ind_col = 1; ind_col < cols; ++ind_col)
    {
        amp_times_freq_pow.col(ind_col) = amp_times_freq_pow.col(ind_col-1) * cfg_frequency;
        phase.col(ind_col) = M_PI/2 * ind_col + cfg_phase;
        frequency.col(ind_col) = cfg_frequency;
    }
    offset.setZero();
    offset.col(0) = cfg_offset;
}

TrajectorySine::~TrajectorySine()
{
}

types::Waypoint TrajectorySine::get(types::Time ts)
{
    double time = ts.toSeconds();
    Eigen::MatrixXd sine_matrix = amp_times_freq_pow * Eigen::sin(frequency * time + phase) + offset;
    return types::Waypoint(sine_matrix, ts);
}

}
}
