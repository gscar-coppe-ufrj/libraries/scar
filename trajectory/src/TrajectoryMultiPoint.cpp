#include "scar/trajectory/TrajectoryMultiPoint.hpp"

namespace scar
{
namespace trajectory
{

TrajectoryMultiPoint::TrajectoryMultiPoint():
    TrajectoryWaypoints(), vec_trajectory()
{
}

TrajectoryMultiPoint::TrajectoryMultiPoint(Waypoints waypoints, const types::Waypoint::Data& v_max, bool sort):
    TrajectoryWaypoints(), vec_trajectory(waypoints.size() - 1)
{
    Waypoints two_waypoints_vec(2);
    // Create a local copy from waypoints input vector
    auto it_wp = waypoints.begin();
    two_waypoints_vec[0] = *it_wp;
    for (auto it_traj = vec_trajectory.begin(); it_traj != vec_trajectory.end(); ++it_traj)
    {
        two_waypoints_vec[1] = *(it_wp + 1);
        *it_traj  = TrajectoryTwoPoint(two_waypoints_vec, v_max, sort);
        *(it_wp + 1) = it_traj->get(it_traj->getFinalTime());
        two_waypoints_vec[0] = two_waypoints_vec[1];
        ++it_wp;
    }
}

const types::Time& TrajectoryMultiPoint::getFinalTime() const
{
    return vec_trajectory.crbegin()->getFinalTime();
}

types::Waypoint TrajectoryMultiPoint::get(types::Time ts)
{
    if (ts > this->getFinalTime())
        std::cerr << "[Warn] Time given is higher than the necessary to complete the trajectory";

    auto it_traj = vec_trajectory.begin();
    auto value = it_traj->getFinalTime();
    while (it_traj->getFinalTime().toMicroseconds() < ts.toMicroseconds())
        ++it_traj;

    if (it_traj == vec_trajectory.end() && ts != it_traj->getFinalTime())
    {
        std::cerr << "[Error] Method get in TrajectoryMultiPoint couldn't find a respective value for the given time.";
        return types::Waypoint();
    }
    else
        return it_traj->get(ts);
}

}
}
