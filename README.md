# Description
The SCAR (Solutions for Control Automation and Robotics) project contains several libraries
used for developing solutions for Control Systems and Robotics applications.

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).

# Overview
This library is divided into components. Each one has a `CMakeLists.txt` file and a `cmake` folder. This folder has the files `library.cmake.in` and `library.pc.in`, which  are used during compilation.

This library is designed to be framework independent and, because of that, one should avoid using ROS or ROCK types and methods.

# Building

Create a directory, for instance, and change to this new directory:

`
mkdir -p ~/scar/src
cd ~/scar/src
`

Then clone this repo:

`
git clone git@gitlab.com:gscar-coppe-ufrj/libraries/scar.git
`

Now let's start building:
`
mkdir -p ~/scar/build
cd ~/scar/build
cmake ../src/scar
make
`

# Installation

To install the library to the default path:

`
sudo make install
`
