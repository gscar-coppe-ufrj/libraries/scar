/**
 * @file Controller.hpp
 * @author LEAD
 * @brief Defines a control base class
 * @version 0.1
 * @date 2019-12-18
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once


#include <Eigen/Core>
#include <functional>

#include "scar/types/Time.hpp"


namespace scar
{
namespace controller
{


typedef Eigen::VectorXd Output;
typedef Eigen::VectorXd Input;


class Controller
{
public:
    typedef std::function<void (Output &)> CallbackSaturation;
    typedef std::function<void (Output &)> CallbackPostProcessing;

private:
    Output output, output_presat, output_default;
    types::Time time_last;

    CallbackPostProcessing cb_postprocessing;
    CallbackSaturation cb_saturation;

protected:
    const unsigned int _N;

    /**
     * @brief Called every time #update is called so that the base class performs the actual control algorithm
     * @param time Current time.
     * @param ref Reference.
     * @param signal Signal.
     * @param last_output The last output, defaults to getDefaultOutput during the first run.
     * @return The computed output.
     */
    virtual const Output & updateControl(const types::Time &time, Input & ref, Input & signal) = 0;

    /**
     * @brief Called during the first #update after a #restart is called
     * @param time Current time.
     * @param ref Reference.
     * @param signal Signal.
     */
    virtual void configureFirstRun(const types::Time &time, Input &ref, Input &signal);

public:
    explicit Controller(unsigned int N_controllers);

    /**
     * @brief Checks if all parameters are set correctly.
     * @return True if everything is ok. False otherwise.
     */
    virtual bool ok() const = 0;

    /**
     * @brief Sets the default output.
     */
    void setDefaultOutput(const Output &out);

    /**
     * @brief Returns the default output.
     *
     * This method is called internally when update is called for a previous time instant.
     *
     * @return
     */
    const Output & getDefaultOutput() const;

    /**
     * @brief Restarts controller to its initial configuration.
     */
    virtual void restart();

    /**
     * @brief Configures a callback to be called after #update is finished to let the caller
     * modify the final output, possibly performing some post-processing.
     * @param callback The external callback.
     */
    void setCallbackPostProcessing(CallbackPostProcessing callback);

    /**
     * @brief Clears the post-processing callback.
     * @see #setCallbackPostProcessing
     */
    inline void clearCallbackPostProcessing()
    {
        cb_postprocessing = 0;
    }

    /**
     * @brief Clears the saturation callback.
     * @see #setCallbackSaturation
     */
    inline void clearCallbackSaturation()
    {
        cb_saturation = 0;
    }

    /**
     * @brief Configures a callback to be called after #update is finished to let the caller
     * saturate the final output.
     * @param callback The external callback.
     */
    void setCallbackSaturation(CallbackSaturation callback);

    /**
     * @brief Updates the control algorithm up to the specified \p time.
     *
     * To get the control output after the update is performed call #getOutput.
     *
     * @param time Current time.
     * @param ref Reference.
     * @param signal Signal.
     * @see getOutput update(Time, const Input &, const Input &)
     */
    void update(const types::Time &time, Input ref, Input signal);

    inline const Output & getOutput() const
    {
        return output;
    }

    inline const Output & getOutputPreSat() const
    {
        return output_presat;
    }

    inline unsigned int size() const
    {
        return _N;
    }

};


}
}
