/**
 * @file SettingsPIDFirstOrder.hpp
 * @author LEAD
 * @brief Class to create and handle PID control settings for a first order plant
 * @version 0.1
 * @date 2019-12-18
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once


#include "scar/controller/SettingsPID.hpp"


namespace scar
{
namespace controller
{


class SettingsPIDFirstOrder : public SettingsPID
{
private:
    //! Natural (resonant) frequency.
    double wn;

    //! Cutoff frequency.
    double wc;

    //! Damping coefficient.
    double damp;

    /**
     * @brief Hides the constructor because instances of this class should only be created via
     * #createFromSpecF and #createFromSpecT.
     */
    SettingsPIDFirstOrder(double kp, double ki, double kd);

public:
    /**
     * @brief Returns the suggested cutoff frequency that should be used to design the derivative filter.
     * @return The suggested cutoff frequency or zero if there is no derivative action
     */
    inline double getSuggestedDerivativeCutoff() const override
    {
        return 0;
    }

    /**
     * @brief Returns the suggested sampling, computed as max(0.01 * 2*pi/w_c, 1e-5),
     * where w_c is the cutoff frequency and ts = #getSamplingTime().
     * @return The suggested sampling.
     */
    scar::types::Time getSuggestedSampling() const override;

    /**
     * @brief Returns the expected settling time given the PID settings.
     */
    scar::types::Time getSettlingTime() const override;

    /**
     * @brief Returns the expected overshoot given the PID settings.
     *
     * The overshoot is in [0, 1], e.g. 0.1 corresponds to 10% overshoot.
     */
    double getOvershoot() const;

    /**
     * @brief Returns the expected damping given the PID settings.
     *
     * Returns nan if the controller is purely proportional.
     */
    inline double getDamping() const
    {
        return damp;
    }

    /**
     * @brief Returns the expected natural frequency (in rad/s) given the PID settings.
     *
     * Returns nan if the controller is purely proportional.
     */
    inline double getNaturalFrequency() const
    {
        return wn;
    }

    /**
     * @brief Returns the expected cutoff frequency (in rad/s) given the PID settings.
     */
    inline double getCutoffFrequency() const
    {
        return wc;
    }

    /**
     * @brief Generate a SettingsPID instance for a P controller with gains designed to meet the provided time specifications.
     * @param settling_time The desired settling time, for which the step-response error < 2%.
     * @return An instance of SettingsPID with kp, ki = 0, and kd = 0 designed to meet the specifications.
     * @throws std::logic_error If parameters are not in a valid range.
     */
    static SettingsPIDFirstOrder createFromSpecT(double settling_time);

    /**
     * @brief Generate a SettingsPID instance for a PI controller with gains designed to meet the provided time specifications.
     * @param overshoot The maximum allowed overshoot, typically [0, 0.2]. Accepted values are in [0, 0.5].
     * @param settling_time The desired settling time, for which the step-response error < 2%.
     * @return An instance of SettingsPID with kp, ki, and kd = 0 designed to meet the specifications.
     * @throws std::logic_error If parameters are not in a valid range.
     */
    static SettingsPIDFirstOrder createFromSpecT(double overshoot, double settling_time);

    /**
     * @brief Generate a SettingsPID instance to implement a PI with gains designed to produce the denominator of
     *
     * \f[
     *      \frac{u(s)}{e(s)} =  = \frac{\omega^2_n}{s^2 + 2\zeta\omega_n s + \omega^2_n}
     *      \quad ; \quad
     *      \omega_n = \frac{\omega_c}{\zeta}
     * \f]
     *
     * A general rule of thumb for choosing the cutoff frequency \f$\omega_c\f$, if you don't wish to set
     * it based on frequency analysis, is to set it to \f$ \omega_c = 4\zeta t_s \f$, where \f$ t_s \f$ corresponds
     * to the desired settling time.
     *
     * This choice ensures step-response error < 2% for t >\f$t_s\f$._s\f$ and error < 1% for t >\f$t_s\f$.
     *
     * @param damping Damping coefficient \f$\zeta\f$, valid values are in (0, 2].
     * @param cutoff Cutoff frequency \f$\omega_c\f$, valid values are > 0.
     * @return An instance of SettingsPID with kp, ki, and kd = 0 designed to meet the specifications.
     * @throws std::logic_error If parameters are not in a valid range.
     */
    static SettingsPIDFirstOrder createFromSpecF(double damping, double cutoff);
};


}
}
