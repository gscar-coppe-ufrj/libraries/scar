/**
 * @file SettingsPID.hpp
 * @author LEAD
 * @brief Declares a base class to set and handle PID control settings
 * @version 0.1
 * @date 2019-12-18
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include "scar/types/Time.hpp"


namespace scar
{
namespace controller
{


class SettingsPID
{
private:
    //! Proportional gain.
    double kp;

    //! Integral gain.
    double ki;

    //! Derivative gain.
    double kd;

public:
    //! Setpoint weighing term
    /** Setpoint weighing term, generally between 0 and 1
     * - B = 0 reference is introduced only through integral term
     * - B = 1 disables setpoint weighting
     */
    double weight_reference;

    //! Anti-integrator-windup
    /** Anti-integrator-windup time constant
     * - < 0 disable
     * - = 0 and Td = 0  disable
     * - = 0 and Td > 0  Tt = sqrt(Ti * Td)
     * */
    double gain_antiwidnup;

    /**
     * @brief Constructor.
     *
     * @param kp Proportional gain.
     * @param ki Integral gain.
     * @param kd Derivative gain.
     */
    SettingsPID(double kp, double ki, double kd);
    inline SettingsPID() : SettingsPID(0,0,0) {}

    /**
     * @brief Returns the suggested cutoff frequency that should be used to design the derivative filter.
     * @return The suggested cutoff frequency or zero if there is no derivative action
     */
    virtual double getSuggestedDerivativeCutoff() const;

    /**
     * @brief Returns the suggested sampling.
     */
    virtual scar::types::Time getSuggestedSampling() const;

    /**
     * @brief Returns the expected settling time given the PID settings.
     *
     * Defaults to a null time value.
     */
    virtual scar::types::Time getSettlingTime() const;

    inline double getKp() const {return kp;}
    inline double getKi() const {return ki;}
    inline double getKd() const {return kd;}
};


}
}
