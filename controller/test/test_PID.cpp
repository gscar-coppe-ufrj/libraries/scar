#include <gtest/gtest.h>

#include "scar/controller/PID.hpp"
#include "scar/controller/SettingsPIDSecondOrder.hpp"
#include "scar/controller/SettingsPIDFirstOrder.hpp"
#include "scar/linear-system/HelperFunctions.hpp"


using namespace ::scar::controller;


bool equal_eigen(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2)
{
    return (m1.array() == m2.array()).all();
}

bool different_eigen(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2)
{
    return (m1.array() != m2.array()).any();
}

void testReset(Controller &controller)
{
    // run twice and see if we get the same results
    // this should verify that restart is working properly
    const unsigned int N = controller.size();
    Input ref(N), sig(N);
    ref << 1, 0.8;
    sig << 0.5, 0.6;
    scar::types::Time time = scar::types::Time::fromMicroseconds(0);
    scar::types::Time dt = scar::types::Time::fromSeconds(1);
    unsigned int n_turns = 10;
    std::vector<Output> history(n_turns);
    controller.restart();
    for (unsigned int k = 0; k < n_turns; ++k)
    {
        controller.update(
            time,
            ref,
            sig
        );
        history[k] = controller.getOutput();
        time += dt;
    }
    controller.restart();
    time = scar::types::Time::fromMicroseconds(0);
    for (unsigned int k = 0; k < n_turns; ++k)
    {
        controller.update(
            time,
            ref,
            sig
        );
        time += dt;
        EXPECT_FALSE( ((controller.getOutput() - history[k]).array() != 0).any() );
    }
}

void testInputChannels(Controller &controller)
{
    const unsigned int N = controller.size();
    Input ref(N), sig(N);
    ref.setConstant(23.43);
    sig.setConstant(11.33);
    scar::types::Time time = scar::types::Time::fromMicroseconds(0);
    scar::types::Time dt = scar::types::Time::fromSeconds(1);
    unsigned int n_turns = 10;
    controller.restart();
    for (unsigned int k = 0; k < n_turns; ++k)
    {
        controller.update(
            time,
            ref,
            sig
        );
        Output out = controller.getOutput();
        time += dt;
        EXPECT_FALSE( ((out.array() - out[0]) != 0).any() );
    }
}

::scar::linear_system::LinearSystem getDoubleIntegrator(scar::types::Time sampling)
{
    Eigen::VectorXd num(3), den(3);
    num << 0, 0, 1;
    den << 1, 0, 0;
    ::scar::linear_system::LinearSystem plant(num, den, sampling);
    plant.setInitialConditions(
        Eigen::MatrixXd::Zero(1, 2),
        Eigen::MatrixXd::Zero(1, 2)
    );
    return plant;
}

::scar::linear_system::LinearSystem getIntegrator(scar::types::Time sampling)
{
    Eigen::VectorXd num(1), den(2);
    num << 1;
    den << 1, 0;
    ::scar::linear_system::LinearSystem plant(num, den, sampling);
    plant.setInitialConditions(
        Eigen::MatrixXd::Zero(1, 1),
        Eigen::MatrixXd::Zero(1, 1)
    );
    return plant;
}

void testStepResponse(Controller &controller, const SettingsPIDSecondOrder &settings, scar::types::Time sampling,
    scar::types::Time time_settling, double max_overshoot, double tol_settling_error, double tol_diff_model)
{
    Input ref(1);
    ref.setConstant(1);

    auto plant = getDoubleIntegrator(sampling);

    double wn = settings.getNaturalFrequency();
    double damp = settings.getDamping();
    double far = settings.getFarPole();

    Eigen::VectorXd num_filter(3), den_filter(3);
    num_filter << 0, 0, settings.getKi();
    den_filter << settings.getKd(), settings.getKp(), settings.getKi();
    ::scar::linear_system::LinearSystem prefilter(num_filter, den_filter, sampling);

    num_filter << settings.getKi(), 0, 0;
    den_filter << settings.getKd(), settings.getKp(), settings.getKi();
    ::scar::linear_system::LinearSystem feedforward(num_filter, den_filter, sampling);
    controller.setCallbackPostProcessing(
        [&feedforward] (Output &out) -> void {
            out += feedforward.getOutput();
        }
    );

    Eigen::VectorXd num_model(1), den_model(4);
    num_model << wn*wn*far;
    den_model << 1, 2*damp*wn+far, 2*damp*wn*far+wn*wn, far*wn*wn;
    ::scar::linear_system::LinearSystem model(num_model, den_model, sampling);

    Eigen::MatrixXd init_out_filter(1, 2);
    init_out_filter << plant.getOutput()[0], 0;
    prefilter.setInitialConditions(
        Eigen::MatrixXd::Constant(1,2, ref[0]),
        init_out_filter);
    feedforward.setInitialConditions(
        Eigen::MatrixXd::Constant(1,2, ref[0]),
        Eigen::MatrixXd::Zero(1,2));
    model.setInitialConditions(
        Eigen::MatrixXd::Constant(1,3, ref[0]),
        Eigen::MatrixXd::Zero(1,3)
    );

    scar::types::Time time = scar::types::Time::fromMicroseconds(0);
    scar::types::Time dt = sampling;
    double ratio = 0;
    double overshoot = 0;
    scar::types::Time time_overshoot;
    plant.setInitialTime(time);
    prefilter.setInitialTime(time);
    feedforward.setInitialTime(time);
    model.setInitialTime(time);
    controller.restart();
    std::int64_t samples = (time_settling.toMicroseconds() / dt.toMicroseconds()) + 2;
    Eigen::MatrixXd data(samples, 3);
    unsigned int k = 0;
    while (time < time_settling)
    {
        prefilter.update(
            ref,
            time
        );
        feedforward.update(
            ref,
            time
        );
        controller.update(
            time,
            prefilter.getOutput(),
            plant.getOutput()
        );
        plant.update(controller.getOutput(), time);
        ratio = std::abs( plant.getOutput()[0]/ref[0] - 1 );
        if (plant.getOutput()[0] > ref[0] && ratio > overshoot)
        {
            overshoot = ratio;
            time_overshoot = time;
        }
        data(k,0) = time.toMicroseconds();
        data(k,1) = model.update(ref, time)[0];
        data(k,2) = plant.getOutput()[0];
        ++k;
        time += dt;

    }
    data.conservativeResize(k, Eigen::NoChange);
    if (overshoot > max_overshoot)
    {
        std::stringstream error;
        error << "PID exceeded the maximum overshoot at t = " << time_overshoot
              << " ts : (max_overshoot, overshoot) = ("
              << max_overshoot << " , " << overshoot << ")";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();

    }
    if (ratio >= tol_settling_error)
    {
        std::stringstream error;
        error << "PID did not meet the required final performance: "
              << "(ref , out) = (" << 1 << " , " << plant.getOutput()[0]/ref[0]
              << ")";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();
    }
    double diff_model = (data.col(1) - data.col(2)).array().abs().maxCoeff();
    if ( diff_model > tol_diff_model )
    {
        std::stringstream error;
        error << "PID closed-loop step-response differs from its nominal value by: "
              << diff_model << " > " << tol_diff_model
              << ")";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();
    }
}

void testFrequencyResponse(Controller &controller, const SettingsPIDSecondOrder &settings, scar::types::Time sampling, double damp, double cutoff, double farpole)
{
    double wn = ::scar::linear_system::cutoff2resonant(cutoff, damp);
    scar::types::Time dt = sampling;

    auto plant = getDoubleIntegrator(sampling);

    Eigen::VectorXd num_filter(3), den_filter(3);
    num_filter << 0, pow(settings.getNaturalFrequency(), 2), settings.getKi();
    den_filter << settings.getKd(), settings.getKp(), settings.getKi();
    ::scar::linear_system::LinearSystem prefilter(num_filter, den_filter, sampling);

    Eigen::VectorXd num_model(4), den_model(4);
    num_model << 0, farpole + 2*damp*wn, wn*wn + farpole*2*damp*wn, farpole*wn*wn;
    den_model << 1, farpole + 2*damp*wn, wn*wn + farpole*2*damp*wn, farpole*wn*wn;
    ::scar::linear_system::LinearSystem model_closedloop(num_model, den_model, sampling);

    Eigen::MatrixXd init_out_model(1, 3), init_out_filter(1, 2);
    init_out_model << 0, 0, 0;
    init_out_filter << 0, 0;

    Eigen::MatrixXd init_in_model(1, 3), init_in_filter(1, 2);
    init_in_model.setZero();
    init_in_filter.setZero();

    Input ref(1);

    double input_freq_array[] = {cutoff / 10, cutoff, wn};
    for (double input_freq : input_freq_array)
    {
        double T = 2*M_PI / input_freq;
        scar::types::Time t_final = scar::types::Time::fromSeconds(10 * T);
        scar::types::Time t_transient = t_final - scar::types::Time::fromSeconds(2*T);
        scar::types::Time time = scar::types::Time::fromMicroseconds(0);
        plant.setInitialConditions(
            Eigen::MatrixXd::Zero(1, 2),
            Eigen::MatrixXd::Zero(1, 2)
        );
        prefilter.setInitialConditions(
            init_in_filter,
            init_out_filter
        );
        model_closedloop.setInitialConditions(
            init_in_model,
            init_out_model
        );
        plant.setInitialTime(time);
        prefilter.setInitialTime(time);
        model_closedloop.setInitialTime(time);
        controller.restart();
        //
        double amplitude = 0, max_error = 0;
        //
        while (time < t_final)
        {
            ref[0] = std::sin(input_freq * time.toSeconds());
            prefilter.update(
                ref,
                time
            );
            controller.update(
                time,
                prefilter.getOutput(),
                plant.getOutput()
            );
            plant.update(controller.getOutput(), time);

            model_closedloop.update(prefilter.getOutput(), time);

            if (time > t_transient)
            {
                double amplitude_now = model_closedloop.getOutput()[0];
                double error = std::abs(amplitude_now - plant.getOutput()[0]);
                if (error > max_error)
                    max_error = error;
                if (amplitude_now > amplitude)
                    amplitude = amplitude_now;
            }
            time += dt;
        }
        if (max_error / amplitude > 0.1)
        {
            std::cerr << "PID did not meet the required frequency response" << std::endl;
            ADD_FAILURE();
        }
    }
}

void testStepResponsePrefilter(
    std::shared_ptr<SettingsPID> settings,
    scar::linear_system::LinearSystem &plant,
    scar::linear_system::LinearSystem &model,
    const scar::types::Time &sampling
)
{
    Input ref(1);
    ref.setConstant(1);

    scar::controller::SettingsFilter settings_prefilter;
    settings_prefilter.sampling_period = sampling;

    scar::controller::PID controller(1);
    controller.configure(*settings, sampling);
    scar::types::Poly num_plant(1);
    num_plant << 1;
    controller.enableInputPrefilter(num_plant, settings_prefilter);

    scar::types::Time time = scar::types::Time::fromMicroseconds(0);
    scar::types::Time dt = sampling;
    scar::types::Time timeout = settings->getSettlingTime() + scar::types::Time::fromSeconds(2);
    double ratio = 0;
    double overshoot = 0;
    scar::types::Time time_overshoot;
    plant.setInitialTime(time);
    model.setInitialTime(time);
    controller.restart();
    std::int64_t samples = (timeout.toMicroseconds() / dt.toMicroseconds());
    Eigen::MatrixXd data(samples, 3);
    unsigned int k = 0;
    while (time < timeout)
    {
        controller.update(
            time,
            ref,
            plant.getOutput()
        );
        plant.update(controller.getOutput(), time);
        ratio = std::abs( plant.getOutput()[0]/ref[0] - 1 );
        if (plant.getOutput()[0] > ref[0] && ratio > overshoot)
        {
            overshoot = ratio;
            time_overshoot = time;
        }
        data(k,0) = time.toMicroseconds();
        data(k,1) = model.update(ref, time)[0];
        data(k,2) = plant.getOutput()[0];
        ++k;
        time += dt;
    }
    data.conservativeResize(k, Eigen::NoChange);
    auto settings_1st = std::dynamic_pointer_cast<SettingsPIDFirstOrder>(settings);
    auto settings_2nd = std::dynamic_pointer_cast<SettingsPIDSecondOrder>(settings);

    double max_overshoot;
    if (settings_1st)
        max_overshoot = settings_1st->getOvershoot() * 1.01;
    else if (settings_2nd)
        max_overshoot = settings_2nd->getOvershoot() * 1.01;
    else
    {
        max_overshoot = 0;
        std::stringstream error;
        error << "PID settings should be of either first or second order.";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();
    }
    double tol_settling_error = 1e-2;
    double tol_diff_model = 2.5e-3;
    if (overshoot > max_overshoot)
    {
        std::stringstream error;
        error << "PID exceeded the maximum overshoot at t = " << time_overshoot
              << " ts : (max_overshoot, overshoot) = ("
              << max_overshoot << " , " << overshoot << ")";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();

    }
    if (ratio >= tol_settling_error)
    {
        std::stringstream error;
        error << "PID did not meet the required final performance: "
              << "(ref , out) = (" << 1 << " , " << plant.getOutput()[0]/ref[0]
              << ")";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();
    }
    double diff_model = (data.col(1) - data.col(2)).array().abs().maxCoeff();
    if ( diff_model > tol_diff_model )
    {
        std::stringstream error;
        error << "PID closed-loop step-response differs from its nominal value by: "
              << diff_model << " > " << tol_diff_model
              << ")";
        std::cerr << error.str() << std::endl;
        ADD_FAILURE();
    }
}

TEST(TestPID, test_channels)
{
    PID pid(2);

    SettingsPID settings(1, 0.2, 2);

    pid.configure({settings, settings}, scar::types::Time::fromSeconds(0.2));
    //
    testReset(pid);
}

TEST(TestPID, test_reset)
{
    unsigned int N = 20;
    PID pid(N);

    SettingsPID settings(1, 0.2, 2);

    std::vector<SettingsPID> settings_all(N);
    for (auto &s : settings_all)
        s = settings;

    pid.configure(settings_all, scar::types::Time::fromSeconds(0.2));
    //
    testInputChannels(pid);
}

TEST(TestPID, test_step_response)
{
    PID pid(1);
    std::vector<double> ts_array        = {0.1, 0.5, 1, 5, 10, 50, 100, 500, 1000, 5000, 10000};
    std::vector<double> overshoot_array = {0, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5};
    double tol_ratio_overshoot = 1.1;
    double tol_settling_error = 0.03;
    double tol_diff_model = 1e-1;
    for (double over : overshoot_array)
    {
        for (double ts : ts_array)
        {
            auto settings = SettingsPIDSecondOrder::createFromSpecT(over, ts);
            scar::types::Time sampling = settings.getSuggestedSampling();
            pid.configure(settings, sampling);
            //
            testStepResponse(pid, settings, sampling, scar::types::Time::fromSeconds(ts),
                             tol_ratio_overshoot * over, tol_settling_error, tol_diff_model);
        }
    }
}

TEST(TestPID, test_frequency_response)
{
    PID pid(1);
    std::vector<double> cutoff_array = {0.01, 0.05, 0.1, 0.5, 1, 5, 10, 50, 100, 500, 1000};
    std::vector<double> damp_array   = {0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
    for (double cutoff : cutoff_array)
    {
        for (double damp : damp_array)
        {
            auto settings = SettingsPIDSecondOrder::createFromSpecF(damp, cutoff, 10);
            double wn = cutoff / damp;
            scar::types::Time sampling = scar::types::Time::fromSeconds((2*M_PI/wn) / 1000);
            auto velocity = SettingsFilter::createSecondOrder(0.7, wn * 200);
            velocity.sampling_period = sampling;
            pid.configure(settings, velocity);
            //
            testFrequencyResponse(pid, settings, sampling, damp, cutoff, settings.getFarPole());
        }
    }
}

TEST(TestPID, test_saturation_cb)
{
    PID pid(1);
    auto settings = SettingsPIDSecondOrder::createFromSpecT(0.05, 1);
    scar::types::Time sampling = settings.getSuggestedSampling();
    pid.configure(settings, sampling);
    //
    Input in(1), ref(1);
    in[0] = 0;
    ref[0] = 0;
    pid.update(scar::types::Time::fromMicroseconds(0), ref, in);
    ref[0] = 100000000;
    //
    pid.update(5*sampling, ref, in);
    EXPECT_TRUE( equal_eigen(pid.getOutput(), pid.getOutputPreSat()) );
    //
    int k = 0;
    pid.setCallbackSaturation(
        [&k] (Output &out) -> void {
            out.setConstant(123);
            k = 321;
        }
    );
    pid.update(10*sampling, ref, in);
    EXPECT_TRUE(k == 321);
    EXPECT_TRUE( different_eigen(pid.getOutput(), pid.getOutputPreSat()) );
    EXPECT_TRUE( (pid.getOutput().array() == 123).all() );
}

TEST(TestPID, test_postprocessing_cb)
{
    PID pid(1);
    SettingsPID settings(1, 0, 0);
    scar::types::Time sampling = scar::types::Time::fromSeconds(1);
    pid.configure(settings, sampling);
    //
    Input in(1), ref(1);
    in[0] = 0;
    ref[0] = 0;
    pid.update(scar::types::Time::fromMicroseconds(0), ref, in);
    ref[0] = 1;
    //
    pid.update(5*sampling, ref, in);
    EXPECT_TRUE( equal_eigen(pid.getOutput(), pid.getOutputPreSat()) );
    //
    int k = 0;
    pid.setCallbackPostProcessing(
        [&k] (Output &out) -> void {
            out.setConstant(123);
            k = 321;
        }
    );
    pid.update(10*sampling, ref, in);
    EXPECT_TRUE(k == 321);
    EXPECT_TRUE( equal_eigen(pid.getOutput(), pid.getOutputPreSat()) );
    EXPECT_TRUE( (pid.getOutput().array() == 123).all() );
}

TEST(TestPID, test_derivative_mode)
{
    PID pid(1);
    scar::types::Time sampling = scar::types::Time::fromMilliseconds(10);
    pid.configure( SettingsPIDSecondOrder::createFromSpecT(0.02, 1.0), sampling );
    Input in(1), ref(1);
    in << 0;
    ref << 0;
    for (unsigned int k = 0; k < 10; ++k)
    {
        in[0] = k;
        pid.update(k*sampling, in, ref);
    }
    auto deriv1 = pid.getErrorDerivative();
    Eigen::VectorXd deriv2(1);
    deriv2 = deriv1.array() + 1;
    pid.setDerivativeFiltered(false);
    pid.setErrorDerivative(deriv2);
    auto deriv3 = pid.getErrorDerivative();
    pid.setDerivativeFiltered(true);
    auto deriv4 = pid.getErrorDerivative();

    EXPECT_TRUE( equal_eigen(deriv1, deriv4) );
    EXPECT_TRUE( equal_eigen(deriv2, deriv3) );
    EXPECT_TRUE( different_eigen(deriv1, deriv2) );
    EXPECT_TRUE( different_eigen(deriv3, deriv4) );
}

TEST(TestPrefilter, test_prefilter_PID)
{
    auto sampling = scar::types::Time::fromMilliseconds(1);
    auto plant = getDoubleIntegrator(sampling);
    auto settings = std::make_shared<SettingsPIDSecondOrder>(SettingsPIDSecondOrder::createFromSpecT(0.02, 4));

    double wn   = settings->getNaturalFrequency();
    double damp = settings->getDamping();
    double far  = settings->getFarPole();

    Eigen::VectorXd num_model(1), den_model(4);
    num_model << wn*wn*far;
    den_model << 1, 2*damp*wn+far, 2*damp*wn*far+wn*wn, far*wn*wn;
    ::scar::linear_system::LinearSystem model(num_model, den_model, sampling);
    model.setInitialConditions(
        Eigen::MatrixXd::Constant(1,3, 1),
        Eigen::MatrixXd::Zero(1,3)
    );

    testStepResponsePrefilter(settings, plant, model, sampling);
}

TEST(TestPrefilter, test_prefilter_P)
{
    auto sampling = scar::types::Time::fromMilliseconds(1);
    auto plant = getIntegrator(sampling);

    auto settings = std::make_shared<SettingsPIDFirstOrder>(SettingsPIDFirstOrder::createFromSpecT(1));

    Eigen::VectorXd num_model(1), den_model(2);
    num_model << settings->getKp();
    den_model << 1, settings->getKp();
    ::scar::linear_system::LinearSystem model(num_model, den_model, sampling);
    model.setInitialConditions(
        Eigen::MatrixXd::Constant(1,1, 1),
        Eigen::MatrixXd::Zero(1,1)
    );

    testStepResponsePrefilter(settings, plant, model, sampling);
}

TEST(TestPrefilter, test_prefilter_PI)
{
    auto sampling = scar::types::Time::fromMilliseconds(1);
    auto plant = getIntegrator(sampling);

    auto settings = std::make_shared<SettingsPIDFirstOrder>(SettingsPIDFirstOrder::createFromSpecT(0.07, 1));

    double wn = settings->getNaturalFrequency();
    double damp = settings->getDamping();

    Eigen::VectorXd num_model(1), den_model(3);
    num_model << wn*wn;
    den_model << 1, 2*damp*wn, wn*wn;
    ::scar::linear_system::LinearSystem model(num_model, den_model, sampling);
    model.setInitialConditions(
        Eigen::MatrixXd::Constant(1,2, 1),
        Eigen::MatrixXd::Zero(1,2)
    );

    testStepResponsePrefilter(settings, plant, model, sampling);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
