#include "scar/controller/SettingsPIDFirstOrder.hpp"

#include "scar/linear-system/HelperFunctions.hpp"
#include <cmath>
#include <stdexcept>


using namespace ::scar::controller;


SettingsPIDFirstOrder::SettingsPIDFirstOrder(double kp, double ki, double kd) :
    SettingsPID(kp, ki, kd), wn(std::nan("")), wc(std::nan("")), damp(std::nan(""))
{
}

scar::types::Time SettingsPIDFirstOrder::getSuggestedSampling() const
{
    double sampling;
    sampling = 2 * M_PI / wc / 100;
    return scar::types::Time::fromSeconds(std::max(sampling, 1e-5));
}

scar::types::Time SettingsPIDFirstOrder::getSettlingTime() const
{
    double damp = getDamping();
    if (std::isnan(damp))
        return scar::types::Time::fromSeconds(4 / getKp());
    // underdamped
    else if (damp < 0.99)
        return scar::types::Time::fromSeconds(
            -std::log(0.02*std::sqrt(1 - damp*damp)) / damp / getNaturalFrequency()
        );
    // critically damped
    else if (damp < 1.01)
        return scar::types::Time::fromSeconds(5.8 / getNaturalFrequency());
    // overdamped
    else if (damp < 1.1)
        return scar::types::Time::fromSeconds((5.8 + (damp - 1.01) * 1.1 / 0.09) / getNaturalFrequency());
    // "more" overdamped
    else if (damp < 5)
        return scar::types::Time::fromSeconds((6.9 + (damp - 1.1) * (38.8 - 6.9) / 4.9) / getNaturalFrequency());
    // "really" overdamped
    else
        return scar::types::Time::fromSeconds(38.8 / getNaturalFrequency());
}

double SettingsPIDFirstOrder::getOvershoot() const
{
    double damp = getDamping();
    if (std::isnan(damp))
        return 0;
    // underdamped
    else if (damp < 0.99)
        return std::exp( -damp * M_PI / std::sqrt(1 - damp*damp) );
    else
        return 0;
}

SettingsPIDFirstOrder SettingsPIDFirstOrder::createFromSpecT(double settling_time)
{
    if (settling_time <= 0)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createT) settling time must be positive");
    double kp = 4 / settling_time;
    double ki = 0;
    double kd = 0;
    SettingsPIDFirstOrder ret(kp, ki, kd);
    ret.wc = kp;
    return ret;
}

SettingsPIDFirstOrder SettingsPIDFirstOrder::createFromSpecT(double overshoot, double settling_time)
{
    if (overshoot < 0)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createT) overshoot must lie in [0, 0.5]");
    else if (overshoot > 0.5)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createT) overshoot must lie in [0, 0.5]");
    if (settling_time <= 0)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createT) settling time must be positive");
    double damp, cutoff;
    if (overshoot > 0)
    {
        double tmp = std::log(overshoot);
        damp = -tmp / std::sqrt(M_PI*M_PI + tmp*tmp);
        cutoff = linear_system::resonant2cutoff(-std::log(0.02 * std::sqrt(1-damp*damp)) / damp / settling_time, damp);
    }
    else
    {
        damp = 1;
        cutoff = 5.8337 / settling_time;
    }
    return createFromSpecF(damp, cutoff);
}

SettingsPIDFirstOrder SettingsPIDFirstOrder::createFromSpecF(double damping, double cutoff)
{
    if (damping <= 0)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createF) damping must lie in (0, 2]");
    else if (damping > 2)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createF) damping must lie in (0, 2]");
    if (cutoff <= 0)
        throw std::logic_error("[ERROR] (SettingsPIDFirstOrder::createF) cutoff frequency must be positive");
    double wn = linear_system::cutoff2resonant(cutoff, damping);
    double kp = 2 * damping * wn;
    double ki = wn * wn;
    double kd = 0;
    SettingsPIDFirstOrder ret(kp,ki,kd);
    ret.gain_antiwidnup = 1 / ki;
    ret.weight_reference = 1;
    ret.damp = damping;
    ret.wn = wn;
    ret.wc = cutoff;
    return ret;
}
