#include "scar/controller/SettingsPID.hpp"
#include "scar/linear-system/HelperFunctions.hpp"
#include <cmath>


using namespace ::scar::controller;


SettingsPID::SettingsPID(double kp, double ki, double kd) :
    kp(kp), ki(ki), kd(kd),
    weight_reference(1),
    gain_antiwidnup(0)
{
}

double SettingsPID::getSuggestedDerivativeCutoff() const
{
    return 4 * kd;
}

scar::types::Time SettingsPID::getSuggestedSampling() const
{
    return scar::types::Time::fromSeconds(std::max(2 * M_PI / getSuggestedDerivativeCutoff() / 100, 1e-5));
}

scar::types::Time SettingsPID::getSettlingTime() const
{
    return scar::types::Time();
}
