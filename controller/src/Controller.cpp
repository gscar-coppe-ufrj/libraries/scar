#include "scar/controller/Controller.hpp"
#include <iostream>
#include <limits>


using namespace ::scar::controller;


Controller::Controller(unsigned int N_controllers) :
    output_default( Eigen::VectorXd::Zero(N_controllers) ),
    cb_postprocessing(nullptr), cb_saturation(nullptr), _N(N_controllers)
{
    restart();
}

void Controller::setDefaultOutput(const Output &out)
{
    if (out.rows()*out.cols() != size())
        std::logic_error("[ERROR] (Controller::setDefaultOutput) argument size differs from "
                         "the controller size");
    output_default = out;
}

const Output &Controller::getDefaultOutput() const
{
    return output_default;
}

void Controller::restart()
{
    time_last = scar::types::Time();
}

void Controller::setCallbackPostProcessing(Controller::CallbackPostProcessing callback)
{
    cb_postprocessing = callback;
}

void Controller::setCallbackSaturation(Controller::CallbackSaturation callback)
{
    cb_saturation = callback;
}

void Controller::configureFirstRun(const types::Time &time, Input &, Input &)
{
    time_last = time;
    output = getDefaultOutput();
    output_presat = output;
}

void Controller::update(const types::Time &time, Input ref, Input signal)
{
    if (ref.size() != _N || signal.size() != _N)
    {
        std::stringstream error;
        error << "[ERROR] (Controller::update) the size of at least one argument != "
              << _N << ", which is the controller dimension";
        throw std::logic_error(error.str());
    }
    if (time_last.isNull())
        configureFirstRun(time, ref, signal);
    else if (time <= time_last)
    {
        if (time < time_last)
            std::cerr << "[WARN] (Controller::update) update called for a past time; output will not change" << std::endl;
        return;
    }
    output = updateControl(time, ref, signal);
    if (cb_postprocessing)
        cb_postprocessing(output);
    output_presat = output;
    if (cb_saturation)
        cb_saturation(output);
    time_last = time;
}
