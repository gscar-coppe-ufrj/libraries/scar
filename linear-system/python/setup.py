from distutils.core import setup

import sys
if sys.version_info < (3,0):
  sys.exit('Sorry, Python < 3.0 is not supported')

setup(
    name        = '${SCAR_CURRENT_TARGET}',
    version     = '${PROJECT_VERSION}', # TODO: might want to use commit ID here
    packages    = [ '${SCAR_CURRENT_TARGET}' ],
    package_dir = {
        '': '${CMAKE_CURRENT_BINARY_DIR}/python'
    },
    package_data = {
        '': ['${SCAR_CURRENT_TARGET_PY}.so']
    }
)
