# ==============================================================================
# Initialization
#

set(LIBNAME "linear-system")
string(TOUPPER "${LIBNAME}" LIBNAME_UPPER)

SCAR_SET_SOURCES_RECURSE(${LIBNAME})
SCAR_SET_HEADERS_RECURSE(${LIBNAME})

# ==============================================================================
# Create libraries and executables
#

find_package(yaml-cpp QUIET)
find_package(Eigen3 REQUIRED)

add_library(${LIBNAME} SHARED "${SCAR_${LIBNAME_UPPER}_SOURCES}")
add_library(${SCAR_NAMESPACE}::${LIBNAME} ALIAS ${LIBNAME})
target_link_libraries(${LIBNAME} ${SCAR_NAMESPACE}::types)
target_include_directories(${LIBNAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    "${EIGEN3_INCLUDE_DIRS}"
)

# ==============================================================================
# Install this component
#

SCAR_INSTALL(${LIBNAME})
SCAR_GENERATE_PACKAGE(${LIBNAME} "eigen3" "" "-std=c++11")
SCAR_GENERATE_PYTHON_BINDINGS(${LIBNAME})

# ==============================================================================
# Unit tests using GTest
#

if (SCAR_ENABLE_TESTS AND yaml-cpp_FOUND)
    configure_file(
        "test/gtest_LinearSystem.cpp"
        "${CMAKE_CURRENT_BINARY_DIR}/test/gtest_LinearSystem.cpp"
        @ONLY
    )
    add_executable(gtest-${LIBNAME}-LinearSystem "${CMAKE_CURRENT_BINARY_DIR}/test/gtest_LinearSystem.cpp")
    target_include_directories(gtest-${LIBNAME}-LinearSystem PRIVATE ${GTEST_INCLUDE_DIRS} ${YAML_CPP_INCLUDE_DIRS})
    target_link_libraries(gtest-${LIBNAME}-LinearSystem ${GTEST_LIBRARIES} ${YAML_CPP_LIBRARIES} pthread ${LIBNAME})
    gtest_discover_tests(gtest-${LIBNAME}-LinearSystem)

    add_executable(gtest-${LIBNAME}-HelperFunctions "${CMAKE_CURRENT_SOURCE_DIR}/test/gtest_HelperFunctions.cpp")
    target_include_directories(gtest-${LIBNAME}-HelperFunctions PRIVATE ${GTEST_INCLUDE_DIRS})
    target_link_libraries(gtest-${LIBNAME}-HelperFunctions ${GTEST_LIBRARIES} pthread ${LIBNAME})
    gtest_discover_tests(gtest-${LIBNAME}-HelperFunctions)
endif ()
