/**
 * @file HelperFunctions.hpp
 * @author LEAD
 * @brief Defines and declares functions necessaries to implement a linear system.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include <cmath>
#include <eigen3/Eigen/Eigen>

#include "scar/types/Poly.hpp"


namespace scar
{
namespace linear_system
{


/**
 * @brief NchooseK Combination of N items taken K at a time.
 * @param N number of choices
 * @param K number of selected choices
 * @return The binomial coefficient
 */
unsigned int nchoosek(unsigned int N, unsigned int K);

/**
 * @brief PolynomialDegree The order of a polynomial.
 * @param P the polynomial
 * @return The order
 */
template<typename Derived>
unsigned int polyDegree(const Eigen::MatrixBase<Derived> & P, double tol = 10 * std::numeric_limits<double>::epsilon())
{
    int ret = 0;
    for (unsigned int i = 0; i < P.size(); i++)
    {
        if (std::fabs(P(i)) > tol)
        {
            ret = P.size() - i - 1;
            break;
        }
    }
    return ret;
}

template<typename Derived>
Eigen::VectorXd shiftLeftVector(const Eigen::MatrixBase<Derived> & V, int n)
{
    int s = V.size();
    Eigen::VectorXd ret = Eigen::VectorXd::Zero(s);
    ret.head(s-n) = V.tail(s-n);
    return ret;
}

/*!
 * \brief PolynomialDivision Performs a polynomial division such that N/D = q/D + r
 *
 * Note that q and r should have the same size as D prior to calling this function.
 *
 * \param N Numerator
 * \param D Denominator
 * \param q Quotient
 * \param r Remainder
 */
template<typename Derived>
void polyDivision(const Eigen::MatrixBase<Derived> & N, const Eigen::MatrixBase<Derived> & D, Eigen::MatrixBase<Derived> & q, Eigen::MatrixBase<Derived> & r)
{
    unsigned int s = D.size();

    // These instructions only assert that the new size equals the old size, and do nothing else
    // This is so because N, D are MatrixBase objects
    q.resize(s);
    r.resize(s);

    q.setZero();
    r.setZero();
    unsigned int i = 0;

    Eigen::VectorXd N_aux(s);
    N_aux = N;
    Eigen::VectorXd D_aux(s);
    int degD = polyDegree(D);
    int degN_aux = polyDegree(N_aux);
    int n = degN_aux - degD;
    double a;

    while (n >= 0)
    {
        D_aux = shiftLeftVector(D,n);
        a = N_aux(i)/D_aux(i);
        q(s-n-1) = a;
        D_aux *= a;
        N_aux -= D_aux;
        i ++;
        n = polyDegree(N_aux) - degD;
    }
    r = N_aux;
}

template<typename Derived>
scar::types::Poly polyProduct(const Eigen::MatrixBase<Derived> & poly_a, const Eigen::MatrixBase<Derived> & poly_b)
{
    auto size_a = poly_a.size();
    auto size_b = poly_b.size();

    scar::types::Poly result(size_a + size_b - 1);
    result.setZero();

    for (Eigen::Index ind_a = 0; ind_a < size_a; ++ind_a)
    {
        auto val_a = poly_a(ind_a);
        for (Eigen::Index ind_b = 0; ind_b < size_b; ++ind_b)
        {
            result(ind_a + ind_b) += val_a * poly_b(ind_b);
        }
    }

    return result;
}

/*!
 * \brief Wraps the angle to the (-pi,pi] interval
 *
 * \param ang angle to be wrapped
 */
void wrap2pi(double &ang);
void wrap2pi(Eigen::VectorXd &ang);

/**
 * @brief Computes the cutoff frequency of a nominal second-order system
 * given the resonant frequency \p w and damping coefficient \p damp.
 * @param w Resonant frequency.
 * @param damp Damping coefficient.
 * @return Cutoff frequency.
 */
double resonant2cutoff(double w, double damp);

/**
 * @brief Computes the resonant frequency of a nominal second-order system
 * given the cutoff frequency \p w and damping coefficient \p damp.
 * @param w Cutoff frequency.
 * @param damp Damping coefficient.
 * @return Resonant frequency.
 */
double cutoff2resonant(double w, double damp);


}
}
