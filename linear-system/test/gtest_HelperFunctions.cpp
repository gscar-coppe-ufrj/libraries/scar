#include <gtest/gtest.h>
#include "scar/linear-system/HelperFunctions.hpp"

using namespace ::scar::linear_system;
using namespace ::scar::types;


static const double tol = 10 * std::numeric_limits<double>::epsilon();


TEST(LinearSystemTest, testPolyProduct)
{
    Poly p1(3);
    Poly p2(5);

    p1 << 1, 2, 3;
    p2 << 1, 2, 3, 4, 5;

    Poly res_nom(7);
    res_nom << 1, 4, 10, 16, 22, 22, 15;

    auto res = polyProduct(p1, p2);
    ASSERT_LE((res_nom - res).array().abs().maxCoeff(), tol);

    Poly p3(1), p4(1);
    p3 << 1;
    p4 << 1;
    res = polyProduct(p3, p4);
    ASSERT_LE((res - p3).array().abs().maxCoeff(), tol);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
