#include "scar/rbd/FrameRelation.hpp"


using namespace ::scar::rbd;


const Eigen::Vector6d FrameRelation::Vector6dNaN = Eigen::Vector6d::Constant( std::numeric_limits<double>::quiet_NaN() );

FrameRelation::FrameRelation() :
    FrameRelation("", "")
{
}

FrameRelation::FrameRelation(const std::string& source, const std::string& target) :
    source_frame(source),
    target_frame(target),
    mode_data(Target),
    mode_computations(Dynamic),
    flag_rpy_derivatives(true),
    flag_target(mode_data == Target)
{
}

void FrameRelation::convert()
{
    R = rpy2rot(rpy);
    switch (mode_computations)
    {
    case Kinematic:
        convertDirectKinematics();
        break;
    case DifferentialKinematic:
        convertDirectKinematics();
        convertDifferentialKinematics();
        break;
    case Dynamic:
        convertDirectKinematics();
        convertDifferentialKinematics();
        dot_R = dotRotationRepRPY(rpy, dot_rpy);
        dot_T = dotJacobianRepRPY(rpy, dot_rpy);
        dot_J = dotJacobian(dot_R, dot_T);
        if (flag_target)
            ddot_pose = J * ddot_pose_target + dot_J * dot_pose_target;
        ddot_pos = ddot_pose.head<3>();
        ddot_rpy = ddot_pose.tail<3>();
        break;
    }
}

void FrameRelation::update(const Eigen::Vector6d& pose, const Eigen::Vector6d& dot_pose, const Eigen::Vector6d& ddot_pose)
{
    switch (mode_data)
    {
    case Source:
        pos = pose.head<3>();
        rpy = pose.tail<3>();
        this->pose = pose;
        this->dot_pose = dot_pose;
        this->ddot_pose = ddot_pose;
        break;
    case Target:
        pos_target = pose.head<3>();
        rpy = pose.tail<3>();
        dot_pose_target = dot_pose;
        ddot_pose_target = ddot_pose;
        break;
    default:
        throw std::logic_error("FrameRelation::update - unknown data mode");
        break;
    }
    convert();
}
