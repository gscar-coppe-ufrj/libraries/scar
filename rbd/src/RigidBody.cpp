#include "scar/rbd/RigidBody.hpp"


using namespace ::scar::rbd;


RigidBody::RigidBody() : RigidBody("", "")
{
}

RigidBody::RigidBody(const std::string& source, const std::string& target) : FrameRelation(source, target),
    mass(std::numeric_limits<double>::quiet_NaN())
{
    gravity_direction << 0, 0, -1;
}

void RigidBody::calc(bool calc_M, bool calc_C, bool calc_G)
{
    R = scar::rbd::rpy2rot(rpy);
    if (calc_M || calc_C)
    {
        inv_R = R.transpose();
        dot_R = scar::rbd::dotRotationRepRPY(rpy, dot_rpy);
        dot_inv_R = dot_R.transpose();
        inv_T = scar::rbd::inverseJacobianRepRPY(rpy);
        dot_inv_T = scar::rbd::dotInverseJacobianRepRPY(rpy, dot_rpy);
    }

    if (calc_M)
        M_pose << mass * inv_R, Eigen::Matrix3d::Zero(), Eigen::Matrix3d::Zero(), inertia_matrix * inv_T;
    else
        M_pose = Eigen::Matrix6d::Zero();

    if (calc_C)
        C_pose << mass * dot_inv_R,
            -mass * scar::rbd::hat( inv_R * dot_pos ) * inv_T,
            -mass * inv_R * scar::rbd::hat(dot_pos),
            -scar::rbd::hat(inertia_matrix * inv_T * dot_rpy)
                * inv_T + inertia_matrix * dot_inv_T;
    else
        C_pose = Eigen::Matrix6d::Zero();

    if (calc_G)
    {
        G_pose << - inv_R * gravity_direction, 0, 0, 0;
        G_pose = mass * GRAVITY * G_pose;
    }
    else
        G_pose = Eigen::Vector6d::Zero();
}
