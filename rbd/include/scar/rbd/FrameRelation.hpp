/**
 * @file FrameRelation.hpp
 * @author LEAD
 * @brief Defines functions to convert data from one frame to another.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include "scar/rbd/GeneralFunctions.hpp"
#include <Eigen/Eigen>


namespace Eigen
{
    typedef Matrix<double, 6, 6> Matrix6d;
    typedef Matrix<double, 6, 1> Vector6d;
}


namespace scar
{
namespace rbd
{


/**
 * @brief Converts the data of frame \f$ E_{target} \f$ relative to \f$ E_{source} \f$,
 * represented in \f$ E_{target} \f$. Needs \f$ rpy \f$, \f$ V^{source,target}_{st} \f$
 * and \f$ \dot{V}^{source,target}_{st} \f$, where the upperscript \f${}^{source,target}\f$
 * denotes that the twist or its derivative can be written on the source or target frames.
 *
 * \f[ pose = \left[\begin{array}{c}
 * p^{s}_{st} \\
 * \phi \\ \theta \\ \psi \end{array}\right] \\
 * \dot{pose} =  J_{st} V^t_{st} \\
 * \ddot{pose} = J_{st} \dot{V}^s_{st} + \dot{J}_{st} V^t_{st} \f]
 *
 * @param frData Object of class FrameRelation with data between two frames.
 */
class FrameRelation
{
public:
    const static Eigen::Vector6d Vector6dNaN;

    /**
     * Choose the frame on which the position, the twist and the twist derivative provided to this
     * class are written on; the orientation always corresponds the rotation matrix R from the source
     * to the target (i.e. the target frame written on the source frame), such that \f$ v_{source} = R * v_{target} \f$
     * @see setMode
     * @see update
     */
    enum ModeData
    {
        Source,
        Target
    };

    /**
     * Choose Kinematic to compute only the rotation matrix, DifferentialKinematic to compute the jacobians,
     * and Dynamic to compute the rotation matrix and jacobians first derivatives w.r.t. time
     * @see setMode
     */
    enum ModeComputations
    {
        Kinematic,
        DifferentialKinematic,
        Dynamic
    };

private:
    /** Name of the source frame (typically the inertial frame or the previous link on a serial-link manipulator) */
    std::string source_frame;

    /** Name of the target frame (typically the body frame or the current link on a serial-link manipulator) */
    std::string target_frame;

    /**
     * Mode used when reading data
     * @see update
     */
    ModeData mode_data;

    /** Mode used when computing the rotation matrix and jacobians */
    ModeComputations mode_computations;

    /** Indicates if \p dot_pose and \p ddot_pose in #update receive angular data roll, pitch, and yaw; defaults to true */
    bool flag_rpy_derivatives;

    /** True if mode_data equals Target */
    bool flag_target;

    /**
     * @brief Convert data from the source to the target frame, or vice-versa, depending on the current computation mode
     * @see ModeComputations, setMode
     */
    void convert();

    /**
     * @brief Assuming all needed data has been correctly set, computes all data needed for direct kinematics
     */
    inline void convertDirectKinematics()
    {
        if (flag_target)
        {
            pos = R * pos_target;
            pose << pos, rpy;
        }
    }

    /**
     * @brief Assuming all needed data has been correctly set, computes all data needed for differential kinematics
     */
    inline void convertDifferentialKinematics()
    {
        T = jacobianRepRPY(rpy);
        J = jacobian(R, T);
        if (flag_target)
        {
            pos = R * pos_target;
            dot_pose = J * dot_pose_target;
        }
        dot_pos = dot_pose.head<3>();
        dot_rpy = dot_pose.tail<3>();
    }

public:
    /** Linear position, from source to target, written on the target frame */
    Eigen::Vector3d pos_target;

    /** Pose with roll, pitch and yaw representation in m and rad */
    Eigen::Vector6d pose;

    /** Linear position, from source to target, written on the source frame */
    Eigen::Vector3d pos;

    /** Roll, pitch and yaw in rad */
    Eigen::Vector3d rpy;

    /** Time derivative of the pose with Roll, pitch and yaw representation in m/s and rad/s */
    Eigen::Vector6d dot_pose;

    /** Linear velocity in m/s */
    Eigen::Vector3d dot_pos;

    /** Time derivative of roll, pitch yaw orientation representation in rad/s */
    Eigen::Vector3d dot_rpy;

    /** Second time derivative of the pose with Roll, pitch and yaw representation in m/s2 and rad/s2 */
    Eigen::Vector6d ddot_pose;

    /** Linear acceleration in m/s2 */
    Eigen::Vector3d ddot_pos;

    /** Second time derivative of roll, pitch yaw orientation representation in rad/s2 */
    Eigen::Vector3d ddot_rpy;

    /** Body velocity twist in m/s and rad/s*/
    Eigen::Vector6d dot_pose_target;

    /** Body linear velocity in m/s */
    Eigen::Vector3d dot_pos_target;

    /** Body angular velocity in rad/s */
    Eigen::Vector3d omega;

    /** Body acceleration twist in m/s2 and rad/s2*/
    Eigen::Vector6d ddot_pose_target;

    /** Body linear acceleration in m/s2 */
    Eigen::Vector3d ddot_pos_target;

    /** Body angular acceleration in rad/s2 */
    Eigen::Vector3d dot_omega;

    /** Rotation matrix */
    Eigen::Matrix3d R;

    /** Inverse of the rotation matrix */
    Eigen::Matrix3d inv_R;

    /** Time derivative of the rotation matrix */
    Eigen::Matrix3d dot_R;

    /** Time derivative of the inverse of the rotation matrix*/
    Eigen::Matrix3d dot_inv_R;

    /** Homogeneous transformation */
    Eigen::Matrix4d g;

    /** Adjoint map */
    Eigen::Matrix6d Ad;

    /** Inverse adjoint map */
    Eigen::Matrix6d inv_Ad;

    /** Representation Jacobian for RPY */
    Eigen::Matrix3d T;

    /** Inverse representation Jacobian for RPY */
    Eigen::Matrix3d inv_T;

    /** Time derivative of the representation Jacobian for RPY */
    Eigen::Matrix3d dot_T;

    /** Time derivative of the inverse representation Jacobian for RPY */
    Eigen::Matrix3d dot_inv_T;

    /** Velocity transformation matrix */
    Eigen::Matrix6d J;

    /** Inverse velocity transformation matrix */
    Eigen::Matrix6d inv_J;

    /** Time derivative of the velocity transformation matrix */
    Eigen::Matrix6d dot_J;

    /** Time derivative of the inverse velocity transformation matrix */
    Eigen::Matrix6d dot_inv_J;

    /**
     * @brief Default constructor
     */
    FrameRelation();

    /**
     * @brief Constructs a frame relation between frames \p source and \p target
     * @param source Source frame
     * @param target Target frame
     */
    FrameRelation(const std::string& source, const std::string& target);

    /**
     * @brief Selects modes for the data and computations settings
     *
     * If never called, default values are \p mode_data = Source and \p mode_computations = Dynamic
     *
     * @param mode_data Data mode
     * @param mode_computations Computations mode
     * @param angvel true if the pose first and second derivatives (\p dot_pose and \p ddot_pose) are
     * passed to #update with angular velocities and angular accelerations instead of roll, pitch, and
     * yaw derivatives; defaults to false
     * @see ModeData, ModeComputations, update
     */
    inline void setMode(ModeData mode_data, ModeComputations mode_computations, bool angvel = false)
    {
        this->mode_data = mode_data;
        this->mode_computations = mode_computations;
        flag_target = this->mode_data == Target;
        flag_rpy_derivatives = !angvel;
    }

    /**
     * @brief Configures this frame data
     * @param pos Position in the source or target frame, depending on the data mode
     * @param rpy Roll, pitch, and yaw angles for the rotation matrix (R) that
     * represents the source frame written on the target frame; this rotation is such
     * that \f$ v_s = R v_t \f$, where \f$ v_s \f$ and \f$ v_t \f$ are vectors on the
     * source and target frames, respectively
     * @param dot_pose Pose vector derivative; first three elements are the linear
     * velocity (source or target frame) and the last three are the RPY time derivative
     * (if the data mode is set to #Source) or the angular velocity (if the data mode is set to #Target)
     * @param ddot_pose Pose vector second derivative; follows the same construction as \p dot_pose
     */
    void update(const Eigen::Vector6d& pose, const Eigen::Vector6d& dot_pose = Vector6dNaN, const Eigen::Vector6d& ddot_pose = Vector6dNaN);
};


}
}
