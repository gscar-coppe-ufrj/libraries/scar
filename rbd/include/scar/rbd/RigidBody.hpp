/**
 * @file RigidBody.hpp
 * @author LEAD
 * @brief Defines a rigid body and calculates its inertia tensor, Coriolis matrices and Gravity vector according to a pose.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include "scar/rbd/FrameRelation.hpp"


namespace scar
{
namespace rbd
{


const double GRAVITY = 9.80665;


/**
 * @brief The RigidBody class
 */
class RigidBody : public FrameRelation
{
public:
    RigidBody();
    RigidBody(const std::string& source, const std::string& target);

    /** Body mass */
    double mass;

    /** Distance from the frame to the Center of Gravity, written in the frame of reference */
    Eigen::Vector3d distance_frame2cog_frame;

    /** Generalized inertia tensor written in the frame of reference */
    Eigen::Matrix3d inertia_matrix;

    Eigen::Matrix6d M_pose;
    Eigen::Matrix6d C_pose;
    Eigen::Vector6d G_pose;
    Eigen::Vector3d gravity_direction;

    void calc(bool calc_M = true, bool calc_C = true, bool calc_G = false);
};


}
}
