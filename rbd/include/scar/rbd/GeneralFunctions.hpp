/**
 * @file GeneralFunctions.hpp
 * @author LEAD
 * @brief Define and declare functions that are used in \a FrameRelation and \a RigidBody.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include <Eigen/Eigen>


namespace Eigen
{
    typedef Matrix<double, 6, 6> Matrix6d;
    typedef Matrix<double, 6, 1> Vector6d;
}


namespace scar
{
namespace rbd
{


/*!
 * \brief Returns the elementary rotation matrix corresponing to a rotation of a frame around the x axis.
 *
 * \f[
 * R_{x}(\theta) = \left[\begin{array}{ccc}
 * 1 & 0 & 0 \\
 * 0 & c(\theta) & -s(\theta) \\
 * 0 & s(\theta) & c(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle around the x axis
 * \return Rotation matrix
 */
Eigen::Matrix3d rotX(double ang);

/*!
 * \brief Returns the elementary rotation matrix corresponing to a rotation of a frame around the y axis.
 *
 * \f[
 * R_{y}(\theta) = \left[\begin{array}{ccc}
 * c(\theta) & 0 & s(\theta) \\
 * 0 & 1 & 0 \\
 * -s(\theta) & 0 & c(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle around the y axis
 * \return Rotation matrix
 */
Eigen::Matrix3d rotY(double ang);

/*!
 * \brief Returns the elementary rotation matrix corresponing to a rotation of a frame around the z axis.
 *
 * \f[
 * R_{z}(\theta) = \left[\begin{array}{ccc}
     * c(\theta) & -s(\theta) & 0 \\
     * s(\theta) & c(\theta) & 0 \\
     * 0 & 0 & 1
     * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle around the x axis
 * \return Rotation matrix
 */
Eigen::Matrix3d rotZ(double ang);

/*!
 * \brief Returns the rotation matrix corresponing to a rotation of an angle \f$ \theta \f$ over an axis \f$ \vec{h} \f$.
 *
 * \f[
 * R(\theta , h) = I + \sin(\theta)\hat{h} + (1-\cos(\theta))\hat{h}^2
 * \f]
 *
 * \param ang Rotation angle around the h axis
 * \param h Axis of rotation
 * \return Rotation matrix
 */
template<typename Derived>
Eigen::Matrix3d rotAngleAxis(double ang, const Eigen::MatrixBase<Derived> & h)
{
    double c = cos(ang);
    double s = sin(ang);
    double v = 1 - c;
    Eigen::Matrix3d ret;
    ret << h(0) * h(0) * v + c, h(0) * h(1) * v - h(2) * s, h(0) * h(2) * v + h(1) * s,
            h(0) * h(1) * v + h(2) * s, h(1) * h(1) * v + c, h(1) * h(2) * v - h(0) * s,
            h(0) * h(2) * v - h(1) * s, h(1) * h(2) * v + h(0) * s, h(2) * h(2) * v + c;
    return ret;
}

/*!
 * \brief Calculates the skew-symmetric matrix of a vector
 *
 * Considering a vector \f$ v \in \mathbb{R}^3 \f$, the skew-symmetric matrix \f$ \hat{v} \in \mathbb{R}^{3 \times 3} \f$ can be written with the elements of \f$ v \f$:
 *
 * \f[
 * v = \left[\begin{array}{ccc} x \\ y \\ z \end{array}\right] \qquad
 * \hat{v} = \left[\begin{array}{ccc}
 * 0 & -z & y \\
 * z & 0 & -x \\
 * -y & x & 0
 * \end{array}\right]
 * \f]
 *
 * \param vec Vector
 * \return Skew-symmetric matrix
 */
template<typename Derived>
Eigen::Matrix3d hat(const Eigen::MatrixBase<Derived> & vec)
{
    Eigen::Matrix3d ret;
    ret << 0, -vec(2), vec(1),
            vec(2), 0, -vec(0),
            -vec(1), vec(0), 0;
    return ret;
}


/*!
 * \brief Calculates the skew-symmetric matrix of a vector
 *
 * Considering a vector \f$ v \in \mathbb{R}^3 \f$, the skew-symmetric matrix \f$ \hat{v} \in \mathbb{R}^{3 \times 3} \f$ can be written with the elements of \f$ v \f$:
 *
 * \f[
 * v = \left[\begin{array}{ccc} x \\ y \\ z \end{array}\right] \qquad
 * \hat{v} = \left[\begin{array}{ccc}
 * 0 & -z & y \\
 * z & 0 & -x \\
 * -y & x & 0
 * \end{array}\right]
 * \f]
 *
 * \param hatvec Skew-symmetric matrix of a Vector
 * \return Vector
 */
template<typename Derived>
Eigen::Matrix3d unhat(const Eigen::MatrixBase<Derived> & hatvec)
{
    Eigen::Vector3d ret;
    ret << hatvec(2 , 1), hatvec(0 , 2), hatvec(1 , 0);
    return ret;
}

/*!
 * \brief Calculates the partial derivative of the elementary rotation matrix of a rotation on an x axis
 *
 * Considering \f$ R_x \in \mathbb{R}^{3 \times 3} \f$ as a rotation around the x axis by the angle \f$ \theta \in \mathbb{R} \f$, the partial derivative of this matrix relative to \f$ \theta \f$ is:
 *
 * \f[
 * \frac{\partial R_{x}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * 0 & 0 & 0 \\
 * 0 & -s(\theta) & -c(\theta) \\
 * 0 & c(\theta) & -s(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle
 * \return Partial derivative of Rot_x
 */
Eigen::Matrix3d dotRotX(double ang);

/*!
 * \brief Calculates the partial derivative of the elementary rotation matrix of a rotation on a y axis
 *
 * Considering \f$ R_y \in \mathbb{R}^{3 \times 3} \f$ as a rotation around the y axis by the angle \f$ \theta \in \mathbb{R} \f$, the partial derivative of this matrix relative to \f$ \theta \f$ is:
 *
 * \f[
 * \frac{\partial R_{y}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * -s(\theta) & 0 & c(\theta) \\
 * 0 & 0 & 0 \\
 * -c(\theta) & 0 & -s(\theta)
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle
 * \return Partial derivative of Rot_y
 */
Eigen::Matrix3d dotRotY(double ang);

/*!
 * \brief Calculates the partial derivative of the elementary rotation matrix of a rotation on a z axis
 *
 * Considering \f$ R_z \in \mathbb{R}^{3 \times 3} \f$ as a rotation around the z axis by the angle \f$ \theta \in \mathbb{R} \f$, the partial derivative of this matrix relative to \f$ \theta \f$ is:
 *
 * \f[
 * \frac{\partial R_{z}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * -s(\theta) & -c(\theta) & 0 \\
 * c(\theta) & -s(\theta) & 0 \\
 * 0 & 0 & 0
 * \end{array}\right]
 * \f]
 *
 * \param ang Rotation angle
 * \return Partial derivative of Rot_z
 */
Eigen::Matrix3d dotRotZ(double ang);

/*!
 * \brief Wraps the angle to the (-pi , pi] interval
 *
 * \param ang angle to be wrapped
 */
void wrap2pi(double & ang);

/*!
 * \brief Wraps the angles to the (-pi , pi] interval
 *
 * \param ang vector of angles to be wrapped
 */
void wrap2pi(Eigen::VectorXd & ang);

unsigned int nchoosek(unsigned int N, unsigned int K);

template<typename Derived>
unsigned int getPolynomialDegree(const Eigen::MatrixBase<Derived> & P)
{
    int ret = 0;
    for (unsigned int i = 0; i < P.size(); i++)
    {
        if (fabs(P(i)) > 1e-8)
        {
            ret = P.size() - i - 1;
            break;
        }
    }
    return ret;
}

template<typename Derived>
Eigen::VectorXd shiftVectorLeft(const Eigen::MatrixBase<Derived> & V, int n)
{
    int s = V.size();
    Eigen::VectorXd ret = Eigen::VectorXd::Zero(s);
    ret.head(s-n) = V.tail(s-n);
    return ret;
}

/*!
 * \brief Performs a polynomial division such that N/D = q/D + r
 *
 * Note that q and r should have the same size as D prior to calling this function.
 *
 * \param N Numerator
 * \param D Denominator
 * \param q Quotient
 * \param r Remainder
 */
template<typename Derived>
void dividePolynomial(const Eigen::MatrixBase<Derived> & N, const Eigen::MatrixBase<Derived> & D, Eigen::MatrixBase<Derived> & q, Eigen::MatrixBase<Derived> & r)
{
    unsigned int s = D.size();

    // These instructions only assert that the new size equals the old size, and do nothing else
    // This is so because N, D are MatrixBase objects
    q.resize(s);
    r.resize(s);

    q.setZero();
    r.setZero();
    unsigned int i = 0;

    Eigen::VectorXd N_aux(s);
    N_aux = N;
    Eigen::VectorXd D_aux(s);
    int degD = getPolynomialDegree(D);
    int degN_aux = getPolynomialDegree(N_aux);
    int n = degN_aux - degD;
    double a;

    while (n >= 0)
    {
        D_aux = shiftVectorLeft(D , n);
        a = N_aux(i)/D_aux(i);
        q(s-n-1) = a;
        D_aux *= a;
        N_aux -= D_aux;
        i ++;
        n = getPolynomialDegree(N_aux) - degD;
    }
    r = N_aux;
}

/*!
 * \brief Converts from cartesian to cylindrical coordinates
 *
 * x = rho * cos(theta)       rho   = sqrt(x * x + y * y)
 * y = rho * sin(theta)  -->  theta = atan2(y, x)
 * z = z'                     z'    = z
 *
 * \param pos position in cartesian coordiantes
 * \return position in cylindrical coordinates (rho, theta, z)
 */
Eigen::Vector3d cartesian2cylindrical(const Eigen::Vector3d &pos);

/*!
 * \brief Converts from cartesian to spherical coordinates
 *
 * x = rho * cos(theta) * cos(psi)       rho   = sqrt(x * x + y * y + z * z)
 * y = rho * sin(theta) * cos(psi)  -->  theta = atan2(y, x)
 * z = rho * sin(psi)                    psi   = asin(z / rho)
 *
 * \param pos position in cartesian coordiantes
 * \return position in spherical coordinates (rho, theta, psi)
 */
Eigen::Vector3d cartesian2spherical(const Eigen::Vector3d &pos);

/*!
 * \brief Converts from spherical to cartesian coordinates
 *
 * x = rho * cos(theta) * cos(psi)       rho   = sqrt(x * x + y * y + z * z)
 * y = rho * sin(theta) * cos(psi)  <--  theta = atan2(y, x)
 * z = rho * sin(psi)                    psi   = asin(z / rho)
 *
 * \param pos position in spherical coordiantes
 * \return position in cartesian coordinates (x, y, z)
 */
Eigen::Vector3d spherical2cartesian(const Eigen::Vector3d &pos);

/*!
 * \brief Returns the Jacobian matrix that maps spherical coordinates velocities to cartesian space
 * \return the Jacobian matrix
 */
Eigen::Matrix3d spherical2cartesianJacobian(const Eigen::Vector3d &pos);

/*!
 * \brief Returns the Jacobian matrix derivative w.r.t. time
 * \return the Jacobian matrix derivative
 */
Eigen::Matrix3d spherical2cartesianDotJacobian(const Eigen::Vector3d &pos, const Eigen::Vector3d &vel);

/*!
 * \brief Returns the \f$ SO(3) \f$ rotation matrix based on the roll, picth, yaw (RPY) Euler angles.
 *
 * Considering the RPY angles as \f$ \phi, \theta, \psi \in \mathbb{R} \f$, the RPY (ZYX) rotation matrix \f$ R_{ab} \in SO(3) \f$ of a frame \f$ E_b \f$ relative to a reference frame \f$ E_a \f$ is:
 *
 * \f[ R_{ab}(\phi, \theta, \psi) = R_{z}(\psi)R_{y}(\theta)R_{x}(\phi) \f]
 *
 * The elementary rotations are described as:
 *
 * \f[
 * R_{x}(\phi) = \left[\begin{array}{ccc}
 * 1 & 0 & 0 \\
 * 0 & c(\phi) & -s(\phi) \\
 * 0 & s(\phi) & c(\phi)
 * \end{array}\right] \qquad
 * R_{y}(\theta) = \left[\begin{array}{ccc}
 * c(\theta) & 0 & s(\theta) \\
 * 0 & 1 & 0 \\
 * -s(\theta) & 0 & c(\theta)
 * \end{array}\right] \qquad
 * R_{z}(\psi) = \left[\begin{array}{ccc}
 * c(\psi) & -s(\psi) & 0 \\
 * s(\psi) & c(\psi) & 0 \\
 * 0 & 0 & 1
 * \end{array}\right] \f]
 *
 * \param rpy Vector of the roll, pitch and yaw angles.
 * \return Rotation matrix.
 */
template<typename Derived>
Eigen::Matrix3d rpy2rot(const Eigen::MatrixBase<Derived> & rpy)
{
    return rotZ(rpy(2)) * rotY(rpy(1)) * rotX(rpy(0));
}

/*!
 * \brief Returns the roll, picth, yaw Euler angles from a rotation matrix R.
 *
 * Considering the roll, picth, yaw as \f$ \phi, \theta, \psi \in \mathbb{R} \f$, the rotation \f$ R_{ab} = R_{z}(\psi)R_{y}(\theta)R_{x}(\phi) \in \mathbb{R}^{3 \times 3} \f$ is:
 *
 * \f[ R_{ab}(\phi, \theta, \psi) = \left[\begin{array}{ccc}
 * c(\psi) c(\theta) & * & * \\
 * c(\theta) s(\psi) & * & * \\
 * -s(\theta) & c(\theta) s(\phi) & c(\phi) c(\theta)
 * \end{array}\right] = \left[\begin{array}{ccc}
 * r_{11} & r_{12} & r_{13} \\
 * r_{21} & r_{22} & r_{23} \\
 * r_{31} & r_{32} & r_{33}
 * \end{array}\right] \f]
 *
 * The variables \f$\phi\f$, \f$\theta\f$, \f$\psi\f$ can be calculated through:
 *
 * \f[ \phi = atan2(r_{32},r_{33}) \qquad
 * \theta = atan2(-r_{31},\sqrt{r_{32}^2+r_{33}^2}) \qquad
 * \psi = atan2(r_{21},r_{11}) \f]
 *
 * \param R Rotation matrix.
 * \return Vector of the roll, picth, yaw Euler angles.
 */
template<typename Derived>
Eigen::Vector3d rot2rpy(const Eigen::MatrixBase<Derived> & R)
{
    double R_11 = R(0, 0);
    double R_12 = R(0, 1);
    double R_21 = R(1, 0);
    double R_22 = R(1, 1);
    double R_31 = R(2, 0);
    double R_32 = R(2, 1);
    double R_33 = R(2, 2);
    Eigen::Vector3d rpy;

    double epsilon=1E-12;
    rpy(1) = atan2(-R_31, sqrt(pow(R_32, 2) + pow(R_33, 2)));
    if ( fabs(rpy(1)) > (M_PI/2.0-epsilon) )
    {
        rpy(0) = 0.0; //set roll to zero when pitch is +-pi/2
        rpy(2) = atan2(-R_12, R_22);
    }
    else
    {
        rpy(0) = atan2(R_32, R_33);
        rpy(2) = atan2(R_21, R_11);
    }
    return rpy;
}

/*!
 * \brief Returns the Homogeneous Transformation matrix.
 *
 * Considering the translation vector \f$ p_{ab}^a \in \mathbb{R}^3 \f$ and the rotation \f$ R_{ab} \in SO(3) \f$, the homogeneous transformation  \f$ g_{ab} \in \mathbb{R}^{4 \times 4} \f$
 * between frames \f$ E_b \f$ and \f$ E_a \f$ is:
 *
 * \f[ g_{ab} = \begin{bmatrix} R_{ab} & p_{ab}^a \\ \mathbf{0} & 1 \end{bmatrix} \f]
 *
 * \param p Translation vector.
 * \param R Rotation matrix.
 * \return Homogeneous Transformation matrix.
 */
template<typename Derived1, typename Derived2>
Eigen::Matrix4d homogeneous(const Eigen::MatrixBase<Derived1> & p, const Eigen::MatrixBase<Derived2> & R)
{
    Eigen::Matrix4d g;
    g.block<3, 3>(0, 0) = R;
    g.block<3, 1>(0, 3) = p;
    g.row(3) << 0, 0, 0, 1;
    return g;
}

/*!
 * \brief Returns the Adjoint Map of an Homogenous Transformation matrix that transforms a twist written in body coordinates to spatial coordinates.
 *
 * Consider a reference frame \f$ E_a \f$ and the frame \f$ E_b \f$ of a rigid body. Now, consider the velocity twist of \f$ E_b \f$ relative to \f$ E_a \f$.
 * Then, the Adjoint matrix maps the velocity twist represented on the local frame to spatial coordinates:
 *
 * \f[ \begin{bmatrix} v_{ab}^S \\ \omega_{ab}^S \end{bmatrix} =
 * \underbrace{\left[\begin{array}{cc}
 * R_{ab} & \widehat{p}_{ab}^aR_{ab} \\
 * \mathbf{0} & R_{ab} \end{array}\right]}_{Ad_{g_{ab}}}
 * \begin{bmatrix} v_{ab}^b \\ \omega_{ab}^b \end{bmatrix} \f]
 *
 * \param p Translation vector
 * \param R Rotation between the two frames
 * \return Adjoint map
 */
template<typename Derived1, typename Derived2>
Eigen::Matrix6d adjoint(const Eigen::MatrixBase<Derived1> & p, const Eigen::MatrixBase<Derived2> & R)
{
    Eigen::Matrix6d Ad;
    Ad.block<3, 3>(0, 0) = Ad.block<3, 3>(3, 3) = R;
    Ad.block<3, 3>(0, 3) = hat(p) * R;
    Ad.block<3, 3>(3, 0) = Eigen::Matrix3d::Zero();
    return Ad;
}

/*!
 * \brief Returns Jacobian representation matrix that transforms the body angular velocity into the rate of roll, picth, yaw representation angles.
 *
 * Considering the frames \f$ E_a \f$ and \f$ E_b \f$, roll, picth, yaw angles as \f$ \phi, \theta, \psi \in \mathbb{R} \f$, and \f$ \omega^b_{ab} \in \mathbb{R}^3 \f$ the body angular velocity of frame \f$ E_b \f$ relative to \f$ E_a \f$, the Jacobian \f$ T_{ab} \in \mathbb{R}^{3 \times 3} \f$ can be written as:
 *
 * \f[ \left[\begin{array}{c}
 * \dot{\phi} \\
 * \dot{\theta} \\
 * \dot{\psi} \end{array}\right] =
 * \underbrace{\left[\begin{array}{ccc}
 * 1 & \frac{s(\phi)s(\theta)}{c(\theta)} & \frac{c(\phi)s(\theta)}{c(\theta)} \\
 * 0 & c(\phi) & -s(\phi) \\
 * 0 & \frac{s(\phi)}{c(\theta)} & \frac{c(\phi)}{c(\theta)}
 * \end{array}\right]}_{T_{ab}}
 * \omega_{ab}^b \f]
 *
 * Note that this Jacobian has a singularity in \f$ \theta = \pm\pi/2 \f$, which is the singulatiry of the roll, picth, yaw representation, which should be avoided.
 *
 * \param rpy Vector of the roll, pitch and yaw angles.
 * \return Representation Jacobian matrix of the roll, picth, yaw representation.
 */
template<typename Derived>
Eigen::Matrix3d jacobianRepRPY(const Eigen::MatrixBase<Derived> & rpy)
{
    double s_phi = sin(rpy(0));
    double c_phi = cos(rpy(0));
    double s_theta = sin(rpy(1));
    double c_theta = cos(rpy(1));
    Eigen::Matrix3d T;
    T << 1, s_phi * s_theta/c_theta, c_phi * s_theta/c_theta,
            0, c_phi, -s_phi,
            0, s_phi/c_theta, c_phi/c_theta;
    return T;
}

/*!
 * \brief Returns the velocity transformation matrix that transforms the body of frame \f$ E_b \f$ relative to \f$ E_a \f$ into the time derivative of the pose represented in \f$ E_a \f$.
 *
 * Given the body angular velocity \f$ \omega_{ab}^b \in \mathbb{R}^3 \f$, the body linear velocity \f$ v_{ab}^b \in \mathbb{R}^3 \f$, the linear velocity \f$ v_{ab}^a \in \mathbb{R}^3 \f$, and
 * \f$ \dot{\phi}, \dot{\theta}, \dot{\psi} \in \mathbb{R} \f$ as the roll, picth, yaw angles rates, the velocity transformation matrix \f$ J_{ab} \f$ is expressed as:
 *
 * \f[ \left[\begin{array}{c}
 * v_{ab}^a \\
 * \dot{\phi} \\
 * \dot{\theta} \\
 * \dot{\psi} \end{array}\right] =
 * \underbrace{\left[\begin{array}{cc}
 * R_{ab} & \mathbf{0} \\
 * \mathbf{0} & T_{ab} \end{array}\right]}_{J_{ab}}
 * \left[\begin{array}{c}
 * v_{ab}^b \\
 * w_{ab}^b \end{array}\right] \f]
 *
 * \param R Rotation matrix.
 * \param T Representation Jacobian matrix of the roll, picth, yaw representation.
 * \return Velocity transformation matrix.
 */
template<typename Derived>
Eigen::Matrix6d jacobian(const Eigen::MatrixBase<Derived> & R, const Eigen::MatrixBase<Derived> & T)
{
    Eigen::Matrix6d J;
    J.block<3, 3>(0, 0) = R;
    J.block<3, 3>(3, 3) = T;
    J.block<3, 3>(3, 0) = J.block<3, 3>(0, 3) = Eigen::Matrix3d::Zero();
    return J;
}

/*!
 * \brief Returns the inverse of the Adjoint Map, which is equivalent to the adjoint map of the inverse homogeneous transformation matrix.
 *
 * \f[ Ad^{-1}_{g_{ab}} = Ad_{g_{ba}} =
 * \left[\begin{array}{cc}
 * R_{ab}^T & -R_{ab}^T\widehat{p}_{ab}^a \\
 * \mathbf{0} & R_{ab}^T \end{array}\right] \f]
 *
 * \param p Translation vector
 * \param R Rotation between the two frames
 * \return Inverse of the Adjoint Map
 */
template<typename Derived1, typename Derived2>
Eigen::Matrix6d inverseAdjoint(const Eigen::MatrixBase<Derived1> & p, const Eigen::MatrixBase<Derived2> & R)
{
    Eigen::Matrix6d invAd;
    invAd.block<3, 3>(0, 0) = invAd.block<3, 3>(3, 3) = R.transpose();
    invAd.block<3, 3>(0, 3) = -R.transpose() * hat(p);
    invAd.block<3, 3>(3, 0) = Eigen::Matrix3d::Zero();
    return invAd;
}


/*!
 * \brief Returns the inverse of the representation Jacobian matrix, mapping the RPY angles rates into the body angular velocity.
 *
 * Considering roll, picth, yaw as \f$ \phi, \theta, \psi \in \mathbb{R} \f$, the inverse of the representation Jacobian matrix is:
 *
 * \f[ T_{ab}^{-1} =
 * \left[\begin{array}{ccc}
 * 1 & 0 & -s(\theta_{ab}) \\
 * 0 & c(\phi_{ab}) & c(\theta_{ab})s(\phi_{ab}) \\
 * 0 & -s(\phi_{ab}) & c(\theta_{ab})c(\phi_{ab}) \end{array}\right] \f]
 *
 * Note that the roll, picth, yaw singularity of \f$ \theta = \pm\pi/2 \f$ causes this matrix to lose rank.
 *
 * \param rpy Vector of the roll, pitch and yaw angles.
 * \return Inverse of the representation Jacobian matrix of the roll, picth, yaw representation.
 */
template<typename Derived>
Eigen::Matrix3d inverseJacobianRepRPY(const Eigen::MatrixBase<Derived> & rpy)
{
    double s_phi = sin(rpy(0));
    double c_phi = cos(rpy(0));
    double s_theta = sin(rpy(1));
    double c_theta = cos(rpy(1));
    Eigen::Matrix3d invT;
    invT << 1, 0, -s_theta,
            0, c_phi, c_theta * s_phi,
            0, -s_phi, c_theta * c_phi;
    return invT;
}

/*!
 * \brief Returns the inverse of the velocity transformation matrix, which maps back the pose derivative into the body velocity twist.
 *
 * Considering the rotation matrix \f$ R_{ab} \in SO(3) \f$ and the representation Jacobian matrix of the roll, picth, yaw representation \f$ T_{ab} \in \mathbb{R}^{3 \times 3} \f$, the inverse of the velocity transformation matrix is:
 *
 * \f[ J_{ab}^{-1} =
 * \left[\begin{array}{cc}
 * R_{ab}^T & \mathbf{0} \\
 * \mathbf{0} & T_{ab}^{-1} \end{array}\right] \f]
 *
 * \param R Rotation matrix.
 * \param invT Inverse of the representation Jacobian matrix of the roll, picth, yaw representation.
 * \return Inverse of the velocity transformation matrix.
 */
template<typename Derived1, typename Derived2>
Eigen::Matrix6d inverseJacobian(const Eigen::MatrixBase<Derived1> & R, const Eigen::MatrixBase<Derived2> & invT)
{
    Eigen::Matrix6d invJ;
    invJ.block<3, 3>(0, 0) = R.transpose();
    invJ.block<3, 3>(3, 3) = invT;
    invJ.block<3, 3>(3, 0) = invJ.block<3, 3>(0, 3) = Eigen::Matrix3d::Zero();
    return invJ;
}

/*!
 * \brief Returns the time derivative of a rotation matrix based on the RPY angles.
 *
 * The time derivative \f$ \dot{R}_{ab} \in \mathbb{R}^{3 \times 3} \f$ of a rotation matrix \f$ R_{ab} \in SO(3) \f$ is calculated by deriving each of its terms.
 * Consider roll, picth, yaw angles \f$ \phi, \theta, \psi \in \mathbb{R} \f$ and roll, picth, yaw angle rates \f$ \dot{\phi}, \dot{\theta}, \dot{\psi} \in \mathbb{R} \f$. Then:
 *
 * \f[ R_{ab}(\phi, \theta, \psi) = R_{z}(\psi)R_{y}(\theta)R_{x}(\phi) \f]
 *
 * \f[ \dot{R}_{ab} = \frac{\partial R_{z}(\psi)}{\partial \psi}R_{y}(\theta)R_{x}(\phi)\dot{\psi} +
 * R_{z}(\psi)\frac{\partial R_{y}(\theta)}{\partial \theta}R_{x}(\phi)\dot{\theta} +
 * R_{z}(\psi)R_{y}(\theta)\frac{\partial R_{x}(\phi)}{\partial \phi}\dot{\phi} \f]
 *
 * The partial derivatives of the elementary rotations are given by:
 *
 * \f[
 * \frac{\partial R_{x}(\phi)}{\partial \phi} = \left[\begin{array}{ccc}
 * 0 & 0 & 0 \\
 * 0 & -s(\phi) & -c(\phi) \\
 * 0 & c(\phi) & -s(\phi)
 * \end{array}\right] \qquad
 * \frac{\partial R_{y}(\theta)}{\partial \theta} = \left[\begin{array}{ccc}
 * -s(\theta) & 0 & c(\theta) \\
 * 0 & 0 & 0 \\
 * -c(\theta) & 0 & -s(\theta)
 * \end{array}\right] \qquad
 * \frac{\partial R_{z}(\psi)}{\partial \psi} = \left[\begin{array}{ccc}
 * -s(\psi) & -c(\psi) & 0 \\
 * c(\psi) & -s(\psi) & 0 \\
 * 0 & 0 & 0
 * \end{array}\right] \f]
 *
 * \param rpy Vector of the roll, picth, yaw angles.
 * \param dotrpy Vector of the roll, picth, yaw angle rates.
 * \return Time derivative of the rotation matrix.
 */
template<typename Derived>
Eigen::Matrix3d dotRotationRepRPY(Eigen::MatrixBase<Derived> & rpy, Eigen::MatrixBase<Derived> & dot_rpy)
{
    Eigen::Matrix3d R_x = rotX(rpy[0]);
    Eigen::Matrix3d R_y = rotY(rpy[1]);
    Eigen::Matrix3d R_z = rotZ(rpy[2]);
    //
    return dotRotZ(rpy[2]) * R_y * R_x * dot_rpy[2]
            + R_z * dotRotY(rpy[1]) * R_x * dot_rpy[1]
            + R_z * R_y * dotRotX(rpy[0]) * dot_rpy[0];
}

/*!
 * \brief Returns the time derivative of the representation Jacobian matrix for the roll, picth, yaw representation.
 *
 * The formulation comes from deriving \f$ T_{ab}(\phi, \theta, \psi) \in \mathbb{R}^{3 \times 3} \f$ with \f$ \phi, \theta, \psi \in \mathbb{R}\f$ as the roll, picth, yaw angles.
 *
 * \f[ \dot{T}_{ab} = \begin{bmatrix}
 * 0 & c(\phi)t(\theta)\dot{\phi} + \frac{s(\phi)}{c^2(\theta)}\dot{\theta} & - s(\phi)t(\theta)\dot{\phi} + \frac{c(\phi)}{c^2(\theta)}\dot{\theta} \\
 * 0 & - s(\phi)\dot{\phi} & - c(\phi)\dot{\phi} \\
 * 0 & \frac{c(\phi)}{c(\theta)}\dot{\phi} + \frac{s(\phi)s(\theta)}{c^2(\theta)}\dot{\theta} & -\frac{s(\phi)}{c(\theta)}\dot{\phi} + \frac{c(\phi)s(\theta)}{c^2(\theta)}\dot{\theta}
 * \end{bmatrix} \f]
 *
 * \param rpy Vector of the roll, picth, yaw angles.
 * \param dotrpy Vector of the roll, picth, yaw angle rates.
 * \return Time derivative of the representation Jacobian matrix for the RPY representation.
 */
template<typename Derived>
Eigen::Matrix3d dotJacobianRepRPY(const Eigen::MatrixBase<Derived> & rpy, const Eigen::MatrixBase<Derived> & dotrpy)
{
    double s_phi = sin(rpy(0));
    double c_phi = cos(rpy(0));
    double dotphi = dotrpy(0);
    double s_theta = sin(rpy(1));
    double c_theta = cos(rpy(1));
    double t_theta = tan(rpy(1));
    double dottheta = dotrpy(1);
    Eigen::Matrix3d dotT;
    dotT << 0, (c_phi * t_theta) * dotphi + (s_phi/pow(c_theta, 2)) * dottheta, - s_phi * t_theta * dotphi + (c_phi/pow(c_theta, 2)) * dottheta,
            0, - dotphi * s_phi, - dotphi * c_phi,
            0, (c_phi/c_theta) * dotphi + (s_phi * s_theta/pow(c_theta, 2)) * dottheta, - (s_phi/c_theta) * dotphi + (c_phi * s_theta/pow(c_theta, 2)) * dottheta;
    return dotT;
}

/*!
 * \brief Returns the time derivative of the velocity transformation matrix.
 *
 * The formulation comes from deriving \f$ J_{ab}(R_{ab}, T_{ab}) \in \mathbb{R}^{6 \times 6} \f$.
 * \f[ \dot{J}_{ab} =
 * \left[\begin{array}{cc}
 * \dot{R}_{ab} & \mathbf{0} \\
 * \mathbf{0} & \dot{T}_{ab} \end{array}\right] \f]
 *
 * \param dotR Time derivative of the rotation matrix.
 * \param dotT Time derivative of the representation Jacobian matrix for the roll, picth, yaw representation.
 * \return Time derivative of the velocity transformation matrix.
 */
template<typename Derived>
Eigen::Matrix6d dotJacobian(const Eigen::MatrixBase<Derived> & dotR, const Eigen::MatrixBase<Derived> & dotT)
{
    Eigen::Matrix6d dotJ;
    dotJ.block<3, 3>(0, 0) = dotR;
    dotJ.block<3, 3>(3, 3) = dotT;
    dotJ.block<3, 3>(0, 3) = dotJ.block<3, 3>(3, 0) = Eigen::Matrix3d::Zero();
    return dotJ;
}

/*!
 * \brief Returns the time derivative of the inverse of the representation Jacobian matrix for the roll, picth, yaw representation. Note that we derive the inverse of T, not invert the derivative of T.
 *
 * The formulation comes from deriving \f$ T^{-1}_{ab} \in \mathbb{R}^{3 \times 3} \f$.
 * \f[ \dot{T}^{-1}_{ab} =
 * \left[\begin{array}{ccc}
 * 0 & 0 & -c(\theta)\dot{\theta} \\
 * 0 & -s(\phi)\dot{\phi} & c(\phi)c(\theta)\dot{\phi} - s(\phi)c(\theta)\dot{\theta} \\
 * 0 & -c(\phi)\dot{\phi} & -s(\phi)c(\theta)\dot{\phi} + c(\phi)s(\theta)\dot{\theta} \end{array}\right] \f]
 *
 * \param rpy Roll, pitch and yaw Euler angles.
 * \param dotrpy Roll, pitch and yaw Euler angle rates.
 * \return Time derivative of the inverse of the representation Jacobian matrix for the roll, picth, yaw representation.
 */
template<typename Derived>
Eigen::Matrix3d dotInverseJacobianRepRPY(const Eigen::MatrixBase<Derived> & rpy, const Eigen::MatrixBase<Derived> & dotrpy)
{
    double s_phi = sin(rpy(0));
    double c_phi = cos(rpy(0));
    double dotphi = dotrpy(0);
    double s_theta = sin(rpy(1));
    double c_theta = cos(rpy(1));
    double dottheta = dotrpy(1);
    Eigen::Matrix3d dotinvT;
    dotinvT << 0, 0, - c_theta * dottheta,
            0, - s_phi * dotphi, c_phi * c_theta * dotphi - s_phi * s_theta * dottheta,
            0, - c_phi * dotphi, - s_phi * c_theta * dotphi - c_phi * s_theta * dottheta;
    return dotinvT;
}

/*!
 * \brief Returns the time derivative of the inverse of the velocity transformation matrix \f$ J_{ab} \f$. The expression is found by grouping the derivative of the inverse of the rotation matrix \f$ R_{ab} \f$
 * with the derivative of the inverse of the representation Jacobian \f$ T_{ab} \f$.
 *
 * \f[ \dot{J}^{-1}_{ab} =
 * \left[\begin{array}{cc}
 * \dot{R}^{T}_{ab} & \mathbf{0}  \\
 * \mathbf{0} & \dot{T}^{-1}_{ab} \end{array}\right] \f]
 *
 * \param dotinvR Time derivative of the inverse of the rotation matrix.
 * \param dotinvT Time derivative of the inverse of the representation Jacobian matrix for the roll, picth, yaw representation.
 * \return Time derivative of the inverse of the representation Jacobian matrix for the roll, picth, yaw representation.
 */
template<typename Derived>
Eigen::Matrix6d dotInverseJacobian(const Eigen::MatrixBase<Derived> & dot_inv_R, const Eigen::MatrixBase<Derived> & dot_inv_T)
{
    Eigen::Matrix6d dotinvJ;
    dotinvJ.block<3, 3>(0, 0) = dot_inv_R;
    dotinvJ.block<3, 3>(3, 3) = dot_inv_T;
    dotinvJ.block<3, 3>(0, 3) = dotinvJ.block<3, 3>(3, 0) = Eigen::Matrix3d::Zero();
    return dotinvJ;
}

}
}
