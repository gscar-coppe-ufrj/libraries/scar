#include <gtest/gtest.h>
#include "scar/rbd/FrameRelation.hpp"
#include "scar/rbd/RigidBody.hpp"
#include <yaml-cpp/yaml.h>


using namespace scar::rbd;
using namespace std;


template<int rows, int cols>
void operator >> (const YAML::Node & node, Eigen::Matrix<double, rows, cols> & m)
{
    // the yml data is written column by column
    for (unsigned int j = 0; j < cols; j++)
    {
        for (unsigned int i = 0; i < rows; i++)
            m(i, j) = node[rows * j + i].as<double>();
    }
}


void operator >> (const YAML::Node& node, RigidBody& body)
{
    body.mass = node["m"].as<double>();
    Eigen::Vector3d tmp;
    node["I"] >> tmp;
    body.inertia_matrix = tmp.asDiagonal();
    node["R"] >> body.R;
    node["eta_1"] >> body.pos;
    node["eta_2"] >> body.rpy;
    body.pose << body.pos, body.rpy;
    node["doteta_1"] >> body.dot_pos;
    node["doteta_2"] >> body.dot_rpy;
    body.dot_pose << body.dot_pos, body.dot_rpy;
    node["dotdoteta_1"] >> body.ddot_pos;
    node["dotdoteta_2"] >> body.ddot_rpy;
    body.ddot_pose << body.ddot_pos, body.ddot_rpy;
    node["V"] >> body.dot_pose_target;
    node["dotV"] >> body.ddot_pose_target;
    node["g"] >> body.g;
    node["Ad"] >> body.Ad;
    node["T"] >> body.T;
    node["J"] >> body.J;
    node["invAd"] >> body.inv_Ad;
    node["invT"] >> body.inv_T;
    node["invJ"] >> body.inv_J;
    node["dotR"] >> body.dot_R;
    node["dotT"] >> body.dot_T;
    node["dotJ"] >> body.dot_J;
    node["dotinvT"] >> body.dot_inv_T;
    node["dotinvJ"] >> body.dot_inv_J;
    node["M_eta"] >> body.M_pose;
    node["C_eta"] >> body.C_pose;
    node["G_eta"] >> body.G_pose;
}

void copyData(const RigidBody& src, RigidBody& dst)
{
    dst.pos_target = src.pos_target;
    dst.pos = src.pos;
    dst.rpy = src.rpy;

    dst.dot_pose_target = src.dot_pose_target;
    dst.dot_pos = src.dot_pos;
    dst.dot_rpy = src.dot_rpy;
    dst.dot_pose = src.dot_pose;

    dst.ddot_pose_target = src.ddot_pose_target;

    dst.mass = src.mass;
    dst.inertia_matrix = src.inertia_matrix;
}

TEST(TestFrameRelation, test_static_methods)
{
    YAML::Node doc = YAML::LoadFile("@CMAKE_CURRENT_LIST_DIR@/test/test_frame_relation.yml");

    for (unsigned i = 0; i < doc.size(); i++)
    {
        RigidBody body_data, body_calc;

        // Populate the data
        doc[i] >> body_data;

        // Copy inputs read from the yaml file
        copyData(body_data, body_calc);

        body_calc.setMode(FrameRelation::Target, FrameRelation::Dynamic);
        Eigen::Vector6d pose_target;
        pose_target << body_data.pos_target, body_data.rpy;
        body_calc.update(pose_target, body_data.dot_pose_target, body_data.ddot_pose_target);

        double tolerance = 1e-10;
        double error;

        error = (body_calc.R - body_data.R).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc R =" << body_calc.R << std::endl
                                        << "Data R =" << body_data.R << std::endl;

        error = (body_calc.g - body_data.g).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc g =" << body_calc.g << std::endl
                                        << "Data g =" << body_data.g << std::endl;

        error = (body_calc.Ad - body_data.Ad).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc Ad =" << body_calc.Ad << std::endl
                                        << "Data Ad =" << body_data.Ad << std::endl;

        error = (body_calc.T - body_data.T).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc T =" << body_calc.T << std::endl
                                        << "Data T =" << body_data.T << std::endl;

        error = (body_calc.J - body_data.J).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc J =" << body_calc.J << std::endl
                                        << "Data J =" << body_data.J << std::endl;

        error = (body_calc.inv_Ad - body_data.inv_Ad).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc invAd =" << body_calc.inv_Ad << std::endl
                                        << "Data invAd =" << body_data.inv_Ad << std::endl;

        error = (body_calc.inv_T - body_data.inv_T).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc invT =" << body_calc.inv_T << std::endl
                                        << "Data invT =" << body_data.R << std::endl;

        error = (body_calc.inv_J - body_data.inv_J).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc invJ =" << body_calc.inv_J << std::endl
                                        << "Data invJ =" << body_data.inv_J << std::endl;

        error = (body_calc.dot_R - body_data.dot_R).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc dotR =" << body_calc.dot_R << std::endl
                                        << "Data dotR =" << body_data.dot_R << std::endl;

        error = (body_calc.dot_T - body_data.dot_T).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc dotT =" << body_calc.dot_T << std::endl
                                        << "Data dotT =" << body_data.dot_T << std::endl;

        error = (body_calc.dot_J - body_data.dot_J).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc dotJ =" << body_calc.dot_J << std::endl
                                        << "Data dotJ =" << body_data.dot_J << std::endl;

        error = (body_calc.dot_inv_T - body_data.dot_inv_T).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc dot_invT =" << body_calc.dot_inv_T << std::endl
                                        << "Data dot_invT =" << body_data.dot_inv_T << std::endl;

        error = (body_calc.dot_inv_J - body_data.dot_inv_J).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc dot_invJ =" << body_calc.dot_inv_J << std::endl
                                        << "Data dot_invJ =" << body_data.dot_inv_J << std::endl;

        error = (body_calc.dot_pose - body_data.dot_pose).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc dot_pose =" << body_calc.dot_pose << std::endl
                                        << "Data dot_pose =" << body_data.dot_pose << std::endl;

        error = (body_calc.ddot_pose - body_data.ddot_pose).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc ddot_pose =" << body_calc.ddot_pose << std::endl
                                        << "Data ddot_pose =" << body_data.ddot_pose << std::endl;

        body_calc.calc(true, true, true);
        error = (body_calc.M_pose - body_data.M_pose).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc M_pose =" << body_calc.M_pose << std::endl
                                        << "Data M_pose =" << body_data.M_pose << std::endl;

        error = (body_calc.C_pose - body_data.C_pose).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc C_pose =" << body_calc.C_pose << std::endl
                                        << "Data C_pose =" << body_data.C_pose << std::endl;

        error = (body_calc.G_pose - body_data.G_pose).cwiseAbs().maxCoeff();
        EXPECT_FALSE(error > tolerance) << "max error =" << error << std::endl
                                        << "Calc G_pose =" << body_calc.G_pose << std::endl
                                        << "Data G_pose =" << body_data.G_pose << std::endl;
    }
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
