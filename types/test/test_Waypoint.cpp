#include <gtest/gtest.h>
#include "scar/types/Waypoint.hpp"


using namespace ::scar::types;


TEST(TestWaypoint, test_constructors)
{
    Eigen::MatrixXd data(3,3), min(3,3), max(3,3), res(3,3), min_null(2,2), max_null(2,2);
    data <<  1,  2,  3,
             1,  2,  3,
             1,  2,  3;
    min  <<  2,  0,  0,
             0,  3,  0,
             0,  0,  4;
    max  <<  9, .2,  9,
            .1,  9, .3,
             9,  9,  9;
    res  <<  2, .2,  3,
            .1,  3, .3,
             1,  2,  4;
    Time time = Time::fromMicroseconds(0);

    Waypoint wp;
    EXPECT_TRUE(wp.isNull());
    wp = Waypoint(Waypoint::Data(), Time());
    EXPECT_TRUE(wp.isNull());
    wp = Waypoint(data, Time());
    EXPECT_TRUE(wp.isNull());
    wp = Waypoint(Waypoint::Data(), time);
    EXPECT_TRUE(wp.isNull());
    wp = Waypoint(data, min_null, max, time);
    EXPECT_TRUE(wp.isNull());
    wp = Waypoint(data, min, max_null, time);
    EXPECT_TRUE(wp.isNull());
    wp = Waypoint(data, max, min, time);
    EXPECT_TRUE(wp.isNull());

    wp = Waypoint(data, time);
    EXPECT_FALSE(wp.isNull());
    wp = Waypoint(data, min, max, time);
    EXPECT_FALSE(wp.isNull());

    EXPECT_TRUE( (wp.getData().array() == res.array()).all() );
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
