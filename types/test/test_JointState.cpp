#include <gtest/gtest.h>
#include "scar/types/JointState.hpp"


using namespace ::scar::types;


const Point zero({ 0,  0,  0});
const Point pt_a({.2, .2, .2});
const Point pt_b({.4, .4, .4});
const Point pt_c({.6, .6, .6});
const Point pt_d({.8, .8, .8});
const Point ones({ 1,  1,  1});

const JointState js_zero(zero, Time::fromSeconds(0));
const JointState js_pt_a(pt_a, Time::fromSeconds(1));
const JointState js_pt_b(pt_b, Time::fromSeconds(2));
const JointState js_pt_c(pt_c, Time::fromSeconds(3));
const JointState js_pt_d(pt_d, Time::fromSeconds(4));
const JointState js_ones(ones, Time::fromSeconds(5));

TEST(TestTrajectory, test_constructors)
{
    JointState js;
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(Point(), Time());
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(pt_a, Time());
    EXPECT_TRUE(js.isNull());
    //
    Time time = Time::fromMicroseconds(0);
    js = JointState(Point(), time);
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(pt_a, Point(), time);
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(pt_a, pt_b, Time());
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(pt_a, pt_b, Point(), time);
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(pt_a, pt_b, pt_c, Time());
    EXPECT_TRUE(js.isNull());
    //
    js = JointState(pt_a, time);
    EXPECT_FALSE(js.isNull());
    //
    js = JointState(pt_a, pt_b, time);
    EXPECT_FALSE(js.isNull());
    //
    js = JointState(pt_a, pt_b, pt_c, time);
    EXPECT_FALSE(js.isNull());
}

TEST(TestTrajectory, test_operators)
{
    Time t1 = Time::fromMicroseconds(123456);
    Time t2 = Time::fromMicroseconds(654321);
    JointState js_a_t1(pt_a, t1);
    JointState js_a_t2(pt_a, t2);
    JointState js_b_t1(pt_b, t1);
    EXPECT_TRUE(js_a_t1 == js_a_t1);
    EXPECT_TRUE(js_a_t1 != js_a_t2);
    EXPECT_TRUE(js_b_t1 != js_a_t1);
    EXPECT_TRUE(js_b_t1 != js_a_t2);
}

TEST(TestTrajectory, test_bounds)
{
    JointState js(pt_a, js_pt_b.getTime());
    JointState::setLowerBound(js, js_pt_b);
    EXPECT_TRUE(js == js_pt_b);
    //
    js = JointState(pt_b, js_pt_a.getTime());
    JointState::setUpperBound(js, js_pt_a);
    EXPECT_TRUE(js == js_pt_a);
}

TEST(TestTrajectory, test_methods)
{
    Time time = Time::fromMicroseconds(123456);
    EXPECT_TRUE( JointState(Point(), time).getTime() == time );
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
