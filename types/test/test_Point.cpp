#include <gtest/gtest.h>
#include "scar/types/Point.hpp"


using namespace ::scar::types;


const Point pt_a({1, 2, 3});
const Point pt_b({2, 3, 4}); // b > a
const Point pt_c({1, 3, 4}); // c >= a && c <= b
const Point pt_d({1, 2, 3}); // d == a
const Point pt_e({1, 3, 1});
const Point zero({0, 0, 0});

TEST(TestPoint, test_constructors)
{
    Point pt;
    EXPECT_TRUE(pt.isNull());
    pt = Point(std::vector<double>(3,0));
    EXPECT_FALSE(pt.isNull());
}

TEST(TestPoint, test_operators)
{
    EXPECT_TRUE(pt_b >  pt_a);
    EXPECT_TRUE(pt_c >= pt_a);
    EXPECT_TRUE(pt_c <= pt_b);
    EXPECT_TRUE(pt_d == pt_a);
    EXPECT_TRUE(Point() == Point());
    EXPECT_TRUE(pt_a != pt_b);
    EXPECT_TRUE(pt_b != pt_c);
    EXPECT_TRUE(pt_c != pt_d);
    EXPECT_TRUE(pt_a != Point());

    EXPECT_TRUE(pt_a > pt_a-pt_b);
    EXPECT_TRUE(pt_a < pt_a+pt_b);

    EXPECT_TRUE(pt_a > -pt_a);
    EXPECT_TRUE(pt_a < 2*pt_a);
    EXPECT_TRUE(2*pt_a == pt_a*2);
    EXPECT_TRUE(pt_a > pt_a/2);
    EXPECT_TRUE(zero == 0.0 * pt_a);
    EXPECT_TRUE(zero == pt_a - pt_a);
    EXPECT_TRUE(pt_a < pt_a + pt_a);
}

TEST(TestPoint, test_constrain)
{
    Point pt = pt_b;
    Point::setUpperBound(pt, pt_a);
    EXPECT_TRUE(pt == pt_a);
    EXPECT_FALSE(pt.isNull());
    //
    pt = pt_e;
    Point::setUpperBound(pt, pt_a);
    EXPECT_TRUE(pt == Point({1, 2, 1}));
    EXPECT_FALSE(pt.isNull());
    //
    pt = pt_a;
    Point::setLowerBound(pt, pt_b);
    EXPECT_TRUE(pt == pt_b);
    EXPECT_FALSE(pt.isNull());
    //
    pt = pt_d;
    Point::setLowerBound(pt, pt_e);
    EXPECT_TRUE(pt == Point({1, 3, 3}));
    EXPECT_FALSE(pt.isNull());
    //
    pt = pt_a;
    Point::setBounds(pt, Point(), Point());
    EXPECT_TRUE(pt == pt_a);
    EXPECT_FALSE(pt.isNull());
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
