#include <gtest/gtest.h>
#include "scar/types/Time.hpp"


using namespace ::scar::types;


TEST(TestTime, test_constructors)
{
    Time time;
    EXPECT_TRUE(time.isNull());
    time = Time::fromSeconds(0);
    EXPECT_FALSE(time.isNull());
    time = Time::fromMicroseconds(1);
    EXPECT_FALSE(time.isNull());
}

TEST(TestTime, test_operators)
{
    Time t_micr = Time::fromMicroseconds(1000000);
    Time t_mill = Time::fromMilliseconds(1000);
    Time t_secs = Time::fromSeconds(1);
    EXPECT_TRUE(t_micr == t_mill);
    EXPECT_TRUE(t_micr == t_secs);
    EXPECT_TRUE(t_mill == t_secs);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
