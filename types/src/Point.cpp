#include "scar/types/Point.hpp"
#include <algorithm>
#include <sstream>


namespace scar
{
namespace types
{


Point::Point() : Point(std::vector<double>(0, 0))
{
    default_called = true;
}

Point::Point(const std::vector<double>& data) :
    default_called(false), data(data)
{
}

const std::vector<double>& Point::getData() const
{
    return data;
}

size_t Point::size() const
{
    return data.size();
}

bool Point::operator < (const Point& pt) const
{
    return size() == pt.size() &&
        std::equal(data.cbegin(), data.cend(), pt.data.cbegin(),
            [](double a, double b){return a < b;}
    );
}

bool Point::operator > (const Point& pt) const
{
    return size() == pt.size() &&
        std::equal(data.cbegin(), data.cend(), pt.data.cbegin(),
            [](double a, double b){return a > b;}
    );
}

bool Point::operator <= (const Point& pt) const
{
    return size() == pt.size() &&
        std::equal(data.cbegin(), data.cend(), pt.data.cbegin(),
            [](double a, double b){return a <= b;}
    );
}

bool Point::operator >= (const Point& pt) const
{
    return size() == pt.size() &&
        std::equal(data.cbegin(), data.cend(), pt.data.cbegin(),
            [](double a, double b){return a >= b;}
    );
}

bool Point::operator == (const Point& pt) const
{
    return size() == pt.size() &&
        std::equal(data.cbegin(), data.cend(), pt.data.cbegin(),
            [](double a, double b){return a == b;}
    );
}

bool Point::operator != (const Point& pt) const
{
    return !(*this == pt);
}

Point Point::operator - (const Point& pt) const
{
    if (pt.size() != size())
        throw std::logic_error("Cannot perform arithmetics on points of different sizes");
    std::vector<double> ret(size());
    std::transform(data.cbegin(), data.cend(), pt.data.cbegin(), ret.begin(),
        [](double a, double b){return a - b;}
    );
    return Point(ret);
}

Point Point::operator + (const Point& pt) const
{
    if (pt.size() != size())
        throw std::logic_error("Cannot perform arithmetics on points of different sizes");
    std::vector<double> ret(size());
    std::transform(data.cbegin(), data.cend(), pt.data.cbegin(), ret.begin(),
        [](double a, double b){return a + b;}
    );
    return Point(ret);
}

Point Point::operator / (double num) const
{
    std::vector<double> ret(size());
    std::transform(data.cbegin(), data.cend(), ret.begin(),
        [num](double a){return a / num;}
    );
    return Point(ret);
}

Point Point::operator - () const
{
    std::vector<double> ret(size());
    std::transform(data.cbegin(), data.cend(), ret.begin(),
        [](double a){return -a;}
    );
    return Point(ret);
}

bool Point::isNull() const
{
    return default_called;
}

Point::operator std::string() const
{
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

void Point::setUpperBound(Point& pt, const Point& max)
{
    if (pt.isNull() || max.isNull() || pt.size() != max.size())
        return;
    std::transform(pt.data.cbegin(), pt.data.cend(), max.data.cbegin(), pt.data.begin(),
        [](double val_pt, double val_max)
        {
            return (val_pt > val_max) ? val_max : val_pt;
        }
    );
}

void Point::setLowerBound(Point& pt, const Point& min)
{
    if (pt.isNull() || min.isNull() || pt.size() != min.size())
        return;
    std::transform(pt.data.cbegin(), pt.data.cend(), min.data.cbegin(), pt.data.begin(),
        [](double val_pt, double val_min)
        {
            return (val_pt < val_min) ? val_min : val_pt;
        }
    );
}

void Point::setBounds(Point& pt, const Point& min, const Point& max)
{
    setLowerBound(pt, min);
    setUpperBound(pt, max);
}

std::ostream& operator << (std::ostream& io, const Point& point)
{
    io << "[";
    const std::vector<double>& data = point.getData();
    if (!data.empty())
    {
        io << data[0];
        for (auto iter = data.cbegin()+1 ; iter != data.cend() ; ++iter)
        {
            io << ", " << *iter;
        }
    }
    io << "]";

    return io;
}

Point operator * (const Point& pt, double num)
{
    std::vector<double> ret(pt.size());
    std::transform(pt.getData().cbegin(), pt.getData().cend(), ret.begin(),
        [num](double a){return a * num;}
    );
    return Point(ret);
}

Point operator * (double num, const Point& pt)
{
    return pt * num;
}


}
}
