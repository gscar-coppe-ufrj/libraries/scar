#include "scar/types/JointState.hpp"

#include <sstream>


namespace scar
{
namespace types
{

JointState::JointState() :
    pos(), vel(), acc(), eff(), time(), is_null(true)
{
}

JointState::JointState(const Point& pos, Time time) :
    pos(pos), vel(), acc(), eff(), time(time)
{
    is_null = time.isNull() || pos.isNull();
}

JointState::JointState(const Point& pos, const Point& eff, Time time) :
    pos(pos), vel(), acc(), eff(eff), time(time)
{
    is_null = time.isNull() || pos.isNull() || eff.isNull();
}

JointState::JointState(const Point& pos, const Point& vel, const Point& acc, Time time) :
    pos(pos), vel(vel), acc(acc), eff(), time(time)
{
    is_null = time.isNull() || pos.isNull() || vel.isNull() || acc.isNull();
}

const Point&JointState::getPos() const
{
    return pos;
}

const Point&JointState::getVel() const
{
    return vel;
}

const Point&JointState::getAcc() const
{
    return acc;
}

const Point&JointState::getEff() const
{
    return eff;
}

const Time& JointState::getTime() const
{
    return time;
}

size_t JointState::size() const
{
    if (isNull())
        return -1;
    else
        return pos.size();
}

bool JointState::operator == (const JointState& js) const
{
    return js.time == time && js.pos == pos && js.vel == vel && js.acc == acc && js.eff == eff;
}

bool JointState::operator != (const JointState& js) const
{
    return js.time != time || js.pos != pos || js.vel != vel || js.acc != acc || js.eff != eff;
}

bool JointState::isNull() const
{
    return is_null;
}

JointState::operator std::string() const
{
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

void JointState::setLowerBound(JointState& js, const JointState& min)
{
    Point::setLowerBound(js.pos, min.pos);
    Point::setLowerBound(js.vel, min.vel);
    Point::setLowerBound(js.acc, min.acc);
    Point::setLowerBound(js.eff, min.eff);
}


void JointState::setUpperBound(JointState& js, const JointState& max)
{
    Point::setUpperBound(js.pos, max.pos);
    Point::setUpperBound(js.vel, max.vel);
    Point::setUpperBound(js.acc, max.acc);
    Point::setUpperBound(js.eff, max.eff);
}

void JointState::setBounds(JointState& js, const JointState& min, const JointState& max)
{
    JointState::setLowerBound(js, min);
    JointState::setUpperBound(js, max);
}

std::ostream& operator << (std::ostream& io, const JointState& js)
{
    io << "pos: " << js.getPos()
       << "\nvel: " << js.getVel()
       << "\nacc: " << js.getAcc()
       << "\neff: " << js.getEff()
       << "\ntime: " << js.getTime();
    return io;
}


}
}
