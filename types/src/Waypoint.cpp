#include "scar/types/Waypoint.hpp"

#include <sstream>


using namespace ::scar::types;


Waypoint::Waypoint() :
    is_null(true)
{
}

Waypoint::Waypoint(const Data& data, const Time& time) :
    Waypoint(data, Data(), Data(), time)
{
}

Waypoint::Waypoint(const Data& data, const Data& min, const Data& max, const Time& time) :
    is_null(data.rows() < 1 || data.cols() < 1 || time.isNull()),
    data(data), time(time)
{
    if (is_null)
        return;
    bool has_min = false, has_max = false;
    if (min.diagonalSize() > 0)
    {
        if ( data.rows() != min.rows() || data.cols() != min.cols() )
        {
            is_null = true;
            return;
        }
        this->data = (this->data.array() < min.array()).select(min, this->data);
        has_min = true;
    }
    if (max.diagonalSize() > 0)
    {
        if ( data.rows() != max.rows() || data.cols() != max.cols() )
        {
            is_null = true;
            return;
        }
        this->data = (this->data.array() > max.array()).select(max, this->data);
        has_max = true;
    }
    if (has_max && has_min)
        is_null = (min.array() > max.array()).any();
}

Waypoint::~Waypoint()
{
}

const Waypoint::Data& Waypoint::getData() const
{
    return data;
}

const Time& Waypoint::getTime() const
{
    return time;
}

// ValidType interface
bool Waypoint::isNull() const
{
    return is_null;
}

scar::types::Waypoint::operator std::string() const
{
    std::stringstream ss;
    ss << *this;
    return ss.str();
}
// end - ValidType interface

std::ostream& ::scar::types::operator << (std::ostream& io, const Waypoint& wp)
{
    io << "data:" << std::endl << wp.getData() << std::endl << "time: " << wp.getTime();
    return io;
}
