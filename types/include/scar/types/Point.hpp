/**
 * @file Point.hpp
 * @author LEAD
 * @brief Defines an abastract point that holds a generic data.
 * The point may be the basic type of a trajectory.
 *
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once


#include <vector>
#include "scar/types/Time.hpp"
#include "scar/types/ValidType.hpp"


namespace scar
{
namespace types
{


class Point : public ValidType
{
private:
    bool default_called;

    std::vector<double> data;

public:
    Point();
    Point(const std::vector<double>& data);

    const std::vector<double>& getData() const;
    size_t size() const;

    bool operator < (Point const& pt) const;
    bool operator > (Point const& pt) const;
    bool operator >= (Point const& pt) const;
    bool operator <= (Point const& pt) const;
    bool operator == (Point const& pt) const;
    bool operator != (Point const& pt) const;
    Point operator - (Point const& pt) const;
    Point operator + (Point const& pt) const;
    Point operator / (double num) const;
    Point operator - () const;

    bool isNull() const override;
    operator std::string() const override;

    /**
     * @brief Constrains the \p pt Point to be below (element-wise) the maximum \p max
     * @param pt Point to constrain element-wise
     * @param max Maximum value
     * @warning Nothing is done if either point is null or pt.size() != max.size()
     * @see #constrainBelow, #constrain
     */
    static void setUpperBound(Point& pt, const Point& max);

    /**
     * @brief Constrains the \p pt Point to be above (element-wise) the minimum \p min
     * @param pt Point to constrain element-wise
     * @param max Minimum value
     * @warning Nothing is done if either point min is null or pt.size() != min.size()
     * @see #constrainAbove, #constrain
     */
    static void setLowerBound(Point& pt, const Point& min);

    /**
     * @brief Constrains above and below
     * @see #constrainBelow, #constrainAbove
     */
    static void setBounds(Point& pt, const Point& min, const Point& max);
};

std::ostream& operator << (std::ostream& io, Point const& point);
Point operator * (const Point& pt, double num);
Point operator * (double num, const Point& pt);


}
}
