/**
 * @file Waypoint.hpp
 * @author LEAD
 * @brief Defines a waypoint type to be used as a vector of states.
 * This class is intended to be used by trajectories
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include "scar/types/Time.hpp"
#include <Eigen/Dense>


namespace scar
{
namespace types
{


class Waypoint : public ValidType
{
public:
    typedef Eigen::MatrixXd Data;

private:
    bool is_null;
    Eigen::MatrixXd data;
    Time time;

public:
    Waypoint();
    Waypoint(const Data& data, const Time& time);
    /**
     * @brief Waypoint Eigen::MatrixXd and types::Time objects
     * @param data
     * @param min
     * @param max
     * @param time
     * Change values in data to conform min and max requirements
     */
    Waypoint(const Data& data, const Data& min, const Data& max, const Time& time);
    ~Waypoint() override;

    const Data& getData() const;
    const Time& getTime() const;

// ValidType interface
public:
    bool isNull() const override;
    operator std::string() const override;
// end - ValidType interface
};

std::ostream& operator << (std::ostream& io, Waypoint const& wp);


}
}
