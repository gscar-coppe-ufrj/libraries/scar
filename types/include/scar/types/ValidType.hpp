/**
 * @file ValidType.hpp
 * @author LEAD
 * @brief Defines a base class that implements a method to check whether the type has been properly initialized.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once


#include <string>


namespace scar
{
namespace types
{


class ValidType
{
public:
    virtual ~ValidType();
    /**
     * @brief True if the type has not been properly initialized, false otherwise.
     *
     * The default implementation always returns true.
     */
    virtual bool isNull() const;

    /** Default conversion to string. */
    virtual operator std::string() const = 0;
};


}
}
