/**
 * @file JointState.hpp
 * @author LEAD
 * @brief Defines joint states position, velocity, acceleration and effort.
 * The class uses auxiliary functions to also set upper and lower bounds.
 *
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include "scar/types/Point.hpp"


namespace scar
{
namespace types
{


class JointState : public ValidType
{
private:
    Point pos, vel, acc, eff;
    Time time;
    bool is_null;

public:
    JointState();
    JointState(const Point& pos, Time time);
    JointState(const Point& pos, const Point& eff, Time time);
    JointState(const Point& pos, const Point& vel, const Point& acc, Time time);

    const Point& getPos() const;
    const Point& getVel() const;
    const Point& getAcc() const;
    const Point& getEff() const;
    const Time& getTime() const;
    size_t size() const;

    bool operator == (const JointState& js) const;
    bool operator != (const JointState& js) const;

    bool isNull() const override;
    operator std::string() const override;

    /**
     * @brief Constrains the JointState \p js position, velocity, acceleration and effort using the values in \p min
     * @param js JointState to constrain
     * @param min Minimum value
     * @see Point::constrainBelow
     */
    static void setLowerBound(JointState& js, const JointState& min);

    /**
     * @brief Constrains the JointState \p js position, velocity, acceleration and effort using the values in \p max
     * @param js JointState to constrain
     * @param max Maximum value
     * @see Point::constrainAbove
     */
    static void setUpperBound(JointState& js, const JointState& max);

    /**
     * @brief Constrains above and below
     * @see #constrainBelow, #constrainAbove
     */
    static void setBounds(JointState& js, const JointState& min, const JointState& max);
};

std::ostream& operator << (std::ostream& io, JointState const& point);


}
}
