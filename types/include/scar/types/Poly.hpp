/**
 * @file Poly.hpp
 * @author LEAD
 * @brief Defines a type to be used as a polynomial equation.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include <Eigen/Dense>


namespace scar
{
namespace types
{


typedef Eigen::VectorXd Poly;


}
}
