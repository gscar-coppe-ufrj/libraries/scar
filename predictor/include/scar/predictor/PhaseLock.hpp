/**
 * @file PhaseLock.hpp
 * @author LEAD
 * @brief Defines a class to perform phase lock and detect sinusoidal parameters.
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once


#include "scar/predictor/Predictor.hpp"
#include "scar/trajectory/TrajectorySine.hpp"

#include <memory>


namespace scar
{
namespace predictor
{


/**
 * @brief Knowing the frequency of a sinusoidal signal, this class predicts the amplitude, phase, and offset parameters
 */
class PhaseLock : public Predictor
{
public:
    typedef Eigen::Array<double, Eigen::Dynamic, 1> vec_t;

private:
    const vec_t frequency;
    std::unique_ptr<trajectory::TrajectorySine> trajectory_ptr;

public:
    PhaseLock(const vec_t& frequency, const types::Time& length_memory, const types::Time& length_prediction);

// Predictor interface
protected:
    bool update(Waypoints::const_iterator iter_begin, Waypoints::const_iterator iter_end) override;
    types::Waypoint predict(const types::Time& time) override;
};


}
}
