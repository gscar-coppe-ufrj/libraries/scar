/**
 * @file Predictor.hpp
 * @author LEAD
 * @brief Defines a predictor class
 * @version 0.1
 * @date 2019-12-19
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once


#include <vector>
#include <memory>

#include <Eigen/Dense>

#include "scar/types/Time.hpp"
#include "scar/types/Waypoint.hpp"


namespace scar
{
namespace predictor
{


typedef std::vector<types::Waypoint> Waypoints;
typedef Eigen::Array<double, Eigen::Dynamic, 1> quality_t;


class Predictor : public std::enable_shared_from_this<Predictor>
{
public:
    typedef std::shared_ptr<Predictor> SharedPtr;
    typedef std::unique_ptr<Predictor> UniquePtr;
    typedef std::shared_ptr<const Predictor> ConstSharedPtr;
    typedef std::unique_ptr<const Predictor> ConstUniquePtr;

private:
    const types::Time length_memory;
    const types::Time length_prediction;
    types::Time time_init_estimate;

    quality_t quality;

    unsigned int number_new_measurements;

protected:
    virtual void updateQuality(
        Waypoints::const_iterator iter_begin,
        Waypoints::const_iterator iter_end,
        quality_t& quality
    );

    virtual bool update(
        Waypoints::const_iterator iter_begin,
        Waypoints::const_iterator iter_end
    ) = 0;

    virtual types::Waypoint predict(const types::Time& time) = 0;

public:
    Predictor(const types::Time& length_memory, const types::Time& length_prediction);
    virtual ~Predictor();

    inline void updateQuality(Waypoints::const_iterator iter_begin, Waypoints::const_iterator iter_end)
    {
        return updateQuality(iter_begin, iter_end, quality);
    }

    bool update(const Waypoints& traj, const types::Time& time_init_estimate);
    bool update(
        Waypoints::const_iterator iter_begin,
        Waypoints::const_iterator iter_end,
        const types::Time& time_init_estimate
    );

    /**
     * @brief Returns the prediction at time \p time
     * @param time Time to acquire the prediction
     * @return The prediction
     */
    types::Waypoint get(const types::Time& time);

    /**
     * @brief Returns true if the time lies within the prediction window
     */
    inline bool isValidTime(const types::Time& time)
    {
        return !time.isNull() && !time_init_estimate.isNull() &&
            (time <= time_init_estimate + length_prediction && time >= time_init_estimate);
    }

    /**
     * @brief Returns the quality of the current prediction for each dimension, or a vector of size
     * zero if the last call to #update returned false
     * @return The current prediction quality, where zero corresponds to a perfect fit
     */
    inline const quality_t& getQuality() const
    {
        return quality;
    }
};


}
}
