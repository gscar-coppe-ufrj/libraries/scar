#include "scar/predictor/PhaseLock.hpp"
#include <iostream>


using namespace ::scar::predictor;
using ::scar::trajectory::TrajectorySine;


PhaseLock::PhaseLock(const vec_t& frequency, const types::Time& length_memory, const types::Time& length_prediction) :
    Predictor(length_memory, length_prediction),
    frequency(frequency)
{
}

bool PhaseLock::update(Waypoints::const_iterator iter_begin, Waypoints::const_iterator iter_end)
{
    // assert that the data size matches what is being predicted
    assert(iter_begin->getData().rows() == frequency.rows());

    // extract the number of sampels
    auto n_samples = std::distance(iter_begin, iter_end);

    // extract time and values
    typedef Eigen::Array<double, 1, Eigen::Dynamic> row_vec_t;
    row_vec_t time(1, n_samples);
    Eigen::ArrayXXd values(frequency.rows(), n_samples);
    vec_t max(frequency.rows());
    vec_t min(frequency.rows());
    vec_t col = iter_begin->getData().col(0);
    max = col;
    min = col;
    unsigned int k = 0;
    for (auto iter = iter_begin; iter != iter_end; ++iter)
    {
        col = iter->getData().col(0);
        values.col(k) = col;
        max = (col.array() > max).select(col, max);
        min = (col.array() < min).select(col, min);
        time(k) = iter->getTime().toSeconds();
        ++k;
    }

    // compute the expected sine parameters
    // x = amplitude * sin(frequency * time + phase) + offset
    Eigen::ArrayXXd nominal_sine(frequency.rows(), n_samples);
    for (unsigned int k = 0; k < frequency.rows(); ++k)
        nominal_sine.row(k) = Eigen::sin(frequency(k) * time);
    //
    vec_t offset = (max + min) / 2;
    //
    vec_t amplitude = max - offset;
    //
    Eigen::ArrayXXd values_norm = (values.colwise() - offset).colwise() / amplitude;
    Eigen::ArrayXXd distance = Eigen::pow(values_norm, 2) + Eigen::pow(nominal_sine, 2);
    vec_t distance_max = Eigen::sqrt(distance.rowwise().maxCoeff());
    vec_t distance_min = Eigen::sqrt(distance.rowwise().minCoeff());
    vec_t ratio = distance_min / distance_max;
    vec_t distance_atan2 = Eigen::atan(ratio);
    distance_atan2 = (Eigen::asin(ratio) < 0).select(-distance_atan2, distance_atan2);
    vec_t phase = 2 * distance_atan2;

    // determine if phase is positive or negative
    Eigen::MatrixXd config(frequency.rows(), 4);
    config.col(0) = amplitude;
    config.col(1) = frequency;
    config.col(2) = phase;
    config.col(3) = offset;
    //
    std::unique_ptr<TrajectorySine> trajectory_pos( new TrajectorySine(config, 0) );
    config.col(2) = -config.col(2);
    std::unique_ptr<TrajectorySine> trajectory_neg( new TrajectorySine(config, 0) );
    //
    Eigen::ArrayXXd values_pred_pos(frequency.rows(), n_samples);
    Eigen::ArrayXXd values_pred_neg(frequency.rows(), n_samples);
    k = 0;
    for (auto iter = iter_begin; iter != iter_end; ++iter)
    {
        auto wp_time = iter->getTime();
        values_pred_pos.col(k) = trajectory_pos->get(wp_time).getData().col(0);
        values_pred_neg.col(k) = trajectory_neg->get(wp_time).getData().col(0);
        ++k;
    }
    auto ind_neg = (values_pred_neg - values).abs().rowwise().sum() < (values_pred_pos - values).abs().rowwise().sum();
    config.col(2) = ind_neg.select(-phase, phase);

    trajectory_ptr = std::unique_ptr<TrajectorySine>(new TrajectorySine(config, 2));

    return true;
}

scar::types::Waypoint PhaseLock::predict(const scar::types::Time& time)
{
    if (trajectory_ptr)
        return trajectory_ptr->get(time);
    return types::Waypoint();
}

