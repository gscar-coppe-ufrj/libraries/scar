#include "scar/predictor/Predictor.hpp"
#include <iostream>


using namespace ::scar::predictor;


Predictor::Predictor(const types::Time& length_memory, const types::Time& length_prediction) :
    length_memory(length_memory),
    length_prediction(length_prediction),
    time_init_estimate(),
    quality(0),
    number_new_measurements(0)
{
}

Predictor::~Predictor()
{
}

void Predictor::updateQuality(
    Waypoints::const_iterator iter_begin,
    Waypoints::const_iterator iter_end,
    quality_t& quality
)
{
    quality = Eigen::pow(quality, 2) * number_new_measurements;
    while (iter_begin != iter_end)
    {
        quality += Eigen::pow((iter_begin->getData().col(0) - get(iter_begin->getTime()).getData().col(0)).array(), 2);
        ++iter_begin;
        ++number_new_measurements;
    }
    quality = Eigen::sqrt(quality) / number_new_measurements;
}

bool Predictor::update(const Waypoints& traj, const scar::types::Time& time_init_estimate)
{
    return update(traj.cbegin(), traj.cend(), time_init_estimate);
}

bool Predictor::update(
    Waypoints::const_iterator itb,
    Waypoints::const_iterator ite,
    const types::Time& time_init_estimate
)
{
    quality.resize(0);
    number_new_measurements = 0;
    this->time_init_estimate = types::Time();
    if (std::distance(itb, ite) < 1)
        return false;
    this->time_init_estimate = time_init_estimate;

    auto iter_begin = itb;
    auto iter_end = iter_begin;
    quality.setZero(iter_begin->getData().rows(), 1);
    for (auto it = itb; it != ite; ++it)
    {
        ++iter_end;
        auto wp_time = it->getTime();
        if (wp_time < time_init_estimate - length_memory)
            ++iter_begin;
        else if (wp_time > time_init_estimate)
            break;
    }
    --iter_end;
    bool ret = update(iter_begin, iter_end);
    if (isValidTime( (iter_end - 1)->getTime() ))
        updateQuality(iter_end - 1, iter_end, quality);
    return ret;
}

scar::types::Waypoint Predictor::get(const scar::types::Time& time)
{
    if (!isValidTime(time))
        std::cerr << "[WARN] (Predictor::get) requested a prediction outside the prediction window" << std::endl;
    return predict(time);
}
