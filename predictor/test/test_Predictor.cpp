#include <gtest/gtest.h>

#include <sstream>

#include "scar/predictor/Predictor.hpp"


using namespace ::scar::predictor;
using namespace ::scar::types;
using Eigen::ArrayXd;


std::vector<Waypoint> generateTrajectory(
    ArrayXd amplitude, ArrayXd frequency, ArrayXd phase,
    ArrayXd offset, Time ts, Time t_init, Time t_final
)
{
    unsigned int n_samples = static_cast<unsigned int>(
        std::ceil((t_final - t_init).toSeconds() / ts.toSeconds())
    );
    std::vector<Waypoint> samples(n_samples);
    Time time = t_init;
    Eigen::Index dim = amplitude.rows();
    for (auto& wp : samples)
    {
        Eigen::ArrayXd data(dim, 1);
        data = amplitude * Eigen::sin(frequency * time.toSeconds() + phase) + offset;
        wp = Waypoint(data, time);
        time += ts;
    }
    return samples;
}


class DummyPredictor : public Predictor
{
private:
    ArrayXd amp, freq, phase, off;
    const Eigen::Index dim;

// Predictor interface
protected:
    bool update(
        Waypoints::const_iterator,
        Waypoints::const_iterator
    ) override
    {
        return true;
    }

    Waypoint predict(const Time& time) override
    {
        Eigen::ArrayXd data(dim, 1);
        data = amp * Eigen::sin(freq * time.toSeconds() + phase) + off;
        return Waypoint(data, time);
    }
// end - Predictor interface

public:
    DummyPredictor(
        const Time& length_memory, const Time& length_prediction,
        ArrayXd amp, ArrayXd freq, ArrayXd phase, ArrayXd off
    ) : Predictor(length_memory, length_prediction), amp(amp), freq(freq), phase(phase), off(off), dim(amp.rows())
    {
    }
};


TEST(TestPredictor, test_methods)
{
    unsigned int dim = 4;
    ArrayXd amp(dim, 1);
    ArrayXd freq(dim, 1);
    ArrayXd phase(dim, 1);
    ArrayXd off(dim, 1);
    amp << 1, 2, 3, 4;
    freq << 1, 2, 3, 4;
    phase << 1, 2, 3, 4;
    off << 1, 2, 3, 4;
    Time ts = Time::fromMilliseconds(17);
    Time t0 = Time::fromSeconds(1.12312332);
    Time tf = Time::fromSeconds(2.38932123);
    auto trajectory = generateTrajectory(amp, freq, phase, off, ts, t0, tf);

    Time length_memory = Time::fromSeconds(1);
    Time length_prediction = Time::fromSeconds(3);
    Time time_init = Time::fromSeconds(2.2);
    Predictor::SharedPtr predictor = std::make_shared<DummyPredictor>(
        length_memory, length_prediction, amp, freq, phase, off
    );
    predictor->update(trajectory, time_init);
    EXPECT_TRUE( (predictor->getQuality() == 0).all() );

    // replace cerr buffer to parser its output
    std::ostringstream oss;
    std::streambuf* cerr_buff = std::cerr.rdbuf();
    std::cerr.rdbuf(oss.rdbuf());
    //
    predictor->get(time_init + length_prediction);
    EXPECT_TRUE(oss.str().empty());
    predictor->get(time_init + length_prediction + Time::fromMicroseconds(1));
    EXPECT_TRUE(oss.str().find("[WARN] (Predictor::get)") == 0); // the output starts with the given string
    //
    std::cerr.rdbuf(cerr_buff); // restore buffer
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
