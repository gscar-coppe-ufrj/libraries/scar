#include <gtest/gtest.h>

#include "scar/predictor/PhaseLock.hpp"


using namespace ::scar::predictor;
using namespace ::scar::types;
using Eigen::ArrayXd;


std::vector<Waypoint> generateTrajectory(
    ArrayXd amplitude, ArrayXd frequency, ArrayXd phase,
    ArrayXd offset, Time ts, Time t_init, Time t_final
)
{
    unsigned int n_samples = static_cast<unsigned int>(
        std::ceil((t_final - t_init).toSeconds() / ts.toSeconds())
    );
    std::vector<Waypoint> samples(n_samples);
    Time time = t_init;
    unsigned int dim = amplitude.rows();
    for (auto& wp : samples)
    {
        Eigen::ArrayXd data(dim, 1);
        data = amplitude * Eigen::sin(frequency * time.toSeconds() + phase) + offset;
        wp = Waypoint(data, time);
        time += ts;
    }
    return samples;
}


TEST(TestPhaseLock, test_methods)
{
    unsigned int dim = 4;
    ArrayXd amp(dim, 1);
    PhaseLock::vec_t freq(dim, 1);
    ArrayXd phase(dim, 1);
    ArrayXd off(dim, 1);
    amp << 1.1, 2.4, 3, 4;
    freq << 12, 2.123, 3, 4;
    phase << .14, .2, -.23, 1.1;
    off << 1, 0, -20, 10;
    Time ts = Time::fromSeconds(0.001);
    Time t0 = Time::fromSeconds(0.5);
    Time delta = Time::fromSeconds(2 * M_PI / freq.minCoeff());
    Time tf = t0 + 5*delta;
    auto trajectory = generateTrajectory(amp, freq, phase, off, ts, t0, tf);

    Time length_memory = delta;
    Time length_prediction = delta;
    std::shared_ptr<Predictor> predictor = std::make_shared<PhaseLock>(freq, length_memory, length_prediction);

    predictor->update(trajectory, t0 + length_memory);

    double tolerance = 1e-4;
    for (auto wp : trajectory)
    {
        auto time = wp.getTime();
        if (!predictor->isValidTime(time))
            continue;
        auto distance = (wp.getData().col(0) - predictor->get(time).getData().col(0)).array().abs().maxCoeff();
        EXPECT_TRUE(distance < tolerance);
    }
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}
